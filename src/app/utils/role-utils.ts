/**
 * @author meididiallo
 */
import {Injectable} from '@angular/core';
import {DsnUtils} from './dsn-utils';

@Injectable()
export class RoleUtils {

    dsnUtils: DsnUtils;
    names: string[];

    constructor() {
        this.dsnUtils = new DsnUtils();
        this.names = this.dsnUtils.getPrivilegeNames();
    }

    // ==================== ASSIGN ========================== //

    canAssignTask(): boolean {
        return this.names.includes('ASSIGN_TASK');
    }

    // ==================== CANCEL ========================== //

    canCancelDevis(): boolean {
        return this.names.includes('CANCEL_DEVIS');
    }

    canCancelInvoice(): boolean {
        return this.names.includes('CANCEL_INVOICE');
    }

    canCancelInvoicePayment(): boolean {
        return this.names.includes('CANCEL_INVOICE_PAYMENT');
    }

    canCancelProduct(): boolean {
        return this.names.includes('CANCEL_PRODUCT');
    }

    canChangeAllUserPwd(): boolean {
        return this.names.includes('CHANGE_ALL_USER_PASSWORD');
    }

    canChangeUserPwd(): boolean {
        return this.names.includes('CHANGE_USER_PASSWORD');
    }

    // ==================== CONVERT ========================== //

    canConvertDevis(): boolean {
        return this.names.includes('CONVERT_DEVIS');
    }

    canConvertProspect(): boolean {
        return this.names.includes('CONVERT_PROSPECT');
    }

    // ==================== CREATE ========================== //

    canCreateContact(): boolean {
        return this.names.includes('CREATE_CONTACT');
    }

    canCreateCustomer(): boolean {
        return this.names.includes('CREATE_CUSTOMER');
    }

    canCreateDevis(): boolean {
        return this.names.includes('CREATE_DEVIS');
    }

    canCreateFile(): boolean {
        return this.names.includes('CREATE_FILE');
    }

    canCreateInvoice(): boolean {
        return this.names.includes('CREATE_INVOICE');
    }

    canCreateProduct(): boolean {
        return this.names.includes('CREATE_PRODUCT');
    }

    canCreateProjectFolder(): boolean {
        return this.names.includes('CREATE_PROJECT_FOLDER');
    }

    canCreateProductCategory(): boolean {
        return this.names.includes('CREATE_PRODUCT_CATEGORY');
    }

    canCreateProspect(): boolean {
        return this.names.includes('CREATE_PROSPECT');
    }

    canCreateProspectTask(): boolean {
        return this.names.includes('CREATE_PROSPECT_TASK');
    }

    canCreateUser(): boolean {
        return this.names.includes('CREATE_USER');
    }

    canCreateTask(): boolean {
        return this.names.includes('CREATE_TASK');
    }

    canProjectFolder(): boolean {
        return this.names.includes('CREATE_PROJECT_FOLDER');
    }

    // ==================== DELETE ========================== //

    canDeleteContact(): boolean {
        return this.names.includes('DELETE_CONTACT');
    }

    canDeleteCustomer(): boolean {
        return this.names.includes('DELETE_CUSTOMER');
    }

    canDeleteDevis(): boolean {
        return this.names.includes('DELETE_DEVIS');
    }

    canDeleteFile(): boolean {
        return this.names.includes('DELETE_FILE');
    }

    canDeleteInvoice(): boolean {
        return this.names.includes('DELETE_INVOICE');
    }

    canDeleteInvoicePayment(): boolean {
        return this.names.includes('DELETE_INVOICE_PAYMENT');
    }

    canDeleteProjectFolder(): boolean {
        return this.names.includes('DELETE_PROJECT_FOLDER');
    }

    canDeleteProductCategory(): boolean {
        return this.names.includes('DELETE_PRODUCT_CATEGORY');
    }

    canDeleteProspect(): boolean {
        return this.names.includes('DELETE_PROSPECT');
    }

    canDeleteTask(): boolean {
        return this.names.includes('DELETE_TASK');
    }

    // ==================== DELETE ========================== //

    canDisplayDetailsDevis(): boolean {
        return this.names.includes('DISPLAY_DETAILS_DEVIS');
    }

    canDisplayDetailsInvoice(): boolean {
        return this.names.includes('DISPLAY_DETAILS_INVOICE');
    }

    canDisplayDetailsUser(): boolean {
        return this.names.includes('DISPLAY_DETAILS_USER');
    }

    // ==================== DUPLICATE ========================== //

    canDuplicateDevis(): boolean {
        return this.names.includes('DUPLICATE_DEVIS');
    }

    // ==================== DOWNLOAD ========================== //

    canDownloadFile(): boolean {
        return this.names.includes('DOWNLOAD_FILE');
    }

    // ==================== LOCK ========================== //

    canLockUser(): boolean {
        return this.names.includes('LOCK_USER');
    }

    // ==================== PAID ========================== //

    canPayInvoice(): boolean {
        return this.names.includes('PAY_INVOICE');
    }

    // ==================== PRINT ========================== //

    canPrintDevis(): boolean {
        return this.names.includes('PAY_INVOICE');
    }

    canPrintInvoice(): boolean {
        return this.names.includes('PRINT_INVOICE');
    }

    // ==================== READ ========================== //

    canReadAllCommercialStat(): boolean {
        return this.names.includes('READ_ALL_COMMERCIAL_STAT');
    }

    canReadAllCustomer(): boolean {
        return this.names.includes('READ_ALL_CUSTOMER');
    }

    canReadAllDevis(): boolean {
        return this.names.includes('READ_ALL_DEVIS');
    }

    canReadAllFile(): boolean {
        return this.names.includes('READ_ALL_FILE');
    }

    canReadAllInvoice(): boolean {
        return this.names.includes('READ_ALL_INVOICE');
    }

    canReadAllInvoicePayment(): boolean {
        return this.names.includes('READ_ALL_INVOICE_PAYMENT');
    }

    canReadAllProduct(): boolean {
        return this.names.includes('READ_ALL_PRODUCT');
    }

    canReadAllProject(): boolean {
        return this.names.includes('READ_ALL_PROJECT');
    }

    canReadAllProjectFolder(): boolean {
        return this.names.includes('READ_ALL_PROJECT_FOLDER');
    }

    canReadAllProspect(): boolean {
        return this.names.includes('READ_ALL_PROSPECT');
    }

    canReadAllProspectTask(): boolean {
        return this.names.includes('READ_ALL_PROSPECT_TASK');
    }

    canReadAllRole(): boolean {
        return this.names.includes('READ_ALL_ROLE');
    }

    canReadAllTask(): boolean {
        return this.names.includes('READ_ALL_TASK');
    }

    canReadAllUser(): boolean {
        return this.names.includes('READ_ALL_USER');
    }

    canReadAllUserTask(): boolean {
        return this.names.includes('READ_ALL_USER_TASK');
    }

    // ==================== SEE ========================== //

    canSeeAdminMenu(): boolean {
        return this.names.includes('SEE_ADMIN_MENU');
    }

    canSeeContactMenu(): boolean {
        return this.names.includes('SEE_CONTACT_MENU');
    }

    canSeeCrmMenu(): boolean {
        return this.names.includes('SEE_CRM_MENU');
    }

    canSeeCustomerMenu(): boolean {
        return this.names.includes('SEE_CUSTOMER_MENU');
    }

    canSeeDashCrm(): boolean {
        return this.names.includes('SEE_DASH_CRM');
    }

    canSeeDashCommercial(): boolean {
        return this.names.includes('SEE_DASH_COMMERCIAL');
    }

    canSeeDashInvoice(): boolean {
        return this.names.includes('SEE_DASH_INVOICE');
    }

    canSeeDashMenu(): boolean {
        return this.names.includes('SEE_DASH_MENU');
    }

    canSeeDashTask(): boolean {
        return this.names.includes('SEE_DASH_TASK');
    }

    canSeeDevisMenu(): boolean {
        return this.names.includes('SEE_DEVIS_MENU');
    }

    canSeeFileMenu(): boolean {
        return this.names.includes('SEE_FILE_MENU');
    }

    canSeeInvoiceMenu(): boolean {
        return this.names.includes('SEE_INVOICE_MENU');
    }

    canSeeInvoicingMenu(): boolean {
        return this.names.includes('SEE_INVOICING_MENU');
    }

    canSeePaymentMenu(): boolean {
        return this.names.includes('SEE_PAYMENT_MENU');
    }

    canSeeProductMenu(): boolean {
        return this.names.includes('SEE_PRODUCT_MENU');
    }

    canSeeCategoryMenu(): boolean {
        return this.names.includes('SEE_CATEGORY_MENU');
    }

    canSeeProjectFolderMenu(): boolean {
        return this.names.includes('SEE_PROJECT_FOLDER_MENU');
    }

    canSeeProspectMenu(): boolean {
        return this.names.includes('SEE_PROSPECT_MENU');
    }

    canSeeRoleMenu(): boolean {
        return this.names.includes('SEE_ROLE_MENU');
    }

    canSeeTaskMenu(): boolean {
        return this.names.includes('SEE_TASK_MENU');
    }

    canSeeUserMenu(): boolean {
        return this.names.includes('SEE_USER_MENU');
    }


    // ==================== UNLOCK ========================== //

    canUnlockUser(): boolean {
        return this.names.includes('UNLOCK_USER');
    }

    // ==================== UPDATE ========================== //

    canUpdateContact(): boolean {
        return this.names.includes('UPDATE_CONTACT');
    }

    canUpdateCustomer(): boolean {
        return this.names.includes('UPDATE_CUSTOMER');
    }

    canUpdateDevis(): boolean {
        return this.names.includes('UPDATE_DEVIS');
    }

    canUpdateInvoice(): boolean {
        return this.names.includes('UPDATE_INVOICE');
    }

    canUpdateProduct(): boolean {
        return this.names.includes('UPDATE_PRODUCT');
    }

    canUpdateProjectFolder(): boolean {
        return this.names.includes('UPDATE_PROJECT_FOLDER');
    }

    canUpdateProductCategory(): boolean {
        return this.names.includes('UPDATE_PRODUCT_CATEGORY');
    }

    canUpdateProspect(): boolean {
        return this.names.includes('UPDATE_PROSPECT');
    }

    canUpdateRole(): boolean {
        return this.names.includes('UPDATE_ROLE');
    }

    canUpdateUser(): boolean {
        return this.names.includes('UPDATE_USER');
    }

    canUpdateTask(): boolean {
        return this.names.includes('UPDATE_TASK');
    }

    // ==================== VALIDATE ========================== //

    canValidateInvoice(): boolean {
        return this.names.includes('VALIDATE_INVOICE');
    }

    canValidateInvoicePayment(): boolean {
        return this.names.includes('VALIDATE_INVOICE_PAYMENT');
    }

}