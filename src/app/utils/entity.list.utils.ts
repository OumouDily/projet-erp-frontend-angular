import {Injectable} from '@angular/core';
import {User} from '../data/models/user.model';
import {Customer} from '../data/models/customer.model';
import {Contact} from '../data/models/contact.model';
import {DEVIS_STATE} from '../data/enums/enums';

/**
 * @author meididiallo
 */
@Injectable()
export class EntityListUtils {

    searchUser(query: string, users: User[]): User[] {
        return users.filter((user) => ((user.username.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1) || (user.telephone.indexOf(query.trim().toLowerCase()) > -1) ||
            (user.firstname.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1)) || (user.lastname.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
    }

    searchCustomer(query: string, customers: Customer[]): Customer[] {
        return customers.filter((customer) => ((customer.name.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1)));
    }

    searchYears(query: string, years: string[]): string[] {
        return years.filter((year) => ((year.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)));
    }

    searchState(query: string, deviss: DEVIS_STATE[]): DEVIS_STATE[] {
        return deviss.filter((state) => ((state.toString().trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1)));
    }

    searchContact(query: string, contacts: Contact[]): Contact[] {
        return contacts.filter((contact) => ((contact.name.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
            || (contact.phoneOne.indexOf(query.trim().toLowerCase()) > -1)));
    }

}