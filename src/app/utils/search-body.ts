import {Injectable} from '@angular/core';
import {Deserializable} from '../data/models/deserializable.model';
import {DEVIS_STATE, INVOICE_STATE, TASK_PRIORITY, TASK_STATE} from '../data/enums/enums';

@Injectable()
export class SearchBody implements Deserializable {

    userId?: number;
    createById?: number;
    assignToId?: number;
    customerId?: number;
    contactId?: number;
    page?: number;
    taskState?: TASK_STATE;
    taskPriority?: TASK_PRIORITY;
    devisState?: DEVIS_STATE;
    invoiceState?: INVOICE_STATE;
    startDate?: Date;
    endDate?: Date;
    query?: string;
    enabled?: boolean;
    year?: string;

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
