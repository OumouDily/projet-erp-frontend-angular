/**
 * @author meididiallo
 */
import {Injectable} from '@angular/core';
import {FuseNavigation, FuseNavigationItem} from '../../@fuse/types';
import {RoleUtils} from './role-utils';

@Injectable({
    providedIn: 'root'
})
export class NavItemUtils {

    roleUtils: RoleUtils;
    navigation: FuseNavigation[];
    shortcutItems: any[];

    constructor() {
        this.navigation = [];
        this.shortcutItems = [];
    }

    getNavigations(): FuseNavigation[] {

        this.navigation = [];

        this.roleUtils = new RoleUtils();

        let baseMenuChildren: FuseNavigationItem[] = [];


        // ================ DASH ================================== //
        let dashMenuItem: FuseNavigation = {
            id: 'main-dashboard',
            title: 'Tableau de bord',
            type: 'item',
            icon: 'dashboard',
            url: '/views/dashboards/main'
        };
        if (this.roleUtils.canSeeDashMenu()) {
            baseMenuChildren.push(dashMenuItem);
        }
        // ========================================================== //


        // ================ CRM =================================== //
        let crmMenuChildren: FuseNavigationItem[] = [];

        // ========= PROSPECT ========= //
        let prospectMenuItem: FuseNavigation = {
            id: 'prospects',
            title: 'Prospects',
            icon: 'people',
            type: 'item',
            url: '/views/crm/prospects',
            exactMatch: true
        };
        if (this.roleUtils.canSeeProspectMenu()) {
            crmMenuChildren.push(prospectMenuItem);
        }
        // ========= CUSTOMER ========= //
        let customerMenuItem: FuseNavigation = {
            id: 'customers',
            title: 'Clients',
            icon: 'people',
            type: 'item',
            url: '/views/crm/customers',
            exactMatch: true
        };
        if (this.roleUtils.canSeeCustomerMenu()) {
            crmMenuChildren.push(customerMenuItem);
        }
        // ========= PROJECT FOLDER ========= //
        let projectFolderMenuItem: FuseNavigation = {
            id: 'project-folder',
            title: 'Dossier projet',
            icon: 'folder',
            type: 'item',
            url: '/views/crm/project-folder',
            exactMatch: true
        };
        if (this.roleUtils.canSeeProjectFolderMenu()) {
            crmMenuChildren.push(projectFolderMenuItem);
        }


        // ========= CONTACT ========= //
        let contactMenuItem: FuseNavigation = {
            id: 'contacts',
            title: 'Contacts',
            type: 'item',
            icon: 'account_box',
            url: '/views/crm/contacts'
        };
        if (this.roleUtils.canSeeContactMenu()) {
            crmMenuChildren.push(contactMenuItem);
        }
        // ========= CRM ========= //
        let crmMenuItem: FuseNavigation = {
            id: 'crm',
            title: 'CRM',
            type: 'collapsable',
            icon: 'settings',
            children: crmMenuChildren
        };
        if (this.roleUtils.canSeeCrmMenu() || this.roleUtils.canSeeProspectMenu() || this.roleUtils.canSeeProspectMenu()) {
            baseMenuChildren.push(crmMenuItem);
        }
        // ========================================================== //

        // ================ INVOICING =================================== //
        let invoicingMenuChildren: FuseNavigationItem[] = [];

        // ========= DEVIS ========= //
        let devisMenuItem: FuseNavigation = {
            id: 'devis',
            title: 'Devis',
            icon: 'description',
            type: 'item',
            url: '/views/invoice/devis',
            exactMatch: true
        };
        if (this.roleUtils.canSeeDevisMenu()) {
            invoicingMenuChildren.push(devisMenuItem);
        }

        // ========= INVOICE ========= //
        let invoiceMenuItem: FuseNavigation = {
            id: 'factures',
            title: 'Factures',
            icon: 'receipt',
            type: 'item',
            url: '/views/invoice/factures',
            exactMatch: true
        };
        if (this.roleUtils.canSeeInvoiceMenu()) {
            invoicingMenuChildren.push(invoiceMenuItem);
        }

        // ========= PAYMENT ========= //
        let paymentMenuItem: FuseNavigation = {
            id: 'paiements',
            title: 'Paiements',
            icon: 'payment',
            type: 'item',
            url: '/views/invoice/payments',
            exactMatch: true
        };
        if (this.roleUtils.canSeePaymentMenu()) {
            invoicingMenuChildren.push(paymentMenuItem);
        }

        // ========= INVOICING ========= //
        let invoicingMenuItem: FuseNavigation = {
            id: 'invoice',
            title: 'Facturation',
            type: 'collapsable',
            icon: 'settings',
            children: invoicingMenuChildren
        };
        if (this.roleUtils.canSeeInvoicingMenu() || this.roleUtils.canSeeDevisMenu() || this.roleUtils.canSeeInvoiceMenu() || this.roleUtils.canSeePaymentMenu()) {
            baseMenuChildren.push(invoicingMenuItem);
        }
        // ========================================================== //

        // ================ TASK =================================== //
        let taskMenuItem: FuseNavigation = {
            id: 'task',
            title: 'Tâches',
            icon: 'check_box',
            type: 'item',
            url: '/views/tasks/all',
            exactMatch: true
        };
        if (this.roleUtils.canSeeTaskMenu()) {
            baseMenuChildren.push(taskMenuItem);
        }
        // ========================================================== //

        // ========= PRODUCT/SERVICES ========= //
        let productServiceMenuItem: FuseNavigation = {
            id: 'products',
            title: 'Produits / Services',
            type: 'item',
            icon: 'list',
            url: '/views/products',
            exactMatch: true
        };
        if (this.roleUtils.canSeeProductMenu()) {
            baseMenuChildren.push(productServiceMenuItem);
        }
        // ========================================================== //

        // ================ ADMIN =================================== //
        let adminMenuChildren: FuseNavigationItem[] = [];

        // ========= USER ========= //
        let userMenuItem: FuseNavigation = {
            id: 'user',
            title: 'Utilisateurs',
            type: 'item',
            icon: 'people',
            url: '/views/admin/users',
            exactMatch: true
        };
        if (this.roleUtils.canSeeUserMenu()) {
            adminMenuChildren.push(userMenuItem);
        }

        // ========= ROLE ========= //
        let roleMenuItem: FuseNavigation = {
            id: 'role',
            title: 'Roles',
            icon: 'settings',
            type: 'item',
            url: '/views/admin/roles',
            exactMatch: true
        };
        if (this.roleUtils.canSeeRoleMenu()) {
            adminMenuChildren.push(roleMenuItem);
        }

        // ========= BRANCH ACTIVITY ========= //
        let branchActivityMenuItem: FuseNavigation = {
            id: 'sector-activity',
            title: 'Secteur d\'activité',
            icon: 'settings',
            type: 'item',
            url: '/views/setting/sector-activities',
            exactMatch: true
        };
        adminMenuChildren.push(branchActivityMenuItem);

        // ========= TAX ========= //
        let taxMenuItem: FuseNavigation = {
            id: 'taxe',
            title: 'Taxes',
            icon: 'settings',
            type: 'item',
            url: '/views/setting/taxes',
            exactMatch: true
        };
        adminMenuChildren.push(taxMenuItem);

        // ========= QUOTE TRACE ========= //
        let quoteTraceMenuItem: FuseNavigation = {
            id: 'quote-trace',
            title: 'Trace d\'un devis',
            icon: 'settings',
            type: 'item',
            url: '/views/setting/quote-traces',
            exactMatch: true
        };
        adminMenuChildren.push(quoteTraceMenuItem);

        // ========= ADMIN ========= //
        let adminMenuItem: FuseNavigation = {
            id: 'admin',
            title: 'Admin',
            type: 'collapsable',
            icon: 'settings',
            children: adminMenuChildren
        };
        if (this.roleUtils.canSeeUserMenu() || this.roleUtils.canSeeRoleMenu()) {
            baseMenuChildren.push(adminMenuItem);
        }
        // ========================================================== //

        // ================ MAIN MENU =================================== //
        let baseMenuItem: FuseNavigation = {
            id: 'menu',
            title: 'Menu',
            type: 'group',
            icon: 'menu',
            children: baseMenuChildren
        };

        this.navigation.push(baseMenuItem);

        return this.navigation;
    }

    getShortcutItems(): any[] {

        this.shortcutItems = [];

        this.roleUtils = new RoleUtils();

        // ========= TASK ========= //
        let taskShortcutItem: any = {
            'title': 'Mes tâches',
            'icon': 'check_box',
            'type': 'item',
            'url': '/views/tasks'
        };
        if (this.roleUtils.canSeeTaskMenu()) {
            this.shortcutItems.push(taskShortcutItem);
        }
        // ========= DEVIS ========= //
        let devisShortcutItem: any = {
            'title': 'Devis',
            'icon': 'description',
            'type': 'item',
            'url': '/views/invoice/devis'
        };
        if (this.roleUtils.canSeeTaskMenu()) {
            this.shortcutItems.push(devisShortcutItem);
        }
        // ========= INVOICE ========= //
        let invoiceShortcutItem: any = {
            'title': 'Factures',
            'icon': 'receipt',
            'type': 'item',
            'url': '/views/invoice/factures'
        };
        if (this.roleUtils.canSeeTaskMenu()) {
            this.shortcutItems.push(invoiceShortcutItem);
        }

        return this.shortcutItems;
    }

}