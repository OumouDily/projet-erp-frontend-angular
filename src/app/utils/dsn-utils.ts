/**
 * @author meididiallo
 */
import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {AuthBody} from './auth-body';
import {User} from '../data/models/user.model';

@Injectable()
export class DsnUtils {

    now = new Date();

    constructor() {
    }

    startDate() {
        return {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() - 7};
    }

    endDate() {
        return {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
    }

    formatDate(date) {
        // let d = new Date(Date.parse(date));
        return (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + ((date.getMonth() + 1) < 10 ? '0' + ((date.getMonth() + 1)) : ((date.getMonth() + 1))) + '/' + date.getFullYear();
    }

    formatStringtoDate(date) {
        const d = new Date(Date.parse(date));

        return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + ((d.getMonth() + 1)) : ((d.getMonth() + 1))) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
    }

    formatDateYMD(date) {
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }

    formatDateTo(date) {
        return (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + ((date.getMonth() + 1) < 10 ? '0' + ((date.getMonth() + 1)) : ((date.getMonth() + 1))) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }

    formatDateToString(date) {
        return date.year + '-' + (parseInt(date.month, null) < 10 ? '0' + parseInt(date.month, null) : parseInt(date.month, null)) + '-' + (parseInt(date.day, null) < 10 ? '0' + parseInt(date.day, null) : parseInt(date.day, null));
    }

    addDays(date, numberOfDay) {
        return new Date(date.getTime() + numberOfDay * 24 * 60 * 60 * 1000);
    }

    subDays(date, numberOfDay) {
        return new Date(date.getTime() - numberOfDay * 24 * 60 * 60 * 1000);
    }

    httpHeaders() {
        const token: string = this.getToken();
        let headers = new HttpHeaders();

        headers.set('Access-Control-Allow-Origin', '*');
        headers.set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, DELETE');
        headers.set('Access-Control-Allow-Headers', 'Accept, Authorization, X-Requested-With, remember-me, Content-Type');

        headers.set('Accept', 'application/json');
        headers.set('Content-Type', 'application/json');
        if (token) { // token is present
            headers.set('Authorization', 'Bearer ' + token);
        }
        const httpOptions = {
            headers: headers
        };
        return httpOptions;
    }

    getToken(): string {
        let authBody: AuthBody = this.getAuthBody();
        if (authBody) {
            return authBody.token;
        } else {
            return null;
        }
    }

    getAppUser(): User {
        let authBody: AuthBody = this.getAuthBody();
        if (authBody) {
            return authBody.user;
        } else {
            return null;
        }
    }

    getAuthBody(): AuthBody {
        if (!localStorage.getItem('app-token')) {
            return null;
        } else {
            return JSON.parse(atob(localStorage.getItem('app-token')));
        }
    }

    fomatDateFromJson(date) {
        let dateSplit;
        if (typeof date === 'string') {
            dateSplit = date.replace('T', ' ').replace('.000Z', '');
            dateSplit = new Date(dateSplit);
        } else {
            dateSplit = date;
        }
        return dateSplit;
    }

    removeLastChar(numero) { // une function qui remplace le derniere element d'une chaine de caractere en vide
        let numeroToString = numero.toString();
        let i: number;
        for (i = 0; i <= numeroToString.length; i++) {
            if (i === numeroToString.length - 1) {
                numeroToString = numeroToString.replace(numeroToString[i], '');
                return numeroToString;
            }
        }
    }

    getAmountTva(amountHT: number, tax: number): number {
        return ((1 + (tax * 0.01)) * amountHT);
    }

    getAmountByPercentage(total: number, percentage: number): number {
        return (total * percentage) / 100;
    }

    getPercentage(value: number, total: number): number {
        if (total == 0) {
            return 0;
        } else {
            return (value / total) * 100;
        }
    }

    dateToString(date): Date {
        let sdate = date.year + '-' +
            '' + (parseInt(date.month, null) < 10 ? '0' + parseInt(date.month, null) : parseInt(date.month, null)) + '-' +
            '' + (parseInt(date.day, null) < 10 ? '0' + parseInt(date.day, null) : parseInt(date.day, null)) + ' ' +
            '' + (parseInt(date.getHours(), null) < 10 ? '0' + parseInt(date.getHours(), null) : parseInt(date.getHours(), null)) + ':' +
            '' + (parseInt(date.getMinutes(), null) < 10 ? '0' + parseInt(date.getMinutes(), null) : parseInt(date.getMinutes(), null)) + ':' +
            '' + (parseInt(date.getSeconds(), null) < 10 ? '0' + parseInt(date.getSeconds(), null) : parseInt(date.getSeconds(), null));
        return new Date(sdate);
    }

    addCommas(nStr) {
        nStr += '';
        let x = nStr.split('.');
        let x1 = x[0];
        let x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    getEnumKeys<T>(enumeration: T): string[] {
        return Object.keys(enumeration);
    }

    getEnumValues<T>(enumeration: T): Array<T[keyof T]> {
        let enumKeys = Object.keys(enumeration).map(k => enumeration[k]);

        let items = new Array<T[keyof T]>();
        for (let elem of enumKeys) {
            if (typeof (elem) === 'number') {
                items.push(elem as any);
            }
        }

        return items;
    }

    public objectComparison = function (option, value): boolean {
        return option.id === value.id;
    };

    round(value, precision?): number {
        let multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }

    titleCaseWord(word: string): string {
        if (!word) {
            return word;
        }
        return word[0].toUpperCase() + word.substr(1).toLowerCase();
    }

    getRoleNames(): string[] {
        let names = [];
        let user: User = this.getAppUser();
        if (user) {
            user.roles.forEach(role => {
                names.push(role.name);
            });
        }
        return names;
    }

    getPrivilegeNames(): string[] {
        let names = [];
        let user: User = this.getAppUser();
        if (user) {
            user.roles.forEach(role => {
                role.privileges.forEach(privilege => {
                    names.push(privilege.name);
                });
            });
        }
        return names;
    }

    getLastYears(numberOfYear: number): string[] {
        let year = new Date().getFullYear();
        let range = [];
        range.push(year);
        for (let i = 1; i < numberOfYear; i++) {
            range.push((year - i) + '');
        }
        return range;
    }
}