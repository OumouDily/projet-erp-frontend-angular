import {Injectable} from '@angular/core';
import {Deserializable} from '../../data/models/deserializable.model';
import {GraphBody} from './graph-body';

@Injectable()
export class InvoiceDashBody implements Deserializable {

    sumAmountPaid?: number;
    sumStayToPay?: number;
    totalInvoice?: number;
    totalDevisOpen?: number;
    totalInvoiceCount?: number;
    sumAmountPaidCount?: number;
    sumStayToPayCount?: number;
    totalDevisOpenCount?: number;
    invoiceGraphBodies?: GraphBody[];

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
