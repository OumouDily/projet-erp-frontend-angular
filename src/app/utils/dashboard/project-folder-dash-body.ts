import {Injectable} from '@angular/core';
import {Deserializable} from '../../data/models/deserializable.model';
import {GraphBody} from "./graph-body";

@Injectable()
export class ProjectFolderDashBody implements Deserializable {

   /* notStartedCount?: number;
    inProgressCount?: number;
    pendingCount?: number;
    doneCount?: number;
    sumNotStarted?: number;
    sumInProgress?: number;
    sumPending?: number;
    sumDone?: number;
    projectGraphBodies?: GraphBody[];*/

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
