import {Injectable} from '@angular/core';
import {Deserializable} from '../../data/models/deserializable.model';
import {GraphBody} from './graph-body';
import {Customer} from '../../data/models/customer.model';

@Injectable()
export class CrmDashBody implements Deserializable {

    prospectCount?: number;
    customerCount?: number;
    newProspectCount?: number;
    notContactedProspectCount?: number;
    customers?: Customer[];
    customerGraphBodies?: GraphBody[];


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
