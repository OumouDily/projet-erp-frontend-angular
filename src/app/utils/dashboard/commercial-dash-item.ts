import {Injectable} from '@angular/core';
import {Deserializable} from '../../data/models/deserializable.model';
import {User} from '../../data/models/user.model';
import {InvoiceDashBody} from './invoice-dash-body';
import {CrmDashBody} from './crm-dash-body';
import {ProjectFolderDashBody} from './project-folder-dash-body';

@Injectable()
export class CommercialDashItem implements Deserializable {

    achievedTurnover?: number;
    pendingTurnover?: number;
    objectiveTurnover?: number;
    forecastTurnover?: number;
    commercial?: User;
    invoiceDashBody?: InvoiceDashBody;
    crmDashBody?: CrmDashBody;
    projectFolderDashBody?: ProjectFolderDashBody;


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
