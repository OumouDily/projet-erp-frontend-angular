import {Injectable} from '@angular/core';
import {Deserializable} from '../../data/models/deserializable.model';
import {CommercialDashItem} from './commercial-dash-item';

@Injectable()
export class CommercialDashBody implements Deserializable {

    achievedTurnover?: number;
    pendingTurnover?: number;
    objectiveTurnover?: number;
    forecastTurnover?: number;
    commercialDashItems?: CommercialDashItem[];


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: this): boolean {
        return true;
    }

}
