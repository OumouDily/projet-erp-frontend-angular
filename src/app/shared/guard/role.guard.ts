import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {RoleUtils} from '../../utils/role-utils';

@Injectable({
    providedIn: 'root'
})
export class AdminMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeAdminMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }

}

@Injectable({
    providedIn: 'root'
})
export class ContactMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeContactMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }

}

@Injectable({
    providedIn: 'root'
})
export class CrmMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeCrmMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }

}

@Injectable({
    providedIn: 'root'
})
export class CustomerMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeCustomerMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class CustomerCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateCustomer()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class CustomerUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateCustomer()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DashMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeDashMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeDevisMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateDevis()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateDevis()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisDuplicateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canDuplicateDevis()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisDetailsGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canDisplayDetailsDevis()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class DevisPrintGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canPrintDevis()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoiceMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeInvoiceMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoiceCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateInvoice()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoiceUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateInvoice()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoiceDetailsGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canDisplayDetailsInvoice()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoicePrintGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canPrintInvoice()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class InvoicingMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeInvoicingMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class PaymentMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeePaymentMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProductMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeProductMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProductCategoryMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeCategoryMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProjectFolderMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeProjectFolderMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}


@Injectable({
    providedIn: 'root'
})
export class ProspectMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeProspectMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProspectCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateProspect()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProjectFolderCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canProjectFolder()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProspectUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateProspect()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProjectFolderUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateProspect()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class RoleMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeRoleMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class RoleUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateRole()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class TaskMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeTaskMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class TaskCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateTask()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class TaskUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateTask()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class UserMenuGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canSeeUserMenu()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class UserCreateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canCreateUser()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class UserUpdateGuard implements CanActivate {
    roleUtils: RoleUtils;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.roleUtils = new RoleUtils();
        if (this.roleUtils.canUpdateUser()) {
            return true;
        } else {
            this.router.navigate(['/errors/access-error']);
        }
    }
}