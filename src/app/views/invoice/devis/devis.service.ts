import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Devis} from '../../../data/models/devis.model';
import {environment} from '../../../../environments/environment';
import {ResponseBody} from '../../../utils/response-body';
import {User} from '../../../data/models/user.model';
import {Customer} from '../../../data/models/customer.model';
import {UserService} from '../../../services/user.service';
import {SearchBody} from '../../../utils/search-body';
import {DEVIS_STATE} from '../../../data/enums/enums';
import {CustomersService} from '../../crm/customers/customers.service';

@Injectable({
    providedIn: 'root'
})
export class DevisService implements Resolve<any> {

    page: number = 0;

    onDevisChanged: BehaviorSubject<any>;

    onStatesChanged: BehaviorSubject<any>;
    onUsersChanged: BehaviorSubject<any>;
    onCustomersChanged: BehaviorSubject<any>;


    devis: Devis[] = [];
    users: User[];
    customers: Customer[];
    states = [];


    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService,
        private _customerService: CustomersService,
    ) {
        this.onDevisChanged = new BehaviorSubject({});
        this.onUsersChanged = new BehaviorSubject({});
        this.onCustomersChanged = new BehaviorSubject({});
        this.onStatesChanged = new BehaviorSubject({});
        this.httpOptions = new DsnUtils().httpHeaders();
        this.serviceURL = environment.serviceUrl + '/devis';
    }


    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDevis(),
                this.getUsers(),
                this.getCustomers(),
                this.getStates()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getDevis(searchBody?: SearchBody): Promise<any> {
        if (searchBody == null) {
            searchBody = new SearchBody();
        }
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/find-all', searchBody, this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.devis = res['response'];
                        this.onDevisChanged.next(this.devis);
                        resolve(res['response']);
                    } else {
                        // console.log('error : ' + res['status']);
                    }
                }, reject);
        });
    }

    getDevisById(id: number): Observable<HttpEvent<Devis>> {
        return this._httpClient.get<Devis>(this.serviceURL + '/' + id, this.httpOptions);
    }

    getByGenerated(generated: boolean): Observable<HttpEvent<ResponseBody>> {
        return this._httpClient.get<ResponseBody>(this.serviceURL + '/getByGenereted/' + generated, this.httpOptions);
    }

    cancelDevis(devis: Devis) {
        return this._httpClient.put(this.serviceURL + '/cancel', devis, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {

            this._userService.list()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.users = ret['response'];
                        this.onUsersChanged.next(this.users);
                    }
                    resolve(this.users);
                });
        });
    }

    getCustomers(): any {
        return new Promise((resolve, reject) => {

            this._customerService.findAll()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.customers = ret['response'];
                        this.onCustomersChanged.next(this.customers);
                    }
                    resolve(this.customers);
                });
        });
    }

    getStates(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.states = Object.keys(DEVIS_STATE);
            this.onStatesChanged.next(this.states);
            resolve(this.states);
        });
    }

}
