import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {takeUntil} from 'rxjs/internal/operators';
import {DevisService} from './devis.service';
import {InvoiceFormDialogComponent} from '../invoice-form/invoice-form.component';
import {Devis} from '../../../data/models/devis.model';
import {FormControl} from '@angular/forms';
import {DEVIS_STATE} from '../../../data/enums/enums';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DsnUtils} from '../../../utils/dsn-utils';
import {RoleUtils} from '../../../utils/role-utils';
import {User} from '../../../data/models/user.model';
import {DataSource} from '@angular/cdk/collections';
import {FuseUtils} from '../../../../@fuse/utils';
import {Customer} from '../../../data/models/customer.model';
import {SearchBody} from '../../../utils/search-body';
import {EntityListUtils} from '../../../utils/entity.list.utils';

@Component({
    selector: 'invoice-devis',
    templateUrl: './devis.component.html',
    styleUrls: ['./devis.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class DevisComponent implements OnInit, OnDestroy {

    roleUtils = new RoleUtils();
    dsnUtils = new DsnUtils();
    currentUser: User;
    entityListUtils = new EntityListUtils();

    totalElements: number;
    devis: Devis[] | null;

    dataSource: FilesDataSource | null;
    displayedColumns = ['customer', 'agent', 'number', 'devisDate', 'taxe', 'total', 'amountInvoiced', 'privateComment', 'state', 'createBy', 'updateBy', 'buttons'];

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    customers: Customer[];
    selectedCustomer: Customer;
    filteredCustomers: Customer[];

    states: DEVIS_STATE[];
    selectedState: DEVIS_STATE;
    filteredStates: DEVIS_STATE[];
    devisState = DEVIS_STATE;

    searchBody = new SearchBody();

    searchInput: FormControl;


    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    dialogRef: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _devisService: DevisService,
        public _matDialog: MatDialog,
        private toast: ToastrService,
        private _router: Router
    ) {
        // Set the private defaults
        this.searchInput = new FormControl();
        this._unsubscribeAll = new Subject();

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.currentUser = this.dsnUtils.getAppUser();
        this.dataSource = new FilesDataSource(this._devisService, this.paginator, this.sort);

        this._devisService.onDevisChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedTasks => {
                this.dataSource = new FilesDataSource(this._devisService, this.paginator, this.sort);
            });

        this.getAllDevis();


        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });

        this._devisService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });


        this._devisService.onCustomersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customers => {
                this.customers = customers;
                this.filteredCustomers = this.customers;
            });

        this._devisService.onStatesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(states => {
                this.states = states;
                this.filteredStates = this.states;
            });
    }


    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    newInvoice(devis) {
        this.dialogRef = this._matDialog.open(InvoiceFormDialogComponent, {
            panelClass: 'invoice-form-dialog',
            data: {
                devis: devis,
                action: 'new'
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
            });
    }

    getAllDevis() {
        this._devisService.getDevis(null);
    }

    onPageChanged(e) {
        this._devisService.page = e.pageIndex;
        this.getAllDevis();
    }

    cancel(devis: Devis) {
        devis.cancelBy = this.dsnUtils.getAppUser();
        devis.cancelDate = new Date();
        this._devisService.cancelDevis(devis).subscribe(data => {
            if (data['status'] === 'OK') {
                this.toast.success(data['message']);
                this.getAllDevis();
            } else {
                this.toast.error(data['message']);
            }
        }, error => console.log(error));
    }

    delete(devis): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimer le devis ' + devis.number + ' ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._devisService.delete(devis.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);
                        this.getAllDevis();
                    } else {
                        this.toast.error(ret['message']);
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    onSearch(): void {
        this._devisService.getDevis(this.searchBody);
    }

    onCustomerSelected(customer?, user?): void {
        this.selectedCustomer = customer;
        this.searchBody.customerId = customer ? customer.id : customer;
        this.onSearch();
    }

    onUserSelected(user?): void {
        this.selectedUser = user;
        this.searchBody.userId = user ? user.id : user;
        this.onSearch();
    }

    onStateSelected(state?): void {
        this.selectedState = state;
        this.searchBody.devisState = state;
        this.onSearch();
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'customer') {
            this.filteredCustomers = this.entityListUtils.searchCustomer(query, this.customers);
        } else if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }

}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {DevisService} _devisService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _devisService: DevisService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._devisService.devis;
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._devisService.onDevisChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._devisService.devis.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'customer':
                    [propertyA, propertyB] = [a.customer.id, b.customer.id];
                    break;
                case 'agent':
                    [propertyA, propertyB] = [a.agent.id, b.agent.id];
                    break;
                case 'number':
                    [propertyA, propertyB] = [a.number, b.number];
                    break;
                case 'devisDate':
                    [propertyA, propertyB] = [a.devisDate, b.devisDate];

                    break;
                case 'taxAmount':
                    [propertyA, propertyB] = [a.taxAmount, b.taxAmount];
                    break;
                case 'taxValue':
                    [propertyA, propertyB] = [a.taxValue, b.taxValue];
                    break;
                case 'total':
                    [propertyA, propertyB] = [a.total, b.total];
                    break;
                case 'amountInvoiced':
                    [propertyA, propertyB] = [a.amountInvoiced, b.amountInvoiced];
                    break;
                case 'privateComment':
                    [propertyA, propertyB] = [a.privateComment, b.privateComment];
                    break;
                case 'state':
                    [propertyA, propertyB] = [a.state, b.state];
                    break;
                case 'createBy':
                    [propertyA, propertyB] = [a.createBy, b.createBy];
                    break;
                case 'updateBy':
                    [propertyA, propertyB] = [a.updateBy, b.updateBy];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

