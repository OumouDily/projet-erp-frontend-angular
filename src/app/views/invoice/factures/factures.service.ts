import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Invoice} from '../../../data/models/invoice.model';
import {environment} from '../../../../environments/environment';
import {ResponseBody} from '../../../utils/response-body';
import {ToastrService} from 'ngx-toastr';
import {INVOICE_STATE} from '../../../data/enums/enums';
import {User} from '../../../data/models/user.model';
import {Customer} from '../../../data/models/customer.model';
import {CustomersService} from '../../crm/customers/customers.service';
import {UserService} from '../../../services/user.service';
import {SearchBody} from '../../../utils/search-body';

@Injectable({
    providedIn: 'root'
})
export class FacturesService implements Resolve<any> {

    page: number = 0;

    invoices: Invoice[] = [];
    users: User[];
    customers: Customer[];
    states = [];

    onInvoicesChanged: BehaviorSubject<any>;
    onStatesChanged: BehaviorSubject<any>;
    onUsersChanged: BehaviorSubject<any>;
    onCustomersChanged: BehaviorSubject<any>;

    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _toastr: ToastrService,
        private _customerService: CustomersService,
        private _userService: UserService
    ) {
        // Set the defaults
        this.onInvoicesChanged = new BehaviorSubject({});
        this.onUsersChanged = new BehaviorSubject({});
        this.onCustomersChanged = new BehaviorSubject({});
        this.onStatesChanged = new BehaviorSubject({});

        this.httpOptions = new DsnUtils().httpHeaders();
        this.serviceURL = environment.serviceUrl + '/invoices';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.findAllInvoices(),
                this.getUsers(),
                this.getCustomers(),
                this.getStates()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }


    findAllInvoices() {
        return this.getAllInvoices(null);
    }

    getAllInvoices(searchBody?: SearchBody): Promise<any> {
        if (searchBody == null) {
            searchBody = new SearchBody();
            searchBody.page = this.page;
        }
        return new Promise((resolve, reject) => {
            this.getList(searchBody).subscribe((res: any) => {
                this.invoices = res['content'];
                this.onInvoicesChanged.next(this.invoices);
                resolve(res['content']);
            }, reject);
        });
    }

    getList(searchBody: SearchBody): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/list', searchBody, this.httpOptions);
    }

    validateAnInvoice(invoice: Invoice) {
        return this._httpClient.put(this.serviceURL + '/an-validate', invoice, this.httpOptions);
    }

    cancelInvoice(invoice: Invoice) {
        return this._httpClient.put(this.serviceURL + '/cancel', invoice, this.httpOptions);
    }

    getById(id: number) {
        return this._httpClient.get(this.serviceURL + '/' + id, this.httpOptions);
    }

    getByPaid(paid: boolean): Observable<HttpEvent<ResponseBody>> {
        return this._httpClient.get<ResponseBody>(this.serviceURL + '/getByPaid/' + paid, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {

            this._userService.list()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.users = ret['response'];
                        this.onUsersChanged.next(this.users);
                    }
                    resolve(this.users);
                });
        });
    }

    getCustomers(): any {
        return new Promise((resolve, reject) => {

            this._customerService.findAll()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.customers = ret['response'];
                        this.onCustomersChanged.next(this.customers);
                    }
                    resolve(this.customers);
                });
        });
    }

    getStates(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.states = Object.keys(INVOICE_STATE);
            this.onStatesChanged.next(this.states);
            resolve(this.states);
        });
    }
}
