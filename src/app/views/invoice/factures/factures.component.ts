import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {takeUntil} from 'rxjs/internal/operators';
import {FacturesService} from './factures.service';
import {Invoice} from '../../../data/models/invoice.model';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {InvoicePaymentFormDialogComponent} from '../payment-form/payment-form.component';
import {INVOICE_STATE} from 'app/data/enums/enums';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {FormControl} from '@angular/forms';
import {DsnUtils} from '../../../utils/dsn-utils';
import {RoleUtils} from '../../../utils/role-utils';
import {EntityListUtils} from '../../../utils/entity.list.utils';
import {User} from '../../../data/models/user.model';
import {Customer} from '../../../data/models/customer.model';
import {SearchBody} from '../../../utils/search-body';


@Component({
    selector: 'invoice-factures',
    templateUrl: './factures.component.html',
    styleUrls: ['./factures.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class FacturesComponent implements OnInit, OnDestroy {

    roleUtils = new RoleUtils();
    dsnUtils = new DsnUtils();
    entityListUtils = new EntityListUtils();

    totalElements: number;
    invoices: Invoice[] = [];

    dataSource: FilesDataSource | null;
    displayedColumns = ['customer', 'number', 'invoiceDate', 'devisNumber', 'taxe', 'total', 'state', 'stayToPay', 'createBy', 'updateBy', 'buttons'];

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
    dialogRef: any;

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    customers: Customer[];
    selectedCustomer: Customer;
    filteredCustomers: Customer[];

    states: INVOICE_STATE[];
    selectedState: INVOICE_STATE;
    filteredStates: INVOICE_STATE[];
    invoiceState = INVOICE_STATE;

    searchBody = new SearchBody();

    searchInput: FormControl;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _inovicesService: FacturesService,
        private _toastr: ToastrService,
        private _router: Router,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this.searchInput = new FormControl();
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.dataSource = new FilesDataSource(this._inovicesService, this.paginator, this.sort);

        this._inovicesService.onInvoicesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(invoices => {
                this.dataSource = new FilesDataSource(this._inovicesService, this.paginator, this.sort);
            });

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });

        this._inovicesService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });


        this._inovicesService.onCustomersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customers => {
                this.customers = customers;
                this.filteredCustomers = this.customers;
            });

        this._inovicesService.onStatesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(states => {
                this.states = states;
                this.filteredStates = this.states;
            });

    }

    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    getAllInvoice() {
        this.searchBody.page = this._inovicesService.page;
        this._inovicesService.getList(this.searchBody).subscribe(data => {
            this.invoices = data['content'];
            this._inovicesService.onInvoicesChanged.next(data['content']);
            this.totalElements = data['totalElements'];
        }, error => console.log(error));
    }

    validation(invoice: Invoice) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de valider la facture ' + invoice.number + '?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._inovicesService.validateAnInvoice(invoice).subscribe(data => {
                    if (data['status'] === 'OK') {
                        this._toastr.success(data['message']);
                        this.getAllInvoice();
                    } else {
                        this._toastr.error(data['message']);
                    }
                }, error => console.log(error));
            }
        });
    }

    cancel(invoice: Invoice) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre d\'annuler la facture ' + invoice.number + '?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                invoice.cancelBy = this.dsnUtils.getAppUser();
                invoice.cancelDate = new Date();
                this._inovicesService.cancelInvoice(invoice).subscribe(data => {
                    if (data['status'] === 'OK') {
                        this._toastr.success(data['message']);
                        this.getAllInvoice();
                    } else {
                        this._toastr.error(data['message']);
                    }
                }, error => console.log(error));
            }
        });
    }

    issuePayment(action?: string, invoice?: Invoice) {
        this.dialogRef = this._matDialog.open(InvoicePaymentFormDialogComponent, {
            panelClass: 'payment-form-dialog',
            data: {
                invoice: invoice,
                action: action
            }
        });
    }

    onPageChanged(e) {
        this._inovicesService.page = e.pageIndex;
        this.searchBody.page = e.pageIndex;
        this._inovicesService.getAllInvoices(this.searchBody);
    }

    delete(facture): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimer la facture ' + facture.number + ' ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._inovicesService.delete(facture.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this._toastr.success(ret['message']);
                        this.getAllInvoice();
                    } else {
                        this._toastr.error(ret['message']);
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }


    onSearch() {
        this.searchBody.page = this._inovicesService.page;
        this._inovicesService.getAllInvoices(this.searchBody);
    }

    onCustomerSelected(customer?, user?): void {
        this.selectedCustomer = customer;
        this.searchBody.customerId = customer ? customer.id : customer;
        this.onSearch();
    }

    onUserSelected(user?): void {
        this.selectedUser = user;
        this.searchBody.userId = user ? user.id : user;
        this.onSearch();
    }

    onStateSelected(state?): void {
        this.selectedState = state;
        this.searchBody.invoiceState = state;
        this.onSearch();
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'customer') {
            this.filteredCustomers = this.entityListUtils.searchCustomer(query, this.customers);
        } else if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }

}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {FacturesService} _inovicesService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _inovicesService: FacturesService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._inovicesService.invoices;
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._inovicesService.onInvoicesChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._inovicesService.invoices.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'customer':
                    [propertyA, propertyB] = [a.customer.id, b.customer.id];
                    break;
                case 'number':
                    [propertyA, propertyB] = [a.number, b.number];
                    break;
                case 'invoiceDate':
                    [propertyA, propertyB] = [a.invoiceDate, b.invoiceDate];
                    break;
                case 'devisNumber':
                    [propertyA, propertyB] = [a.devis.number, b.devis.number];
                    break;
                case 'taxe':
                    [propertyA, propertyB] = [a.taxValue, b.taxValue];
                    break;
                case 'total':
                    [propertyA, propertyB] = [a.total, b.total];
                    break;
                case 'state':
                    [propertyA, propertyB] = [a.state, b.state];
                    break;
                case 'stayToPay':
                    [propertyA, propertyB] = [a.stayToPay, b.stayToPay];
                    break;
                case 'createBy':
                    [propertyA, propertyB] = [a.createBy.id, b.createBy.id];
                    break;
                case 'updateBy':
                    [propertyA, propertyB] = [a.updateBy.id, b.updateBy.id];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
