import {NgModule} from '@angular/core';
import {IConfig, NgxMaskModule} from 'ngx-mask';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
} from '@angular/material';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AgmCoreModule} from '@agm/core';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';
import {DevisComponent} from './devis/devis.component';
import {DevisService} from './devis/devis.service';
import {CrudDevisComponent} from './crud-devis/crud-devis.component';
import {CrudDevisService} from './crud-devis/crud-devis.service';
import {InvoiceFormDialogComponent} from './invoice-form/invoice-form.component';
import {DevisDetailsComponent} from './devis-details/devis-details.component';
import {DevisDetailsService} from './devis-details/devis-details.service';
import {NgxPrintModule} from 'ngx-print';
import {FacturesComponent} from './factures/factures.component';
import {FacturesService} from './factures/factures.service';
import {CrudFactureComponent} from './crud-facture/crud-facture.component';
import {CrudFactureService} from './crud-facture/crud-facture.service';
import {InvoicePrintComponent} from './print/invoice/invoice.print.component';
import {InvoicePrintService} from './print/invoice/invoice.print.service';
import {FactureDetailsComponent} from './facture-details/facture-details.component';
import {FactureDetailsService} from './facture-details/facture-details.service';
import {DevisPrintComponent} from './print/devis/devis.print.component';
import {DevisPrintService} from './print/devis/devis.print.service';
import {PhoneMaskDirective} from './phone-mask.directive';
import {PaymentsComponent} from './payments/payments.component';
import {PaymentsService} from './payments/payments.service';
import {InvoicePaymentFormDialogComponent} from './payment-form/payment-form.component';
import {InvoiceDashboardService} from './invoice-dashboard/invoice.dashboard.service';
import {InvoiceDashboardComponent} from './invoice-dashboard/invoice.dashboard.component';
import {HighchartsChartModule} from 'highcharts-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SpinnerModule} from '../../shared/modules/spinner.module';
import {
    DevisCreateGuard,
    DevisDetailsGuard,
    DevisDuplicateGuard,
    DevisMenuGuard,
    DevisPrintGuard,
    DevisUpdateGuard,
    InvoiceCreateGuard,
    InvoiceDetailsGuard,
    InvoiceMenuGuard,
    InvoicePrintGuard,
    InvoiceUpdateGuard,
    PaymentMenuGuard
} from '../../shared/guard/role.guard';
import {ConfirmDialogModule} from '../confirm-dialog/confirm-dialog.module';
import {PieComponent} from './pie/pie.component';

// @ts-ignore
export const options: Partial<IConfig> | (() => Partial<IConfig>);

const routes: Routes = [

    /*======================DEVIS===========================*/
    {
        path: 'devis',
        component: DevisComponent,
        resolve: {
            data: DevisService
        },
        canActivate: [DevisMenuGuard]
    },
    {
        path: 'devis/:id',
        component: CrudDevisComponent,
        resolve: {
            data: CrudDevisService
        },
        canActivate: [DevisCreateGuard]
    },
    {
        path: 'devis/:id/:number',
        component: CrudDevisComponent,
        resolve: {
            data: CrudDevisService
        },
        canActivate: [DevisUpdateGuard]
    },
    {
        path: 'devis/:id/canDuplicate',
        component: CrudDevisComponent,
        resolve: {
            data: CrudDevisService
        },
        canActivate: [DevisDuplicateGuard]
    },
    {
        path: 'devis-details/:id',
        component: DevisDetailsComponent,
        resolve: {
            data: DevisDetailsService
        },
        canActivate: [DevisDetailsGuard]
    },
    {
        path: 'devis-print/:id',
        component: DevisPrintComponent,
        resolve: {
            data: DevisPrintService
        },
        canActivate: [DevisPrintGuard]
    },

    /*======================INVOICE===========================*/
    {
        path: 'factures',
        component: FacturesComponent,
        resolve: {
            data: FacturesService
        },
        canActivate: [InvoiceMenuGuard]
    },
    {
        path: 'factures/:id',
        component: CrudFactureComponent,
        resolve: {
            data: CrudFactureService
        },
        canActivate: [InvoiceCreateGuard]
    },
    {
        path: 'factures/:id/:number',
        component: CrudFactureComponent,
        resolve: {
            data: CrudFactureService
        },
        canActivate: [InvoiceUpdateGuard]
    },
    {
        path: 'print/:id',
        component: InvoicePrintComponent,
        resolve: {
            data: InvoicePrintService
        },
        canActivate: [InvoicePrintGuard]
    },

    {
        path: 'print/payment/:id',
        component: InvoicePrintComponent,
        resolve: {
            data: InvoicePrintService
        },
        canActivate: [InvoicePrintGuard]
    },

    {
        path: 'details/:id',
        component: FactureDetailsComponent,
        resolve: {
            data: FactureDetailsService
        },
        canActivate: [InvoiceDetailsGuard]
    },

    /*======================PAYMENT===========================*/
    {
        path: 'payments',
        component: PaymentsComponent,
        resolve: {
            data: PaymentsService
        },
        canActivate: [PaymentMenuGuard]
    },

];

@NgModule({
    declarations: [
        PaymentsComponent,
        DevisComponent,
        DevisPrintComponent,
        InvoicePrintComponent,
        CrudDevisComponent,
        DevisDetailsComponent,
        FacturesComponent,
        CrudFactureComponent,
        FactureDetailsComponent,
        InvoiceFormDialogComponent,
        InvoicePaymentFormDialogComponent,
        InvoiceDashboardComponent,
        PieComponent,
        PhoneMaskDirective
    ],
    imports: [
        RouterModule.forChild(routes),
        NgxMaskModule.forRoot(),
        SpinnerModule,
        MatButtonModule,
        MatChipsModule,
        MatGridListModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatListModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule,
        HighchartsChartModule,
        MatCardModule,
        MatDatepickerModule,
        MatMenuModule,
        MatToolbarModule,
        NgxPrintModule,
        MatDividerModule,
        FlexLayoutModule,
        MatTooltipModule,
        MatDialogModule,
        MatCheckboxModule,
        ConfirmDialogModule
    ],
    providers: [
        InvoiceDashboardService
    ],
    entryComponents: [
        InvoiceFormDialogComponent,
        InvoicePaymentFormDialogComponent
    ]
})
export class InvoiceModule {
}
