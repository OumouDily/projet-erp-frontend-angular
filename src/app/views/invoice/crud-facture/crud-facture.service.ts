import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Invoice} from '../../../data/models/invoice.model';
import {InvoiceSaveEntity} from '../../../data/wrapper/invoice.save.entity.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CrudFactureService implements Resolve<any> {
    routeParams: any;
    invoice: Invoice;
    readonly httpOptions: any;
    onInvoiceChanged: BehaviorSubject<any>;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onInvoiceChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/invoices';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoice()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get invoice
     *
     * @returns {Promise<any>}
     */
    getInvoice(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onInvoiceChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.invoice = response['response'];
                        this.onInvoiceChanged.next(this.invoice);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

    createInvoice(invoiceSaveEntity: InvoiceSaveEntity) {
        return this._httpClient.post(this.serviceURL + '/save', invoiceSaveEntity, this.httpOptions);
    }

    updateInvoice(invoiceSaveEntity: InvoiceSaveEntity) {
        return this._httpClient.put(this.serviceURL + '/update', invoiceSaveEntity, this.httpOptions);
    }

}
