import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {MatDatepickerInputEvent, MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {CrudFactureService} from './crud-facture.service';
import {Customer} from '../../../data/models/customer.model';
import {CustomersService} from '../../crm/customers/customers.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Tax} from '../../../data/models/tax.model';
import {TaxesService} from '../../setting/taxes/taxes.service';
import {DsnUtils} from '../../../utils/dsn-utils';
import {InvoiceLine} from '../../../data/models/invoice.line.model';
import {Invoice} from '../../../data/models/invoice.model';
import {InvoiceSaveEntity} from '../../../data/wrapper/invoice.save.entity.model';
import {InvoiceLineService} from '../../../services/invoice.line.service';
import {DevisService} from '../devis/devis.service';
import {Devis} from '../../../data/models/devis.model';
import {METHOD_OF_PAYMENT} from '../../../data/enums/enums';


@Component({
    selector: 'invoice-crud-facture',
    templateUrl: './crud-facture.component.html',
    styleUrls: ['./crud-facture.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CrudFactureComponent implements OnInit, OnDestroy {

    customers: Customer[];
    taxes: Tax[];
    devis: Devis[];
    customer: Customer;
    invoiceLines: InvoiceLine[] = [];
    invoiceLine: InvoiceLine;
    invoice: Invoice;
    invoiceSaveEntity: InvoiceSaveEntity;
    discountVisibility: boolean = false;
    idCustomer: number;
    taxValue: number = 0;
    taxAmount: number = 0;
    discount: number = 0;
    total: number;
    minDate: Date;
    pageType: string;
    invoiceForm: FormGroup = this._formBuilder.group({});
    subTotal: number = 0;
    dsnUtils = new DsnUtils();
    methodes: any[];
    methodOfPaymentEnum = METHOD_OF_PAYMENT;
    selected = 'espece';

    // Private
    private _unsubscribeAll: Subject<any>;
    private devisEntity: Devis;

    /**
     * Constructor
     *
     * @param {CrudFactureService} _crudFactureService
     * @param {CustomersService} _customersService
     * @param {InvoiceLineService} _invoiceLineService
     * @param {TaxesService} _taxesService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     * @param {ToastrService} _toastr
     * @param {DevisService} _devisService
     * @param {Router} _router
     */
    constructor(
        private _crudFactureService: CrudFactureService,
        private _customersService: CustomersService,
        private _taxesService: TaxesService,
        private _devisService: DevisService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _invoiceLineService: InvoiceLineService,
        private _toastrService: ToastrService,
        private _matSnackBar: MatSnackBar,
        private _toastr: ToastrService,
        private _router: Router,
    ) {
        // Set the default
        this.invoice = new Invoice();
        this.invoiceLine = new InvoiceLine();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.methodes = Object.keys(this.methodOfPaymentEnum);

        this.invoiceLines = [];
        this.createInvoice();
        this.findAllTaxe();
        this.getAllDevis();
        this.findAllProspectCustomer();
        this._crudFactureService.onInvoiceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(invoice => {

                if (invoice) {

                    this.getCustomer(invoice.customer.id);
                    this.getDevis(invoice.devis.id);

                    this.findAllLineByInvoiceId(invoice.id);

                    this.subTotal = invoice.subTotal;
                    this.total = invoice.total;
                    this.discount = invoice.discount;
                    this.taxAmount = invoice.taxAmount;
                    this.taxValue = invoice.taxValue;

                    this.invoiceForm.get('id').setValue(invoice.id);
                    this.invoiceForm.get('number').setValue(invoice.number);
                    this.invoiceForm.get('reference').setValue(invoice.reference);
                    this.invoiceForm.get('title').setValue(invoice.title);
                    this.invoiceForm.get('discount').setValue(invoice.discount);
                    this.invoiceForm.get('comment').setValue(invoice.comment);
                    this.invoiceForm.get('methodOfPayment').setValue(invoice.methodOfPayment);
                    this.invoiceForm.get('deliveryDelay').setValue(invoice.deliveryDelay);
                    this.invoiceForm.get('privateComment').setValue(invoice.privateComment);
                    this.invoiceForm.get('invoiceAddress').setValue(invoice.invoiceAddress);
                    this.invoiceForm.get('customer').setValue(invoice.customer.id);
                    this.invoiceForm.get('devis').setValue(invoice.devis.id);
                    this.invoiceForm.get('invoiceDate').setValue(new Date(invoice.invoiceDate));
                    this.invoiceForm.get('dueDate').setValue(new Date(invoice.dueDate));
                    this.invoiceForm.get('createBy').setValue(invoice.createBy);
                    this.pageType = 'edit';
                    this.invoice = new Invoice(invoice);

                } else {
                    this.pageType = 'new';
                    this.invoice = new Invoice();
                    this.invoiceForm.get('createBy').setValue(this.dsnUtils.getAppUser());
                    this.invoiceForm.get('discount').setValue(this.discount);
                }
                this.minDate = new Date(this.invoiceForm.get('invoiceDate').value);
                this.addEvent();
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    createInvoice() {
        this.invoiceForm = this._formBuilder.group({
            id: new FormControl(''),
            number: new FormControl(''),
            reference: new FormControl(''),
            title: new FormControl('', Validators.required),
            customer: new FormControl('', Validators.required),
            devis: new FormControl('', Validators.required),
            comment: new FormControl(''),
            invoiceDate: new FormControl(new Date(), Validators.required),
            discount: new FormControl(this.discount),
            privateComment: new FormControl(''),
            invoiceAddress: new FormControl(''),
            methodOfPayment: new FormControl(''),
            deliveryDelay: new FormControl(''),
            dueDate: new FormControl(''),
            createBy: new FormControl('')
        });
        this.invoiceForm.get('customer').valueChanges.subscribe(value => this.invoice.customer = value);
        this.addNewRow();
    }

    onInvoiceLineChanged(index: number) {
        let row: InvoiceLine = this.invoiceLines[index];
        if (row.quantity) {
            row.amount = row.quantity * row.unitPrice;
            this.invoiceLines[index] = row;
        }
        this.onTotalCalculate();
    }

    onTotalCalculate() {
        this.subTotal = this.dsnUtils.round(this.invoiceLines.length > 0 ?
            this.invoiceLines.map(c => c.amount)
                .reduce((sum, current) => +sum + +current)
            : 0.0);

        this.onAmountUpdate();
    }

    onAmountUpdate() {
        let totalWithDiscount = this.subTotal - this.discount;
        let mTotal = this.dsnUtils.round(this.dsnUtils.getAmountTva(totalWithDiscount, this.taxValue));
        this.taxAmount = mTotal - totalWithDiscount;
        this.total = this.dsnUtils.round(mTotal);
    }

    addNewRow() {
        let size = this.invoiceLines.length;
        let newLine = new InvoiceLine();
        this.invoiceLines.splice(size + 1, 0, newLine);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    findAllProspectCustomer() {
        return this._customersService.findAll().subscribe(value => {
            this.customers = value['response'];
        }, error => console.log(error));
    }

    findAllLineByInvoiceId(id: number) {
        return this._invoiceLineService.findListByInvoice(id).subscribe(value => {
            this.invoiceLines = value;
        }, error => console.log(error));
    }


    findCustomerSelected(value: number) {
        this.idCustomer = value;
        this.getCustomer(this.idCustomer);
    }

    findAllTaxe() {
        return this._taxesService.findAll().subscribe(value => {
            this.taxes = value['response'];
        }, error => console.log(error));
    }

    getAllDevis() {
        return this._devisService.getByGenerated(false).subscribe(value => {
            this.devis = value['response'];
            if (!this.invoice.id && this.devis.length <= 0) {
                this._toastr.warning('Aucun autre devis n\'est disponible pour la facturation');
            }
        }, error1 => console.log(error1));
    }

    getCustomer(id: number) {
        return this._customersService.getById(id).subscribe(value => {
            this.customer = value['response'];
            if (this.customer) {
                this.invoiceForm.get('invoiceAddress').setValue(this.customer.addressOne);
            }
        }, error => console.log(error));
    }

    getDevis(id: number) {
        return this._devisService.getDevisById(id).subscribe(value => {
            this.devisEntity = value['response'];
        }, error1 => console.log(error1));
    }


    deleteLine(index: number) {
        this.invoiceLines.splice(index, 1);
        this.onTotalCalculate();
    }

    getTaxeSelected(value: number) {
        this.taxValue = value;
        this.onTotalCalculate();
    }

    isLinesCorrect(): boolean {
        let ret: boolean = true;
        if (this.invoiceLines.length < 1) {
            ret = false;
        } else if (this.total <= 0) {
            ret = false;
        } else {
            for (let item of this.invoiceLines) {
                if (!item.designation || !item.quantity || !item.unitPrice || item.designation.length <= 0 || item.quantity <= 0 || item.unitPrice <= 0) {
                    return false;
                }
            }
        }
        return ret;
    }

    save() {
        this.invoice = new Invoice();
        this.invoiceSaveEntity = new InvoiceSaveEntity();
        this.invoice = this.invoiceForm.getRawValue();
        this.invoice.customer = this.customer;
        this.invoice.taxValue = this.taxValue;

        this.invoice.taxAmount = this.dsnUtils.round(this.taxAmount);
        this.invoice.total = this.dsnUtils.round(this.total);
        this.invoice.subTotal = this.dsnUtils.round(this.subTotal);

        this.invoice.devis = this.devisEntity;
        this.invoice.createBy = this.invoiceForm.get('createBy').value;
        this.invoiceSaveEntity.invoice = this.invoice;
        this.invoiceSaveEntity.invoiceLines = this.invoiceLines;


        if (!this.invoice.id) {
            this._crudFactureService.createInvoice(this.invoiceSaveEntity).subscribe(ret => {
                if (ret['status'] === 'OK') {
                    let newInvoice = ret['response'];
                    this._toastr.success(ret['message']);
                    this._router.navigateByUrl('/views/invoice/details/' + newInvoice.id);
                } else {
                    this._toastr.error(ret['message']);
                }
            }, error => {
            });
        } else {
            this.invoice.updateBy = this.dsnUtils.getAppUser();
            this._crudFactureService.updateInvoice(this.invoiceSaveEntity).subscribe(data => {
                if (data['status'] === 'OK') {
                    this._toastr.success(data['message']);
                    this._router.navigateByUrl('/views/invoice/details/' + this.invoice.id);
                } else {
                    this._toastr.error(data['message']);
                }
            }, error => {
            });
        }
    }

    addEvent(event?: MatDatepickerInputEvent<Date>) {
        let selectedDate = new Date();
        if (event) {
            selectedDate = new Date(event.value);
            this.minDate = new Date(selectedDate);
        }
        selectedDate.setDate(selectedDate.getDate() + 30);
        this.invoiceForm.get('dueDate').setValue(new Date(selectedDate));
    }

    addDiscount() {
        this.discountVisibility = true;
    }

    cancelDiscount() {
        this.discountVisibility = false;
        this.discount = 0;
        this.invoiceForm.get('discount').setValue(this.discount);
        this.onTotalCalculate();
    }

    includeDiscount(value) {
        if (value) {
            let dsct = Number.parseInt(value.replace(/ /g, ''));
            if (dsct < this.subTotal) {
                this.discount = dsct;
                this.onTotalCalculate();
            } else {
                this.invoiceForm.get('discount').setValue(this.discount);
                this._toastr.warning('La remise ne peut pas depasser le sous total');
            }
        } else {
            this.discount = 0;
            this.onTotalCalculate();
            this.invoiceForm.get('discount').setValue(this.discount);
        }

    }

}
