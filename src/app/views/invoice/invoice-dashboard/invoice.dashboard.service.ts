import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {InvoiceDashBody} from '../../../utils/dashboard/invoice-dash-body';
import {Invoice} from '../../../data/models/invoice.model';

@Injectable({
    providedIn: 'root'
})
export class InvoiceDashboardService implements Resolve<any> {
    totalElements: number;
    page: number = 0;
    invoiceDashBody: InvoiceDashBody;
    invoices: Invoice[];
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        this.serviceURL = environment.serviceUrl + '/invoices';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoiceDashBody(),
                this.findAllInvoice()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get InvoiceDashBoard
     *
     * @returns {Promise<any>}
     */
    getInvoiceDashBody(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/dashBoard', this.httpOptions)
                .subscribe((response: any) => {
                    this.invoiceDashBody = response['response'];
                    resolve(response['response']);
                }, reject);
        });
    }

    findAllInvoice() {
      return this.getInvoices(this.page);
    }

    /**
     * Get Invoices
     *
     * @returns {Promise<any>}
     */
    getInvoices(page: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/find-all' + '?page=' + page, this.httpOptions)
                .subscribe((response: any) => {
                    this.invoices = response['content'];
                    this.totalElements = response['totalElements'];
                    resolve(response['content']);
                }, reject);
        });
    }

    pieChart() {
        return [{
            name: 'Montant Total Payer',
            y: this.invoiceDashBody.sumAmountPaid,
            sliced: true,
            selected: true
        }, {
            name: 'Total Reste à payer',
            y: this.invoiceDashBody.sumStayToPay
        }, {
            name: 'Total Remise',
            y: this.invoiceDashBody.totalDevisOpen
        }, {
            name: 'Total',
            y: this.invoiceDashBody.totalInvoice
        }];
    }

}
