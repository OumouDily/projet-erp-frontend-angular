import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {InvoiceDashboardService} from './invoice.dashboard.service';
import {InvoiceDashBody} from '../../../utils/dashboard/invoice-dash-body';
import {Invoice} from '../../../data/models/invoice.model';

@Component({
    selector: 'invoice-dashboard',
    templateUrl: './invoice.dashboard.component.html',
    styleUrls: ['./invoice.dashboard.theme.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class InvoiceDashboardComponent implements OnInit {
    invoiceDashBody: InvoiceDashBody;
    invoices: Invoice[];
    pieChart = [];
    displayedColumns = ['number', 'amountPaid', 'stayToPay', 'discount', 'total'];

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {InvoiceDashboardService} _invoiceDashboardService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _invoiceDashboardService: InvoiceDashboardService
    ) {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.invoices = this._invoiceDashboardService.invoices;
        this.invoiceDashBody = this._invoiceDashboardService.invoiceDashBody;
        this.pieChart = this._invoiceDashboardService.pieChart();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    onPageChanged(e) {
        this._invoiceDashboardService.page = e.pageIndex;
        this._invoiceDashboardService.findAllInvoice();
    }


}


