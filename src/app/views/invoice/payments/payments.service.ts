import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {InvoicePayment} from '../../../data/models/invoice.payment.model';

@Injectable({
    providedIn: 'root'
})
export class PaymentsService implements Resolve<any> {
    payments: InvoicePayment[];
    onPaymentsChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onPaymentsChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/payments';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPayments()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Payments
     *
     * @returns {Promise<any>}
     */
    getPayments(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/all', this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.payments = res['response'];
                        this.onPaymentsChanged.next(this.payments);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }

}
