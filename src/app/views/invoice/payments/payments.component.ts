import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {takeUntil} from 'rxjs/internal/operators';
import {PaymentsService} from './payments.service';
import {InvoicePaymentFormDialogComponent} from '../payment-form/payment-form.component';
import {InvoicePayment} from '../../../data/models/invoice.payment.model';
import {InvoicePaymentService} from '../../../services/invoice.payment.service';
import {ToastrService} from 'ngx-toastr';
import {METHOD_OF_PAYMENT, PAYMENT_STATE} from '../../../data/enums/enums';
import {DsnUtils} from '../../../utils/dsn-utils';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {RoleUtils} from '../../../utils/role-utils';

@Component({
    selector: 'invoice-payments',
    templateUrl: './payments.component.html',
    styleUrls: ['./payments.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class PaymentsComponent implements OnInit {
    roleUtils = new RoleUtils();
    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();

    dataSource: FilesDataSource | null;
    displayedColumns = ['customer', 'invoice', 'netToPay', 'paymentDate', 'amount', 'methodOfPayment', 'balanceAfter', 'createBy', 'state', 'validateBy', 'buttons'];

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    dialogRef: any;

    methodOfPaymentEnum = METHOD_OF_PAYMENT;
    paymentState = PAYMENT_STATE;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _paymentsService: PaymentsService,
        private _invoicePaymentService: InvoicePaymentService,
        private _toastr: ToastrService,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._paymentsService, this.paginator, this.sort);

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    showPaymentDialog(action?: string, invoicePayment?: InvoicePayment) {
        this.dialogRef = this._matDialog.open(InvoicePaymentFormDialogComponent, {
            panelClass: 'payment-form-dialog',
            data: {
                action: action,
                invoicePayment: invoicePayment
            }
        });
    }


    cancelPayment(payment: InvoicePayment) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre d\'annuler ce paiement ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                payment.cancelBy = this.dsnUtils.getAppUser();
                payment.cancelDate = new Date();
                this._invoicePaymentService.cancelPayment(payment).subscribe(data => {
                    if (data['status'] === 'OK') {
                        this._toastr.success(data['message']);
                        const payIndex = this._paymentsService.payments.indexOf(payment);
                        this._paymentsService.payments.splice(payIndex, 1);
                        this._paymentsService.onPaymentsChanged.next(this._paymentsService.payments);
                    } else {
                        this._toastr.error(data['message']);
                    }
                }, error => console.log(error));
            }
            this.confirmDialogRef = null;
        });
    }
}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {PaymentsService} _paymentsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _paymentsService: PaymentsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._paymentsService.payments;
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._paymentsService.onPaymentsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._paymentsService.payments.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'customer':
                    [propertyA, propertyB] = [a.invoice.customer.id, b.invoice.customer.id];
                    break;
                case 'invoice':
                    [propertyA, propertyB] = [a.invoice.number, b.invoice.number];
                    break;
                case 'netToPay':
                    [propertyA, propertyB] = [a.netToPay, b.netToPay];
                    break;
                case 'paymentDate':
                    [propertyA, propertyB] = [a.paymentDate, b.paymentDate];
                    break;
                case 'amount':
                    [propertyA, propertyB] = [a.amount, b.amount];
                    break;
                case 'methodOfPayment':
                    [propertyA, propertyB] = [a.methodOfPayment, b.methodOfPayment];
                    break;
                case 'balanceAfter':
                    [propertyA, propertyB] = [a.balanceAfter, b.balanceAfter];
                    break;
                case 'createBy':
                    [propertyA, propertyB] = [a.createBy.id, b.createBy.id];
                    break;
                case 'state':
                    [propertyA, propertyB] = [a.state, b.state];
                    break;
                case 'validateBy':
                    [propertyA, propertyB] = [a.validateBy.id, b.validateBy.id];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
