import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Devis} from '../../../data/models/devis.model';
import {DevisSaveEntity} from '../../../data/wrapper/devis.save.entity.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CrudDevisService implements Resolve<any> {
    routeParams: any;
    devis: Devis;
    readonly httpOptions: any;
    readonly serviceURL: string;
    onDevisChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onDevisChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/devis';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDevis()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get invoice
     *
     * @returns {Promise<any>}
     */
    getDevis(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onDevisChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.devis = response['response'];
                        this.onDevisChanged.next(this.devis);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

    createDevis(devisSaveEntity: DevisSaveEntity) {
        return this._httpClient.post(this.serviceURL + '/save', devisSaveEntity, this.httpOptions);
    }

    duplicateDevis(devisSaveEntity: DevisSaveEntity) {
        return this._httpClient.post(this.serviceURL + '/duplicate', devisSaveEntity, this.httpOptions);
    }

    updateDevis(devisSaveEntity: DevisSaveEntity) {
        return this._httpClient.put(this.serviceURL + '/update', devisSaveEntity, this.httpOptions);
    }

}
