import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {CrudDevisService} from './crud-devis.service';
import {Customer} from '../../../data/models/customer.model';
import {CustomersService} from '../../crm/customers/customers.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DevisLine} from '../../../data/models/devis.line.model';
import {Devis} from '../../../data/models/devis.model';
import {DevisSaveEntity} from '../../../data/wrapper/devis.save.entity.model';
import {TaxesService} from '../../setting/taxes/taxes.service';
import {DsnUtils} from '../../../utils/dsn-utils';
import {DevisLineService} from '../../../services/devis.line.service';
import {Tax} from '../../../data/models/tax.model';
import {DevisService} from '../devis/devis.service';
import {User} from '../../../data/models/user.model';
import {UserService} from '../../../services/user.service';

@Component({
    selector: 'invoice-crud-devis',
    templateUrl: './crud-devis.component.html',
    styleUrls: ['./crud-devis.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CrudDevisComponent implements OnInit, OnDestroy {

    customers: Customer[];
    agents: User[];
    taxes: Tax[];
    customer: Customer;
    agent: User;
    devisLines: DevisLine[] = [];
    devisLine: DevisLine;
    devis: Devis;
    devisSaveEntity: DevisSaveEntity;
    taxValue: number = 0;
    taxAmount: number = 0;
    total: number = 0;
    discount: number = 0;
    pageType: string;
    discountVisibility: boolean = false;
    devisForm: FormGroup = this._formBuilder.group({});
    totalDevisHT: number = 0;
    dsnUtils = new DsnUtils();
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CrudDevisService} _crudDevisService
     * @param {CustomersService} _customersService
     * @param {DevisLineService} _devisLineService
     * @param {TaxesService} _taxesService
     * @param _devisService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     * @param {ToastrService} _toastr
     * @param {Router} _router
     */
    constructor(
        private _crudDevisService: CrudDevisService,
        private _customersService: CustomersService,
        private _userService: UserService,
        private _taxesService: TaxesService,
        private _devisService: DevisService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _devisLineService: DevisLineService,
        private _matSnackBar: MatSnackBar,
        private _toastr: ToastrService,
        private _router: Router,
    ) {
        // Set the default
        this.devis = new Devis();
        this.devisLine = new DevisLine();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Devis';

        this.devisLines = [];
        this.createInvoice();
        this.findAllTaxe();
        this.findCustomers();
        this.findAgents();
        this._crudDevisService.onDevisChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(devis => {
                if (devis) {

                    if (devis.customer) {
                        this.onCustomerSelected(devis.customer.id);
                        this.devisForm.get('customer').setValue(devis.customer.id);
                    }

                    if (devis.agent) {
                        this.onAgentSelected(devis.agent.id);
                        this.devisForm.get('agent').setValue(devis.agent.id);
                    }

                    this.findAllLineByDevisId(devis.id);

                    this.totalDevisHT = devis.subTotal;
                    this.total = devis.total;
                    this.taxAmount = devis.taxAmount;
                    this.taxValue = devis.taxValue;
                    this.discount = devis.discount;
                    this.devisForm.get('id').setValue(devis.id);
                    this.devisForm.get('number').setValue(devis.number);
                    this.devisForm.get('title').setValue(devis.title);

                    this.devisForm.get('comment').setValue(devis.comment);
                    this.devisForm.get('showReglementMode').setValue(devis.showReglementMode);
                    this.devisForm.get('showComment').setValue(devis.showComment);
                    this.devisForm.get('discount').setValue(devis.discount);
                    this.devisForm.get('privateComment').setValue(devis.privateComment);
                    this.devisForm.get('devisDate').setValue(devis.devisDate);
                    this.devisForm.get('invoiceAddress').setValue(devis.invoiceAddress);
                    this.devisForm.get('devisDate').setValue(new Date(devis.devisDate));
                    this.devisForm.get('createBy').setValue(devis.createBy);

                    this.pageType = 'edit';
                    this.devis = new Devis(devis);
                } else {
                    this.pageType = 'new';
                    this.devis = new Devis();
                    this.devisForm.get('createBy').setValue(this.dsnUtils.getAppUser());
                    this.devisForm.get('discount').setValue(this.discount);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    createInvoice() {
        this.devisForm = this._formBuilder.group({
            id: new FormControl(''),
            number: new FormControl(''),
            title: new FormControl(''),
            customer: new FormControl('', Validators.required),
            agent: new FormControl('', Validators.required),
            comment: new FormControl(''),
            showReglementMode: new FormControl(''),
            showComment: new FormControl(''),
            discount: new FormControl(this.discount),
            devisDate: new FormControl('', Validators.required),
            privateComment: new FormControl(''),
            invoiceAddress: new FormControl(''),
            createBy: new FormControl('')
        });

        this.devisForm.get('customer').valueChanges.subscribe(value => this.devis.customer = value);

        this.addNewRow();
    }

    onDevisLineChanged(index: number) {
        let row: DevisLine = this.devisLines[index];
        if (row.quantity) {
            row.amount = row.quantity * row.unitPrice;
            this.devisLines[index] = row;
        }
        this.onTotalCalculate();
    }

    onTotalCalculate() {
        this.totalDevisHT = this.dsnUtils.round(this.devisLines.length > 0 ?
            this.devisLines.map(c => c.amount)
                .reduce((sum, current) => +sum + +current)
            : 0.0);

        this.onAmountUpdate();
    }

    onAmountUpdate() {
        let totalWithDiscount = this.totalDevisHT - this.discount;
        let mTotal = this.dsnUtils.round(this.dsnUtils.getAmountTva(totalWithDiscount, this.taxValue));
        this.taxAmount = mTotal - totalWithDiscount;
        this.total = this.dsnUtils.round(mTotal);
    }

    addNewRow() {
        let size = this.devisLines.length;
        let newLine = new DevisLine();
        this.devisLines.splice(size + 1, 0, newLine);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    findCustomers() {
        return this._customersService.findAll().subscribe(value => {
            this.customers = value['response'];
        }, error => console.log(error));
    }

    findAgents() {
        return this._userService.list().subscribe(value => {
            this.agents = value['response'];
        }, error => console.log(error));
    }

    findAllLineByDevisId(id: number) {
        return this._devisLineService.findListByDevis(id).subscribe(value => {
            this.devisLines = value;
            // console.log(this.devisLines);
        }, error => console.log(error));
    }


    onCustomerSelected(id: number) {
        this._customersService.getById(id).subscribe(value => {
            this.customer = value['response'];
            if (this.customer) {
                this.devisForm.get('invoiceAddress').setValue(this.customer.addressOne);
            }
        }, error => console.log(error));
    }

    onAgentSelected(id: number) {
        this._userService.getUserById(id).subscribe(value => {
            this.agent = value['response'];
        }, error => console.log(error));
    }

    findAllTaxe() {
        return this._taxesService.findAll().subscribe(value => {
            this.taxes = value['response'];
        }, error => console.log(error));
    }


    deleteLine(index: number) {
        this.devisLines.splice(index, 1);
        this.onTotalCalculate();
    }

    getTaxeSelected(value: number) {
        this.taxValue = value;
        this.onTotalCalculate();
    }

    isLinesCorrect(): boolean {
        let ret: boolean = true;
        if (this.devisLines.length < 1) {
            ret = false;
        } else if (this.total <= 0) {
            ret = false;
        } else {
            for (let item of this.devisLines) {
                if (!item.designation || !item.quantity || !item.unitPrice || item.designation.length <= 0 || item.quantity <= 0 || item.unitPrice <= 0) {
                    return false;
                }
            }
        }
        return ret;
    }

    duplicate() {
        this.devis = new Devis();
        this.devisSaveEntity = new DevisSaveEntity();

        this.devis.id = this.devisForm.get('id').value;
        this.devis.number = null;
        this.devis.title = this.devisForm.get('title').value;
        this.devis.taxValue = this.taxValue;
        this.devis.comment = this.devisForm.get('comment').value;
        this.devis.showReglementMode = this.devisForm.get('showReglementMode').value;
        this.devis.showComment = this.devisForm.get('showComment').value;
        this.devis.discount = this.devisForm.get('discount').value;
        this.devis.privateComment = this.devisForm.get('privateComment').value;
        this.devis.devisDate = this.devisForm.get('devisDate').value;
        this.devis.invoiceAddress = this.devisForm.get('invoiceAddress').value;

        this.devis.customer = this.customer;
        this.devis.agent = this.agent;

        this.devis.discount = this.dsnUtils.round(this.discount);
        this.devis.taxAmount = this.dsnUtils.round(this.taxAmount);
        this.devis.total = this.dsnUtils.round(this.total);
        this.devis.subTotal = this.dsnUtils.round(this.totalDevisHT);

        this.devis.createBy = this.dsnUtils.getAppUser();
        this.devis.updateBy = this.dsnUtils.getAppUser();

        this.devisSaveEntity.devis = this.devis;

        this.devisSaveEntity.devisLines = this.devisLines;

        this._crudDevisService.duplicateDevis(this.devisSaveEntity).subscribe(data => {
            if (data['status'] === 'OK') {
                this._toastr.success(data['message']);
                this._router.navigateByUrl('/views/invoice/devis');
            } else {
                this._toastr.error(data['message']);
            }
        }, error => {
        });
    }

    save() {
        const devis = new Devis();
        this.devisSaveEntity = new DevisSaveEntity();
        devis.id = this.devisForm.get('id').value;
        devis.number = this.devisForm.get('number').value;
        devis.title = this.devisForm.get('title').value;
        devis.taxValue = this.taxValue;
        devis.comment = this.devisForm.get('comment').value;
        devis.showReglementMode = this.devisForm.get('showReglementMode').value;
        devis.showComment = this.devisForm.get('showComment').value;
        devis.discount = this.devisForm.get('discount').value;
        devis.privateComment = this.devisForm.get('privateComment').value;
        devis.devisDate = this.devisForm.get('devisDate').value;
        devis.invoiceAddress = this.devisForm.get('invoiceAddress').value;

        devis.customer = this.customer;
        devis.agent = this.agent;

        devis.taxAmount = this.dsnUtils.round(this.taxAmount);
        devis.total = this.dsnUtils.round(this.total);
        devis.subTotal = this.dsnUtils.round(this.totalDevisHT);

        devis.createBy = this.devisForm.get('createBy').value;

        this.devisSaveEntity.devis = devis;
        this.devisSaveEntity.devisLines = this.devisLines;
        if (devis.id == 0) {
            this._crudDevisService.createDevis(this.devisSaveEntity).subscribe(ret => {
                if (ret['status'] === 'OK') {
                    let newDevis = ret['response'];
                    this._toastr.success(ret['message']);
                    this._router.navigateByUrl('/views/invoice/devis-details/' + newDevis.id);
                } else {
                    this._toastr.error(ret['message']);
                }
            }, error => {
            });
        } else {
            devis.updateBy = this.dsnUtils.getAppUser();
            this._crudDevisService.updateDevis(this.devisSaveEntity).subscribe(data => {
                if (data['status'] === 'OK') {
                    this._toastr.success(data['message']);
                    this._router.navigateByUrl('/views/invoice/devis-details/' + devis.id);
                } else {
                    this._toastr.error(data['message']);
                }
            }, error => {
            });
        }
    }

    addDiscount() {
        this.discountVisibility = true;
        this.discount = 0;
    }

    includeDiscount(value) {
        if (value) {
            let dsct = Number.parseInt(value.replace(/ /g, ''));
            if (dsct < this.totalDevisHT) {
                this.discount = dsct;
                this.onTotalCalculate();
            } else {
                this.devisForm.get('discount').setValue(this.discount);
                this._toastr.warning('La remise ne peut pas depasser le sous total');
            }
        } else {
            this.discount = 0;
            this.onTotalCalculate();
            this.devisForm.get('discount').setValue(this.discount);
        }

    }

    cancelDiscount() {
        this.discountVisibility = false;
        this.discount = 0;
        this.devisForm.get('discount').setValue(this.discount);
        this.onTotalCalculate();
    }
}
