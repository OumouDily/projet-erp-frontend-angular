import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {Invoice} from '../../../../data/models/invoice.model';
import {DsnUtils} from '../../../../utils/dsn-utils';
import {environment} from '../../../../../environments/environment';
import {InvoicePayment} from '../../../../data/models/invoice.payment.model';
import {InvoicePaymentService} from '../../../../services/invoice.payment.service';

@Injectable({
    providedIn: 'root'
})
export class InvoicePrintService implements Resolve<any> {
    routeParams: any;
    invoice: Invoice;
    invoicePayment: InvoicePayment;
    onInvoiceChanged: BehaviorSubject<any>;
    onInvoicePaymentChanged: BehaviorSubject<any>;
    readonly serviceURL: string;
    readonly httpOptions: any;

    /**
     * Constructor
     *
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _invoicePayment: InvoicePaymentService
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onInvoiceChanged = new BehaviorSubject({});
        this.onInvoicePaymentChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/invoices';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoicePrint()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getInvoicePrint(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onInvoiceChanged.next(false);
                resolve(false);
            } else {
                this._invoicePayment.get(this.routeParams.id).subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.invoicePayment = ret['response'];
                        this.onInvoicePaymentChanged.next(this.invoicePayment);
                        this._httpClient.get(this.serviceURL + '/' + this.invoicePayment.invoice.id, this.httpOptions)
                            .subscribe((response: any) => {
                                this.invoice = response['response'];
                                this.onInvoiceChanged.next(this.invoice);
                                resolve(response['response']);
                            }, reject);
                    }
                });
            }
        });
    }

}
