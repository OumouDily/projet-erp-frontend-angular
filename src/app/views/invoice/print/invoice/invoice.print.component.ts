import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Invoice} from '../../../../data/models/invoice.model';
import {InvoicePrintService} from './invoice.print.service';
import {InvoiceLine} from '../../../../data/models/invoice.line.model';
import {InvoiceLineService} from '../../../../services/invoice.line.service';
import {fuseAnimations} from '../../../../../@fuse/animations';
import {DsnUtils} from '../../../../utils/dsn-utils';
import {InvoicePayment} from '../../../../data/models/invoice.payment.model';


@Component({
    selector: 'invoice-print',
    templateUrl: './invoice.print.component.html',
    styleUrls: ['./invoice.print.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class InvoicePrintComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();
    invoice: Invoice;
    invoicePayment: InvoicePayment;
    invoiceLines: InvoiceLine[] = [];
    societyInfo = {
        name: 'DSN MALI',
        address: 'Hamdallaye ACI 2000',
        phone: '+223 20 29 02 17',
        email: 'contact@dsnmali.com',
        website: 'www.dsnmali.com',
        logoUri: 'assets/images/logos/dsn-logo.png',
        invoiceFooterText: 'N° Compte Orabank : 051639200201 --  NIF : 086138237C --RC : MA.BKO.2015 B.7466'
    };

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {InvoicePrintService} _invoicePrintService
     * @param {InvoiceLineService} _invoiceLineService
     */
    constructor(
        private _invoicePrintService: InvoicePrintService,
        private _invoiceLineService: InvoiceLineService,
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        // Subscribe to update order on changes
        this._invoicePrintService.onInvoicePaymentChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(invoicePayment => {
                this.invoicePayment = invoicePayment;

            });

        this._invoicePrintService.onInvoiceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(invoice => {
                this.findAllLineByInvoiceId(invoice.id);
                this.invoice = invoice;

            });


    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    findAllLineByInvoiceId(id: number) {
        return this._invoiceLineService.findListByInvoice(id).subscribe(value => {
            this.invoiceLines = value;
            // console.log(this.invoiceLines);
        }, error => console.log(error));
    }

    onPrint() {
        document.title = 'FACTURE ' + this.invoice.number;
        window.print();
    }
}
