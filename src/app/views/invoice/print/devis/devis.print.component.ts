import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {Devis} from '../../../../data/models/devis.model';
import {DevisLine} from '../../../../data/models/devis.line.model';
import {DevisLineService} from '../../../../services/devis.line.service';
import {DevisPrintService} from './devis.print.service';
import {DsnUtils} from '../../../../utils/dsn-utils';

@Component({
    selector: 'invoice-devis-print',
    templateUrl: './devis.print.component.html',
    styleUrls: ['./devis.print.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DevisPrintComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();
    isRemise: boolean = true;
    devis: Devis;
    devisLines: DevisLine[] = [];
    societyInfo = {
        name: 'DSN MALI',
        address: 'Hamdallaye ACI 2000',
        phone: '+223 20 29 02 17',
        email: 'contact@dsnmali.com',
        website: 'www.dsnmali.com',
        logoUri: 'assets/images/logos/dsn-logo.png',
        devisFooterText: 'N° Compte Orabank : 051639200201 --  NIF : 086138237C --RC : MA.BKO.2015 B.7466'
    };


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {PrintService} _printService
     * @param {DevisLineService} _devisLineService
     */
    constructor(
        private _printService: DevisPrintService,
        private _devisLineService: DevisLineService,
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        // Set the defaults
        this.devis = new Devis();

        // Subscribe to update order on changes
        this._printService.onDevisChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(devis => {
                this.findAllLineByDevisId(devis.id);
                this.devis = new Devis(devis);
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    findAllLineByDevisId(id: number) {
        return this._devisLineService.findListByDevis(id).subscribe(value => {
            this.devisLines = value;
        }, error => console.log(error));
    }


    onPrint() {
        document.title='DEVIS '+this.devis.number;
        window.print();
    }


}
