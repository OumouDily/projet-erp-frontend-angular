import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {Devis} from '../../../data/models/devis.model';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DevisDetailsService implements Resolve<any> {
    routeParams: any;
    devis: Devis;
    onDevisChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onDevisChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/devis';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDevisDetails()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get devis
     *
     * @returns {Promise<any>}
     */
    getDevisDetails(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onDevisChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/details/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.devis = response['response'];
                        this.onDevisChanged.next(this.devis);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

}
