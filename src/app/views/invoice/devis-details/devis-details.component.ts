import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {Devis} from '../../../data/models/devis.model';
import {DevisDetailsService} from './devis-details.service';
import {DevisLine} from '../../../data/models/devis.line.model';
import {DevisLineService} from '../../../services/devis.line.service';
import {DsnUtils} from '../../../utils/dsn-utils';
import {RoleUtils} from '../../../utils/role-utils';
import {InvoiceFormDialogComponent} from '../invoice-form/invoice-form.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {DevisService} from '../devis/devis.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
    selector: 'invoice-devis-details',
    templateUrl: './devis-details.component.html',
    styleUrls: ['./devis-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DevisDetailsComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    currentUser = this.dsnUtils.getAppUser();

    devisDetails: Devis;
    devisLines: DevisLine[] = [];

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
    dialogRef: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {DevisDetailsService} _devisDetailsService
     * @param {DevisLineService} _devisLineService
     */
    constructor(
        private _devisDetailsService: DevisDetailsService,
        private _devisLineService: DevisLineService,
        private _devisService: DevisService,
        private toast: ToastrService,
        public _matDialog: MatDialog,
        private _router: Router
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        // Set the defaults
        this.devisDetails = new Devis();

        // Subscribe to update order on changes
        this._devisDetailsService.onDevisChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(devisDetails => {
                this.findAllLineByDevisId(devisDetails.id);
                this.devisDetails = new Devis(devisDetails);
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    findAllLineByDevisId(id: number) {
        return this._devisLineService.findListByDevis(id).subscribe(value => {
            this.devisLines = value;
        }, error => console.log(error));
    }

    newInvoice(devis) {
        this.dialogRef = this._matDialog.open(InvoiceFormDialogComponent, {
            panelClass: 'invoice-form-dialog',
            data: {
                devis: devis,
                action: 'new'
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
            });
    }

    delete(devis): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimer le devis ' + devis.number + ' ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._devisService.delete(devis.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);
                        this._router.navigateByUrl('/views/invoice/devis');
                    } else {
                        this.toast.error(ret['message']);
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    cancel(devis: Devis) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir annuler le devis ' + devis.number + ' ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                devis.cancelBy = this.currentUser;
                devis.cancelDate = new Date();
                this._devisService.cancelDevis(devis).subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.devisDetails = ret['response'];
                        this.toast.success(ret['message']);
                    } else {
                        this.toast.error(ret['message']);
                    }
                }, error => console.log(error));
            }
            this.confirmDialogRef = null;
        });
    }
}
