import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Invoice} from '../../../data/models/invoice.model';
import {InvoicePayment} from '../../../data/models/invoice.payment.model';
import {FacturesService} from '../factures/factures.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {InvoicePaymentService} from '../../../services/invoice.payment.service';
import {DsnUtils} from '../../../utils/dsn-utils';
import {PaymentsService} from '../payments/payments.service';
import {METHOD_OF_PAYMENT} from '../../../data/enums/enums';
import {Subject} from 'rxjs';

@Component({
    selector: 'invoice-payment-form-dialog',
    templateUrl: './payment-form.component.html',
    styleUrls: ['./payment-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class InvoicePaymentFormDialogComponent implements OnInit, OnDestroy {
    action: string;
    invoices: Invoice[];
    invoicePayment: InvoicePayment;
    invoicePaymentForm: FormGroup;
    dialogTitle: string;
    dsnUtils = new DsnUtils();
    methodes: any[];
    methodOfPaymentEnum = METHOD_OF_PAYMENT;
    maxDate = new Date();

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<InvoicePaymentFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     * @param _invoiceService
     * @param _paymentsService
     * @param _invoicePaymentService
     * @param _toastrService
     * @param _router
     */
    constructor(
        public matDialogRef: MatDialogRef<InvoicePaymentFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _invoiceService: FacturesService,
        private _paymentsService: PaymentsService,
        private _invoicePaymentService: InvoicePaymentService,
        private _toastrService: ToastrService,
        private _router: Router
    ) {

        this._unsubscribeAll = new Subject();

        // Set the defaults
        this.action = _data.action;
        this.invoicePayment = new InvoicePayment();

        if (this.action === 'issue') {
            this.invoicePayment.invoice = _data.invoice;
            this.dialogTitle = 'Emission de paiement sur la facture Nº' + this.invoicePayment.invoice.number;

            this.issuePaymentForm();

        } else if (this.action === 'validate') {
            this.invoicePayment = _data.invoicePayment;
            this.dialogTitle = 'Validation de paiement de la Facture Nº ' + this.invoicePayment.number;

            this.validatePaymentForm();

        } else {
            this.dialogTitle = 'Nouveau Paiment';
            this.invoicePayment = new InvoicePayment({});
        }

    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.methodes = Object.keys(this.methodOfPaymentEnum);
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    issuePaymentForm() {
        this.invoicePaymentForm = this._formBuilder.group({
            id: new FormControl(this.invoicePayment.id),
            invoice: new FormControl(this.invoicePayment.invoice, Validators.required),
            netToPay: new FormControl('', Validators.required),
            amount: new FormControl(''),
            stayToPay: new FormControl(this.invoicePayment.invoice ? this.invoicePayment.invoice.stayToPay : 0),
            total: new FormControl(this.invoicePayment.invoice ? this.invoicePayment.invoice.total : 0)
        });
        this.updateAmounts();
    }

    validatePaymentForm() {
        let netToPay = this.invoicePayment.netToPay;
        let amount = netToPay;
        let stayToPay = this.invoicePayment.invoice ? (this.invoicePayment.invoice.total - amount) : 0;
        this.invoicePaymentForm = this._formBuilder.group({
            id: new FormControl(this.invoicePayment.id),
            reference: new FormControl(this.invoicePayment.reference),
            methodOfPayment: new FormControl(this.invoicePayment.methodOfPayment, Validators.required),
            paymentDate: new FormControl(new Date(this.invoicePayment.paymentDate), Validators.required),
            invoice: new FormControl(this.invoicePayment.invoice, Validators.required),
            netToPay: new FormControl(netToPay, Validators.required),
            amount: new FormControl(amount, Validators.required),
            stayToPay: new FormControl(stayToPay),
            total: new FormControl(this.invoicePayment.invoice ? this.invoicePayment.invoice.total : 0)
        });
    }

    save() {
        if (this.invoicePaymentForm.get('netToPay').value <= 0) {
            this._toastrService.error('Le net à payer est toujours superieur à 0');
            return;
        }
        this.invoicePayment = this.invoicePaymentForm.getRawValue();
        if (this.action === 'issue') {
            this.invoicePayment.amount = this.invoicePayment.netToPay;
            this.invoicePayment.createBy = this.dsnUtils.getAppUser();
            this._invoicePaymentService.create(this.invoicePayment).subscribe(ret => {
                if (ret['status'] === 'OK') {
                    let invoicePayment = ret['response'];
                    this._toastrService.success(ret['message']);
                    this._router.navigateByUrl('/views/invoice/print/payment/' + invoicePayment.id);
                    if (this._paymentsService.payments) {
                        this._paymentsService.payments.push(ret['response']);
                        this._paymentsService.onPaymentsChanged.next(this._paymentsService.payments);
                    }
                    this.matDialogRef.close();
                } else {
                    this._toastrService.error(ret['message']);
                }
            }, error => {
            });
        } else if (this.action === 'validate') {
            this.invoicePayment.validateBy = this.dsnUtils.getAppUser();
            this._invoicePaymentService.validate(this.invoicePayment).subscribe(ret => {
                if (ret['status'] === 'OK') {
                    this._toastrService.success(ret['message']);
                    this._paymentsService.getPayments();

                    this.matDialogRef.close();
                } else {
                    this._toastrService.error(ret['message']);
                }
            }, error => {
            });
        }
    }

    checkNetToPay(value) {
        if (value) {
            let netToPay = Number.parseInt(value.replace(/ /g, ''));
            if (netToPay < this.invoicePayment.invoice.stayToPay) {
                this.invoicePaymentForm.get('netToPay').setValue(netToPay);
            } else {
                this.invoicePaymentForm.get('netToPay').setValue(this.invoicePayment.invoice.stayToPay);
                this._toastrService.warning('Le montant ne peut pas depasser le reste à payer');
            }
        } else {
            this.invoicePaymentForm.get('netToPay').setValue(0);
        }
        this.updateAmounts();
    }

    updateAmounts() {
        let netToPay = this.invoicePaymentForm.get('netToPay').value;
        let newStayToPay = this.invoicePayment.invoice.stayToPay - netToPay;
        this.invoicePaymentForm.get('stayToPay').setValue(newStayToPay);
    }

}
