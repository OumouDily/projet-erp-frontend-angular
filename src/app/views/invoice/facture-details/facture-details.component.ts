import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FactureDetailsService} from './facture-details.service';
import {Invoice} from '../../../data/models/invoice.model';
import {InvoiceLine} from '../../../data/models/invoice.line.model';
import {InvoiceLineService} from '../../../services/invoice.line.service';
import {INVOICE_STATE, METHOD_OF_PAYMENT} from '../../../data/enums/enums';
import {DsnUtils} from '../../../utils/dsn-utils';
import {RoleUtils} from '../../../utils/role-utils';
import {InvoicePaymentFormDialogComponent} from '../payment-form/payment-form.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {FacturesService} from '../factures/factures.service';
import {Router} from '@angular/router';

@Component({
    selector: 'invoice-facture-details',
    templateUrl: './facture-details.component.html',
    styleUrls: ['./facture-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FactureDetailsComponent implements OnInit, OnDestroy {
    INVOICE_STATE = INVOICE_STATE

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    currentUser = this.dsnUtils.getAppUser();

    invoice: Invoice;
    invoiceLines: InvoiceLine[] = [];
    invoiceState = INVOICE_STATE;
    methodofpayment = METHOD_OF_PAYMENT;

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
    dialogRef: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FactureDetailsService} _invoiceDetailsService
     * @param {InvoiceLineService} _invoiceLineService
     */
    constructor(
        private _invoiceDetailsService: FactureDetailsService,
        private _invoiceLineService: InvoiceLineService,
        private _inovicesService: FacturesService,
        private _toastr: ToastrService,
        private _router: Router,
        public _matDialog: MatDialog
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        // Set the defaults
        this.invoice = new Invoice();

        // Subscribe to update order on changes
        this._invoiceDetailsService.onInvoiceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(facture => {
                this.findAllLineByInvoiceId(facture.id);
                this.invoice = facture;
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    findAllLineByInvoiceId(id: number) {
        return this._invoiceLineService.findListByInvoice(id).subscribe(value => {
            this.invoiceLines = value;
        }, error => console.log(error));
    }

    cancel(invoice: Invoice) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre d\'annuler la facture ' + invoice.number + '?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                invoice.cancelBy = this.dsnUtils.getAppUser();
                invoice.cancelDate = new Date();
                this._inovicesService.cancelInvoice(invoice).subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.invoice = ret['response'];
                        this._toastr.success(ret['message']);
                    } else {
                        this._toastr.error(ret['message']);
                    }
                }, error => console.log(error));
            }
        });
    }

    delete(facture): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimer la facture ' + facture.number + ' ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._inovicesService.delete(facture.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this._toastr.success(ret['message']);
                        this._router.navigateByUrl('/views/invoice/factures');
                    } else {
                        this._toastr.error(ret['message']);
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    issuePayment(action?: string, invoice?: Invoice) {
        this.dialogRef = this._matDialog.open(InvoicePaymentFormDialogComponent, {
            panelClass: 'payment-form-dialog',
            data: {
                invoice: invoice,
                action: action
            }
        });
    }

    validation(invoice: Invoice) {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de valider la facture ' + invoice.number + '?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._inovicesService.validateAnInvoice(invoice).subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.invoice = ret['response'];
                        this._toastr.success(ret['message']);
                        this._router.navigateByUrl('/views/invoice/details/' + invoice.id);
                    } else {
                        this._toastr.error(ret['message']);
                    }
                }, error => console.log(error));
            }
        });
    }


}
