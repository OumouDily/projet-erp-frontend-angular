import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {Invoice} from '../../../data/models/invoice.model';

@Injectable({
    providedIn: 'root'
})
export class FactureDetailsService implements Resolve<any> {

    routeParams: any;
    invoice: Invoice;
    onInvoiceChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onInvoiceChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/invoices';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoiceDetails()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get devis
     *
     * @returns {Promise<any>}
     */
    getInvoiceDetails(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onInvoiceChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/details/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.invoice = response['response'];
                        this.onInvoiceChanged.next(this.invoice);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

}
