import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialogRef} from '@angular/material';
import {Devis} from '../../../data/models/devis.model';
import {DevisLine} from '../../../data/models/devis.line.model';
import {DevisLineService} from '../../../services/devis.line.service';
import {Invoice} from '../../../data/models/invoice.model';
import {InvoiceLine} from '../../../data/models/invoice.line.model';
import {InvoiceSaveEntity} from '../../../data/wrapper/invoice.save.entity.model';
import {CrudFactureService} from '../crud-facture/crud-facture.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {DsnUtils} from '../../../utils/dsn-utils';
import {METHOD_OF_PAYMENT} from '../../../data/enums/enums';


@Component({
    selector: 'invoice-form-dialog',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class InvoiceFormDialogComponent implements OnInit {

    action: string;
    devis: Devis;
    invoice: Invoice;
    invoiceLines: InvoiceLine[] = [];
    invoiceSaveEntity: InvoiceSaveEntity;
    devisLines: DevisLine[] = [];
    invoiceDialogForm: FormGroup;
    dialogTitle: string;
    minDate: Date;
    methodes: any[];
    methodOfPaymentEnum = METHOD_OF_PAYMENT;
    dsnUtils = new DsnUtils();

    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param _toastrService
     * @param {DevisLineService}_devisLineService
     * @param {CrudFactureService} _crudFactureService
     * @param {Router} _router
     * @param {ToastrService} _toastr
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<InvoiceFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _toastrService: ToastrService,
        private _devisLineService: DevisLineService,
        private _crudFactureService: CrudFactureService,
        private _toastr: ToastrService,
        private _router: Router
    ) {
        // Set the defaults
    }

    ngOnInit(): void {

        this.action = this._data.action;

        if (this.action === 'new') {
            this.dialogTitle = 'Convertion en facture';
            this.devis = this._data.devis;
            this.findAllLineByDevisId(this.devis.id);
        }
        this.invoice = new Invoice();
        this.invoiceSaveEntity = new InvoiceSaveEntity();
        this.invoiceDialogForm = this.createInvoiceDialogForm();
        this.minDate = new Date(this.invoiceDialogForm.get('invoiceDate').value);
        this.addEvent();

        this.methodes = Object.keys(this.methodOfPaymentEnum);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    findAllLineByDevisId(id: number) {
        return this._devisLineService.findListByDevis(id).subscribe(value => {
            this.devisLines = value;
        }, error => console.log(error));
    }

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createInvoiceDialogForm(): FormGroup {
        return this._formBuilder.group({
            id: new FormControl(''),
            title: new FormControl(this.devis.title, Validators.required),
            dueDate: new FormControl(''),
            invoiceDate: new FormControl(new Date(), Validators.required),
            deliveryDelay: new FormControl(''),
            reference: new FormControl(''),
        });
    }

    save() {
        this.invoice = new Invoice();
        this.invoiceSaveEntity = new InvoiceSaveEntity();
        this.invoice = this.invoiceDialogForm.getRawValue();
        this.invoice.taxValue = this.devis.taxValue;
        this.invoice.comment = this.devis.comment;
        this.invoice.privateComment = this.devis.privateComment;
        this.invoice.invoiceAddress = this.devis.invoiceAddress;
        this.invoice.customer = this.devis.customer;
        this.invoice.taxAmount = this.devis.taxAmount;
        this.invoice.discount = this.devis.discount;
        this.invoice.subTotal = this.devis.subTotal;
        this.invoice.total = this.devis.total;
        this.invoice.devis = this.devis;
        this.invoice.agent = this.devis.agent;
        this.invoice.createBy = this.dsnUtils.getAppUser();
        this.invoiceSaveEntity.invoice = this.invoice;
        this.invoiceSaveEntity.invoiceLines = this.devisLines;
        this._crudFactureService.createInvoice(this.invoiceSaveEntity).subscribe(ret => {
            if (ret['status'] === 'OK') {

                /**
                 let invoice = new Invoice(ret['response']);
                 this._toastr.success(ret['message']);
                 this._router.navigateByUrl('views/invoice/factures/' + invoice.id + '/' + invoice.number);
                 */

                this._router.navigateByUrl('/views/invoice/factures');
                this.matDialogRef.close();
            } else {
                this._toastr.error(ret['message']);
            }
        }, error => {
        });
    }

    addEvent(event?: MatDatepickerInputEvent<Date>) {
        let selectedDate = new Date();
        if (event) {
            selectedDate = new Date(event.value);
            this.minDate = new Date(selectedDate);
        }
        selectedDate.setDate(selectedDate.getDate() + 30);
        this.invoiceDialogForm.get('dueDate').setValue(new Date(selectedDate));
    }
}
