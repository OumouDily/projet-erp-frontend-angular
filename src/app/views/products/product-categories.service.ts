import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {FuseUtils} from '@fuse/utils';

import {DsnUtils} from '../../utils/dsn-utils';
import {environment} from '../../../environments/environment';
import {ProductCategory} from '../../data/models/product.category.model';

@Injectable({
    providedIn: 'root'
})
export class ProductCategoriesService implements Resolve<any> {
    dsnUtils = new DsnUtils();

    readonly serviceURL: string;
    readonly httpOptions: any;

    onCategoriesChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;

    categories: ProductCategory[];
    selectedCategories: string[] = [];

    searchText: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.serviceURL = environment.serviceUrl + '/product-categories';
        this.httpOptions = new DsnUtils().httpHeaders();

        // Set the defaults
        this.onCategoriesChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCategories(),
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getCategories();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getCategories(): Promise<any> {
        return new Promise((resolve, reject) => {

                this.getAll()
                    .subscribe((ret: any) => {

                        this.categories = ret['response'];


                        if (this.searchText && this.searchText !== '') {
                            this.categories = FuseUtils.filterArrayByString(this.categories, this.searchText);
                        }

                        this.categories = this.categories.map(category => {
                            return new ProductCategory(category);
                        });

                        this.onCategoriesChanged.next(this.categories);
                        resolve(this.categories);
                    }, reject);
            }
        );
    }

    getAll(): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/all', this.httpOptions);
    }

    /**
     * Update product
     *
     * @param product
     * @returns {Promise<any>}
     */
    updateCategory(category): Promise<any> {
        return new Promise((resolve, reject) => {

            this._httpClient.put(this.serviceURL + '/update', category, this.httpOptions)
                .subscribe(ret => {
                    this.getCategories();
                    resolve(ret['response']);
                });
        });
    }


    /**
     * Delete product
     *
     * @param product
     */
    deleteCategory(category: ProductCategory): void {
        this._httpClient.delete(this.serviceURL + '/' + category.id + '/delete', this.httpOptions)
            .subscribe(ret => {
                if (ret['status'] === 'OK') {
                    const productIndex = this.categories.indexOf(category);
                    this.categories.splice(productIndex, 1);
                    this.onCategoriesChanged.next(this.categories);
                }
            });
    }


    save(category: ProductCategory): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/save', category, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    get(id: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/' + id + '/get', this.httpOptions);
    }

    find(customerId: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/all/' + customerId, this.httpOptions);
    }
}
