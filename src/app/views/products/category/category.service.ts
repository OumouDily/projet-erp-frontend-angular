import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {RoleUtils} from '../../../utils/role-utils';
import {ProductCategory} from '../../../data/models/product.category.model';

@Injectable({
    providedIn: 'root'
})
export class CategoryService implements Resolve<any> {

    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();
    roleUtils = new RoleUtils();

    page: number = 0;
    category: ProductCategory[];
    onCategoryChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */ routeParams: any;
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onCategoryChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/product-categories';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProductsCategories()

            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProductsCategories(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/all',this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.category = res['response'];
                        this.onCategoryChanged.next(this.category);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }



    public delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    editProductCategory(category): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.put(this.serviceURL + '/update', category, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }

    saveProductCategory(category): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/save', category, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }




}
