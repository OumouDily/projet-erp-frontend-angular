import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {takeUntil} from 'rxjs/internal/operators';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {FuseConfirmDialogComponent} from '../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {RoleUtils} from '../../../utils/role-utils';
import {DsnUtils} from '../../../utils/dsn-utils';
import {CategoryService} from './category.service';
import {ProductCategory} from '../../../data/models/product.category.model';
import {CategoryFormComponent} from '../category-form/category-form.component';


@Component({
    selector: 'category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class CategoryComponent implements OnInit {

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    searchText: string;
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    dataSource: FilesDataSource | null;
    displayedColumns = ['name','description','buttons'];



    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _categoryService: CategoryService,
        public _matDialog: MatDialog,
        private toast: ToastrService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Catégorie';
        this.dataSource = new FilesDataSource(this._categoryService, this.paginator, this.sort);

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(100),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }

                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }


    showCategoryDialog(action?: string, category?: ProductCategory): void {

        if (action === 'edit' && !this.roleUtils.canUpdateProductCategory()) {
            return;
        }

        this.dialogRef = this._matDialog.open(CategoryFormComponent, {
            panelClass: 'category-form-dialog',
            data: {
                category: category,
                action: action
            }
        });

        this.dialogRef.afterClosed().subscribe(response => {
        });
    }


    delete(category): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimé ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._categoryService.delete(category.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);

                        const categoryIndex = this._categoryService.category.indexOf(category);
                        this._categoryService.category.splice(categoryIndex, 1);
                        this._categoryService.onCategoryChanged.next(this._categoryService.category);

                    } else {

                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {CategoryService} _categoryService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _categoryService: CategoryService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._categoryService.category;
    }

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._categoryService.onCategoryChanged,
            this._matPaginator.page,
            this._filterChange,
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._categoryService.category.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }


    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
