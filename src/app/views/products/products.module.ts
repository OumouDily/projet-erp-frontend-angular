import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule, MatPaginatorModule,
    MatRippleModule, MatSelectModule,
    MatTableModule,
    MatToolbarModule
} from '@angular/material';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseSidebarModule} from '@fuse/components';

import {ProductsComponent} from './products.component';
import {ProductsService} from './products.service';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductFormDialogComponent} from './product-form/product-form.component';
import {SpinnerModule} from '../../shared/modules/spinner.module';
import {NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';
import {ProductCategoryMenuGuard, ProductMenuGuard} from '../../shared/guard/role.guard';
import {CategoryComponent} from './category/category.component';
import {CategoryService} from './category/category.service';
import {CategoryFormComponent} from './category-form/category-form.component';

const routes: Routes = [
    {
        path: '',
        component: ProductsComponent,
        resolve: {
            products: ProductsService
        },
        canActivate: [ProductMenuGuard]
    },
    {
        path: 'product-categories',
        component: CategoryComponent,
        resolve: {
            products: CategoryService
        },
        canActivate: [ProductCategoryMenuGuard]
    },
    {
        path: 'product-categories/:id',
        component: CategoryFormComponent,
        resolve: {
            products: CategoryService
        }
    },
    {
        path: 'product-categories/:id/:name',
        component: CategoryFormComponent,
        resolve: {
            products: CategoryService
        }
    }
];

@NgModule({
    declarations: [
        ProductsComponent,
        ProductListComponent,
        ProductFormDialogComponent,
        CategoryComponent,
        CategoryFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        SpinnerModule,
        NgxMatIntlTelInputModule,
        MatSelectModule,
        MatPaginatorModule
    ],
    providers: [CategoryService],
    entryComponents: [
        ProductFormDialogComponent
    ]

})
export class ProductsModule {
}
