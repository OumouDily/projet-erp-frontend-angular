import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Product} from '../../../data/models/product.model';
import {ProductCategory} from '../../../data/models/product.category.model';
import {ProductCategoriesService} from '../product-categories.service';
import {DsnUtils} from '../../../utils/dsn-utils';


@Component({
    selector: 'products-product-form-dialog',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProductFormDialogComponent implements OnInit {

    dsnUtils = new DsnUtils();
    action: string;
    product: Product;
    productForm: FormGroup;
    dialogTitle: string;

    categories: ProductCategory[] = [];

    /**
     * Constructor
     *
     * @param {MatDialogRef<ProductFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ProductFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _productCategoriesService: ProductCategoriesService
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Mise à jour Produit / Service';
            this.product = _data.product;
        } else {
            this.dialogTitle = 'Nouveau Produit / Service';
            this.product = new Product({});
        }

        this.productForm = this.createProductForm();
    }

    ngOnInit(): void {
        this.getCategories();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createProductForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.product.id],
            reference: [this.product.reference],
            designation: new FormControl(this.product.designation, [Validators.required]),
            unitPriceXOF: new FormControl(this.product.unitPriceXOF, [Validators.required]),
            productCategory: new FormControl(this.product.productCategory, [Validators.required])
        });
    }

    getCategories() {
        this._productCategoriesService.getAll().subscribe(ret => {
            if (ret['status'] === 'OK') {
                this.categories = ret['response'];
            }
        });
    }
}
