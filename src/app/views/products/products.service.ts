import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { FuseUtils } from '@fuse/utils';

import {DsnUtils} from '../../utils/dsn-utils';
import {Product} from '../../data/models/product.model';
import {environment} from '../../../environments/environment';
import {ToastrService} from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class ProductsService implements Resolve<any>
{
    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();

    readonly serviceURL: string;
    readonly httpOptions: any;

    onProductsChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;

    products: Product[];
    selectedProducts: string[] = [];

    searchText: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _toastrService: ToastrService
    )
    {
        this.serviceURL = environment.serviceUrl + '/products';
        this.httpOptions = new DsnUtils().httpHeaders();

        // Set the defaults
        this.onProductsChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts(),
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getProducts();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getProducts(): Promise<any> {
        return new Promise((resolve, reject) => {

                this._httpClient.get(this.serviceURL + '/all/' + 0, this.httpOptions)
                    .subscribe((ret: any) => {

                        if (ret['status']==='OK') {
                            this.products = ret['response'];
                            // console.log('this.products : '+this.products);

                            /*if ( this.filterBy === 'starred' )
                            {
                                this.products = this.products.filter(_product => {
                                    return this.user.starred.includes(_product.id);
                                });
                            }

                            if ( this.filterBy === 'frequent' )
                            {
                                this.products = this.products.filter(_product => {
                                    return this.user.frequentProducts.includes(_product.id);
                                });
                            }*/

                            if ( this.searchText && this.searchText !== '' )
                            {
                                this.products = FuseUtils.filterArrayByString(this.products, this.searchText);
                            }

                            this.products = this.products.map(product => {
                                return new Product(product);
                            });
                        } else {
                            this._toastrService.error(ret['message']);
                        }

                        this.onProductsChanged.next(this.products);
                        resolve(this.products);
                    }, reject);
            }
        );
    }

    /**
     * Update product
     *
     * @param product
     * @returns {Promise<any>}
     */
    updateProduct(product): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.put(this.serviceURL + '/update', product, this.httpOptions)
                .subscribe(ret => {
                    if (ret['status']==='OK') {
                        if (product.id) {
                            this._toastrService.success(ret['message']);
                        } else {
                            this._toastrService.success('Produit / Service ajoute avec succes');
                        }
                    }
                    this.getProducts();
                    resolve(ret['response']);
                });
        });
    }


    /**
     * Delete product
     *
     * @param product
     */
    deleteProduct(product: Product): void
    {   this._httpClient.delete(this.serviceURL + '/' + product.id + '/delete', this.httpOptions)
        .subscribe(ret=>{
            if (ret['status']==='OK') {
                this._toastrService.success(ret['message']);
                const productIndex = this.products.indexOf(product);
                this.products.splice(productIndex, 1);
                this.onProductsChanged.next(this.products);
            }
        });
    }


    save(product: Product): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/save', product, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    get(id: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/' + id + '/get', this.httpOptions);
    }

    find(customerId: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/all/' + customerId, this.httpOptions);
    }
}
