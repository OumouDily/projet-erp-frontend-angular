import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {Product} from '../../../data/models/product.model';
import {ProductsService} from '../products.service';
import {ProductFormDialogComponent} from '../product-form/product-form.component';
import {RoleUtils} from '../../../utils/role-utils';


@Component({
    selector: 'products-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProductListComponent implements OnInit, OnDestroy {

    roleUtils = new RoleUtils();

    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    products: Product[];
    dataSource: FilesDataSource | null;
    displayedColumns = ['reference', 'designation', 'unitPriceXOF', 'buttons'];
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProductsService} _productsService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _productsService: ProductsService,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._productsService);

        this._productsService.onProductsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(products => {
                this.products = products;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit product
     *
     * @param product
     */
    editProduct(product: Product): void {

        if (!this.roleUtils.canUpdateProduct()) {
            return;
        }

        this.dialogRef = this._matDialog.open(ProductFormDialogComponent, {
            panelClass: 'product-form-dialog',
            data: {
                product: product,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this._productsService.updateProduct(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteProduct(product);

                        break;
                }
            });
    }

    /**
     * Delete Product
     */
    deleteProduct(product): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes vous sure de supprimer ce product ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._productsService.deleteProduct(product);
            }
            this.confirmDialogRef = null;
        });

    }

}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param {ProductsService} _productsService
     */
    constructor(
        private _productsService: ProductsService
    ) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._productsService.onProductsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
