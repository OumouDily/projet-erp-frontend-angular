import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {ProductCategory} from '../../../data/models/product.category.model';
import {CategoryService} from '../category/category.service';

@Component({
    selector: 'category-form-dialog',
    templateUrl: './category-form.component.html',
    styleUrls: ['./category-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CategoryFormComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();

    action: string;
    dialogTitle: string;

    categories: ProductCategory;


    productCategoryForm: FormGroup;


    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CategoryFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     * @param {CategoryService} _productCategoryService
     * @param _toastrService
     * @param _router
     */
    constructor(
        public matDialogRef: MatDialogRef<CategoryFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _productCategoryService: CategoryService,
        private _toastrService: ToastrService,
        private _router: Router
    ) {

        this._unsubscribeAll = new Subject();

        // Set the defaults
    }

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Catégorie produit';

        this.action = this._data.action;
        this.categories = new ProductCategory();

        if (this.action === 'add') {
            this.dialogTitle = 'Nouvelle catégorie produit';
        } else if (this.action === 'edit') {
            this.dialogTitle = 'Mise à jour de la catégorie produit';
            this.categories = this._data.category;

        }

        this.initProductCategoryForm();
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    initProductCategoryForm() {
        this.productCategoryForm = this._formBuilder.group({
            id: new FormControl(this.categories.id),
            name: new FormControl(this.categories.name, [Validators.required]),
            description: new FormControl(this.categories.description)

        });
    }


    save(): void {
        this.categories = this.productCategoryForm.getRawValue();
        if (this.action === 'add') {
            this._productCategoryService.saveProductCategory(this.categories)
                .then(() => {
                    // Show the success message
                    this._toastrService.success('Catégorie créee avec succès !');
                    this._productCategoryService.getProductsCategories();
                    this.matDialogRef.close();
                });
        } else if (this.action === 'edit') {
            this._productCategoryService.editProductCategory(this.categories).then(() => {
                // Show the success message
                this._toastrService.success('Catégorie modifiée avec succès !');
                this._productCategoryService.getProductsCategories();
                this.matDialogRef.close();
            });
        }
    }


}
