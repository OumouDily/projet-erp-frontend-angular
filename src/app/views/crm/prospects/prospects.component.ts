import {Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {fuseAnimations} from '@fuse/animations';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {CUSTOMER_TYPE} from '../../../data/enums/enums';
import {environment} from '../../../../environments/environment';
import {RoleUtils} from '../../../utils/role-utils';
import {DataSource} from '@angular/cdk/collections';
import {FuseUtils} from '../../../../@fuse/utils';
import {CustomersService} from '../customers/customers.service';
import {User} from '../../../data/models/user.model';
import {SearchBody} from '../../../utils/search-body';
import {EntityListUtils} from '../../../utils/entity.list.utils';

@Component({
    selector: 'crm-prospects',
    templateUrl: './prospects.component.html',
    styleUrls: ['./prospects.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class ProspectsComponent implements OnInit {

    roleUtils = new RoleUtils();
    entityListUtils = new EntityListUtils();

    action: string;
    dialogTitle: string;

    isGridView: boolean = true;
    totalElements: number;
    dialogRef: any;
    dataSource: FilesDataSource | null;
    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
    displayedColumns = ['name', 'agent', 'telephoneOne', 'email', 'addressOne', 'buttons'];
    @ViewChild(MatPaginator)
    paginator: MatPaginator;
    @ViewChild(MatSort)
    sort: MatSort;
    @ViewChild('filter')
    filter: ElementRef;
    private _unsubscribeAll: Subject<any>;
    searchInput: FormControl;
    users: User[];
    selectedUser: User;
    filteredUsers: User[];
    searchBody = new SearchBody();

    constructor(
        private _formBuilder: FormBuilder,
        private _customersService: CustomersService,
        public _matDialog: MatDialog,
        private toast: ToastrService,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _router: Router) {

        this.searchInput = new FormControl();
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }


    ngOnInit(): void {

        document.title = 'DSN ERP | Prospects';

        this.dataSource = new FilesDataSource(this._customersService, this.paginator, this.sort);

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(100),
                distinctUntilChanged()
            )
            .subscribe((folder) => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });

        this._customersService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });

        this.action = this._data.action;

        if (this.action === 'list') {
            this.dialogTitle = 'Prospects';
            this._customersService.getProspects(this.searchBody)
        }

    }


    getProspects() {
        this._customersService.getAllProspects(this._customersService.page).subscribe(ret => {
            this._customersService.prospects = ret['content'];
            this._customersService.onProspectsChanged.next(ret['content']);
            this.totalElements = ret['totalElements'];

            this.dataSource = new FilesDataSource(this._customersService, this.paginator, this.sort);

        }, error => console.log(error));
    }

    delete(prospect): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimé ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._customersService.delete(prospect.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);
                        this.getProspects();
                    } else {
                        this.toast.error(ret['message'] + '\nCe prospect a déjà un devis ou une facture à son nom');
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    onPageChanged(e) {
        this._customersService.page = e.pageIndex;
        this.getProspects();
    }

    onDisplayChanged() {
        this.isGridView = !this.isGridView;
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }
    onSearch(): void {
        this._customersService.getCustomers(this.searchBody);
    }
    onUserSelected(user?): void {
        this.selectedUser = user;
        this.searchBody.userId = user ? user.id : user;
        this.onSearch();
    }
    onListItemFiltering(tag: string, query: any) {
        if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }
    convert(prospect) {
        prospect.type = CUSTOMER_TYPE['CLIENT'];
        this._customersService.update(prospect)
            .subscribe((response: any) => {
                if (response['status'] == 'OK') {
                    this.toast.success('Prospect convertis avec succès !', 'Prospect');
                    this._router.navigateByUrl('/views/crm/customers');
                    this.getProspects();
                } else {
                    this.toast.error(response['message']);
                }
            }, e => {
                this.toast.error(environment.errorMessage);
                // console.log(e);
            });

    }


}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    constructor(
        private _customersService: CustomersService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._customersService.prospects;
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._customersService.onProspectsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._customersService.prospects.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }


    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'agent':
                    [propertyA, propertyB] = [a.agent.id, b.agent.id];
                    break;
                case 'telephoneOne':
                    [propertyA, propertyB] = [a.telephoneOne, b.telephoneOne];
                    break;
                case 'email':
                    [propertyA, propertyB] = [a.email, b.email];
                    break;
                case 'addressOne':
                    [propertyA, propertyB] = [a.addressOne, b.addressOne];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect(): void {
    }
}

