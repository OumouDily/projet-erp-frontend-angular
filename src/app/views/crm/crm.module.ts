import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MAT_DIALOG_DATA,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule, MatDialogRef,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
} from '@angular/material';
import {AgmCoreModule} from '@agm/core';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';
import {CustomersComponent} from './customers/customers.component';
import {CustomersService} from './customers/customers.service';
import {ProspectsComponent} from './prospects/prospects.component';
import {CrudProspectComponent} from './crud-prospect/crud-prospect.component';
import {CrudProspectService} from './crud-prospect/crud-prospect.service';
import {CrudCustomerComponent} from './crud-customer/crud-customer.component';
import {CrudCustomerService} from './crud-customer/crud-customer.service';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {HighchartsChartModule} from 'highcharts-angular';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {CustomerResolverService} from '../../services/customer.resolver.service';
import {ProspectResolverService} from '../../services/prospect.resolver.service';
import {SpinnerModule} from '../../shared/modules/spinner.module';
import {ConfirmDialogModule} from '../confirm-dialog/confirm-dialog.module';
import {FuseConfirmDialogModule} from '../../../@fuse/components';
import {ProjectFolderComponent} from './project-folder/project-folder.component';
import {ProjectFolderService} from './project-folder/project-folder.service';
import {
    ContactMenuGuard,
    CustomerCreateGuard,
    CustomerMenuGuard,
    CustomerUpdateGuard,
    ProjectFolderCreateGuard,
    ProjectFolderMenuGuard,
    ProjectFolderUpdateGuard,
    ProspectCreateGuard,
    ProspectMenuGuard,
    ProspectUpdateGuard
} from '../../shared/guard/role.guard';
import {ProjectFolderFormComponent} from './project-folder-form/project-folder-form.component';
import {IConfig, NgxMaskModule} from 'ngx-mask';


// @ts-ignore
export const options: Partial<IConfig> | (() => Partial<IConfig>);


const routes: Routes = [

    {
        path: 'customers',
        component: CustomersComponent,
        resolve: {
            data: CustomersService
        },
        canActivate: [CustomerMenuGuard]
    },
    {
        path: 'customers/:id/:name',
        component: CrudCustomerComponent,
        resolve: {
            data: CrudCustomerService
        },
        canActivate: [CustomerUpdateGuard]
    },
    {
        path: 'customers/:id',
        component: CrudCustomerComponent,
        resolve: {
            data: CrudCustomerService
        },
        canActivate: [CustomerCreateGuard]
    },

    {
        path: 'prospects',
        component: ProspectsComponent,
        resolve: {
            data: ProspectResolverService
        },
        canActivate: [ProspectMenuGuard]
    },
    {
        path: 'prospects/:id/:name',
        component: CrudProspectComponent,
        resolve: {
            data: CrudProspectService
        },
        canActivate: [ProspectUpdateGuard]
    },
    {
        path: 'prospects/:id',
        component: CrudProspectComponent,
        resolve: {
            data: CrudProspectService
        },
        canActivate: [ProspectCreateGuard]
    },

    {
        path: 'contacts',
        loadChildren: './contacts/contacts.module#ContactsModule',
        canActivate: [ContactMenuGuard]
    },

    {
        path: 'project-folder',
        component: ProjectFolderComponent,
        resolve: {
            data: ProjectFolderService
        },
        canActivate: [ProjectFolderMenuGuard]
    },
    {
        path: 'project-folder/:id',
        component: ProjectFolderFormComponent,
        resolve: {
            data: ProjectFolderService
        },
        canActivate: [ProjectFolderCreateGuard]
    },
    {
        path: 'project-folder/:id/:name',
        component: ProjectFolderFormComponent,
        resolve: {
            data: ProjectFolderService
        },
        canActivate: [ProjectFolderUpdateGuard]
    }
];

@NgModule({
    declarations: [
        CustomersComponent,
        CrudProspectComponent,
        CrudCustomerComponent,
        ProspectsComponent,
        ProjectFolderComponent,
        ProjectFolderFormComponent

    ],
    imports: [
        RouterModule.forChild(routes),
        NgxMaskModule.forRoot(),
        MatButtonModule,
        MatChipsModule,
        MatGridListModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatTooltipModule,
        MatListModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatGridListModule,
        MatCardModule,
        MatMenuModule,
        Ng2SearchPipeModule,
        HighchartsChartModule,
        SpinnerModule,
        ConfirmDialogModule,
        FuseConfirmDialogModule,
        MatToolbarModule,
        MatSliderModule
    ],
    exports: [
        ProjectFolderFormComponent,
        CustomersComponent
    ],
    providers: [
        CustomersService,
        CrudCustomerService,
        CrudProspectService,
        ProspectResolverService,
        CustomerResolverService,
        ProjectFolderService,
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: []}

    ],
    entryComponents: [
        ProjectFolderFormComponent,
        CustomersComponent

    ]

})
export class CrmModule {
}
