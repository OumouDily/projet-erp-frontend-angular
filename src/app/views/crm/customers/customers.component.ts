import {Component, ElementRef, Inject, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {takeUntil} from 'rxjs/internal/operators';
import {CustomersService} from './customers.service';
import {FuseConfirmDialogComponent} from '../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {RoleUtils} from '../../../utils/role-utils';
import {User} from '../../../data/models/user.model';
import {SearchBody} from '../../../utils/search-body';
import {EntityListUtils} from '../../../utils/entity.list.utils';
import {Customer} from '../../../data/models/customer.model';

@Component({
    selector: 'crm-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class CustomersComponent implements OnInit {

    roleUtils = new RoleUtils();
    entityListUtils = new EntityListUtils();

    isGridView: boolean = true;

    customer: Customer;

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    searchBody = new SearchBody();

    action: string;
    dialogTitle: string;


    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    dataSource: FilesDataSource | null;
    displayedColumns = ['name', 'telephoneOne', 'email', 'addressOne', 'siteWeb', 'description', 'buttons'];

    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        public matDialogRef: MatDialogRef<CustomersComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _customersService: CustomersService,
        public _matDialog: MatDialog,
        private toast: ToastrService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Clients';

        this.dataSource = new FilesDataSource(this._customersService, this.paginator, this.sort);

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(100),
                distinctUntilChanged()
            )
            .subscribe((folder) => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });

        this._customersService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });

        this.action = this._data.action;

        if (this.action === 'list') {
            this.dialogTitle = 'Clients';
            this._customersService.getCustomers(this.searchBody)
        }

    }

    onDisplayChanged() {
        this.isGridView = !this.isGridView;
    }

    delete(customer): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimé ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._customersService.delete(customer.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);

                        const customerIndex = this._customersService.customers.indexOf(customer);
                        this._customersService.customers.splice(customerIndex, 1);
                        this._customersService.onCustomersChanged.next(this._customersService.customers);

                    } else {
                        this.toast.error(ret['message'] + '\nCe client a déjà un devis ou une facture à son nom');
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    onSearch(): void {
        this._customersService.getCustomers(this.searchBody);
    }

    onUserSelected(user?): void {
        this.selectedUser = user;
        this.searchBody.userId = user ? user.id : user;
        this.onSearch();
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }

}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    constructor(
        private _customersService: CustomersService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._customersService.customers;
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._customersService.onCustomersChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._customersService.customers.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'telephoneOne':
                    [propertyA, propertyB] = [a.telephoneOne, b.telephoneOne];
                    break;
                case 'email':
                    [propertyA, propertyB] = [a.email, b.email];
                    break;
                case 'addressOne':
                    [propertyA, propertyB] = [a.addressOne, b.addressOne];
                    break;
                case 'siteWeb':
                    [propertyA, propertyB] = [a.siteWeb, b.siteWeb];
                    break;
                case 'description':
                    [propertyA, propertyB] = [a.description, b.description];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect(): void {
    }
}
