import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Customer} from '../../../data/models/customer.model';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {SearchBody} from '../../../utils/search-body';
import {RoleUtils} from '../../../utils/role-utils';
import {User} from '../../../data/models/user.model';
import {UserService} from '../../../services/user.service';

@Injectable({
    providedIn: 'root'
})
export class CustomersService implements Resolve<any> {

    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();
    roleUtils = new RoleUtils();
    page: number = 0;

    customers: Customer[] = [];
    prospects: Customer[] = [];
    users: User[];

    onCustomersChanged: BehaviorSubject<any>;
    onProspectsChanged: BehaviorSubject<any>;

    onUsersChanged: BehaviorSubject<any>;


    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService,
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onCustomersChanged = new BehaviorSubject([]);
        this.onProspectsChanged = new BehaviorSubject([]);
        this.onUsersChanged = new BehaviorSubject([]);
        this.serviceURL = environment.serviceUrl + '/customers';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCustomers(null),
                this.getUsers(),
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get customers
     *
     * @returns {Promise<any>}
     */
    getCustomers(searchBody?: SearchBody): Promise<any> {
        if (searchBody==null) {
            searchBody = new SearchBody();
        }
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/allCustomers', searchBody, this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.customers = res['response'];
                        this.onCustomersChanged.next(this.customers);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }

    getProspects(searchBody?: SearchBody): Promise<any> {
        if (searchBody==null) {
            searchBody = new SearchBody();
        }
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/allProspects', searchBody, this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.prospects = res['response'];
                        this.onProspectsChanged.next(this.prospects);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }

    findAll(): Observable<HttpEvent<Customer[]>> {
        return this._httpClient.get<Customer[]>(this.serviceURL + '/all', this.httpOptions);
    }

    getById(id: number): Observable<HttpEvent<Customer>> {
        return this._httpClient.get<Customer>(this.serviceURL + '/' + id + '/all', this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    allCustomer() {
        this.getAllCustomers(this.page);
    }

    getAllProspects(page: number) {
        this.roleUtils = new RoleUtils();
        let searchBody = new SearchBody();
        searchBody.page = page;
        if (!this.roleUtils.canReadAllProspect()) {
            searchBody.userId = this.currentUser.id;
        }
        return this._httpClient.post(this.serviceURL + '/prospects', searchBody, this.httpOptions);
    }

    getAllCustomers(page: number) {
        this.roleUtils = new RoleUtils();
        let searchBody = new SearchBody();
        searchBody.page = page;
        if (this.roleUtils.canReadAllCustomer()) {
            searchBody.userId = this.currentUser.id;
        }
        return this._httpClient.post(this.serviceURL + '/clients', searchBody, this.httpOptions);
    }

    update(customer: Customer): Observable<any> {
        return this._httpClient.put(this.serviceURL + '/update', customer, this.httpOptions);
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {

            this._userService.list()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.users = ret['response'];
                        this.onUsersChanged.next(this.users);
                    }
                    resolve(this.users);
                });
        });
    }


}
