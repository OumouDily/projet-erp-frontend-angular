import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {CrudProspectService} from './crud-prospect.service';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../data/models/user.model';
import {BranchActivityService} from '../../setting/branch-activity/branch.activity.service';
import {environment} from '../../../../environments/environment';
import {Customer} from '../../../data/models/customer.model';
import {CUSTOMER_STATE, CUSTOMER_TYPE} from '../../../data/enums/enums';
import {DsnUtils} from '../../../utils/dsn-utils';

@Component({
    selector: 'crud-propsect',
    templateUrl: './crud-prospect.component.html',
    styleUrls: ['./crud-prospect.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CrudProspectComponent implements OnInit, OnDestroy {
    currentUser = new DsnUtils().getAppUser();
    prospect: Customer;
    branchs: BranchActivity[];
    users: User[];
    user: User;
    branchActivity: BranchActivity = null;
    dsnUtils = new DsnUtils();
    pageType: string;
    customerStates: any[];
    prospectForm = this._formBuilder.group({});

    typeProspect = CUSTOMER_TYPE['PROSPECT'];
    customerstate = CUSTOMER_STATE;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CrudProspectService} _crudProspectService
     * @param {BranchActivityService} _branchActivityService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     * @param {UserService} _userService
     * @param {ToastrService} toastr
     * @param {Router} _router
     */
    constructor(
        private _crudProspectService: CrudProspectService,
        private _branchActivityService: BranchActivityService,
        private _userService: UserService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _router: Router,
        private _matSnackBar: MatSnackBar,
        private toastr: ToastrService
    ) {
        // Set the default
        this.prospect = new Customer();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit(): void {
        document.title = 'DSN ERP | Prospects';

        this.customerStates = Object.keys(this.customerstate);
        this.prospectForm.addControl('id', new FormControl(''));
        this.prospectForm.addControl('name', new FormControl('', [Validators.required]));
        this.prospectForm.addControl('description', new FormControl(''));
        this.prospectForm.addControl('judicialNature', new FormControl(''));
        this.prospectForm.addControl('siteWeb', new FormControl(''));
        this.prospectForm.addControl('email', new FormControl('', [Validators.email]));
        this.prospectForm.addControl('telephoneOne', new FormControl('', [Validators.required]));
        this.prospectForm.addControl('telephoneTwo', new FormControl(''));
        this.prospectForm.addControl('addressOne', new FormControl('', [Validators.required]));
        this.prospectForm.addControl('contactName', new FormControl(''));
        this.prospectForm.addControl('telephoneContact', new FormControl(''));
        this.prospectForm.addControl('state', new FormControl('', [Validators.required]));
        this.prospectForm.addControl('enabled', new FormControl(''));
        this.prospectForm.addControl('createDate', new FormControl(''));
        this.prospectForm.addControl('branchActivity', new FormControl(''));
        this.prospectForm.addControl('agent', new FormControl('', [Validators.required]));
        this.getSectorOfActivities();
        this.getUsers();
        // Subscribe to update product on changes
        this._crudProspectService.onProspectChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(prospect => {
                if (prospect) {
                    if (prospect.branchActivity) {
                        this.onBranchSelected(prospect.branchActivity.id);
                        this.prospectForm.get('branchActivity').setValue(prospect.branchActivity.id);
                    }
                    if (prospect.agent) {
                        this.onUserSelected(prospect.agent.id);
                        this.prospectForm.get('agent').setValue(prospect.agent.id);
                    }
                    this.prospectForm.get('id').setValue(prospect.id);
                    this.prospectForm.get('name').setValue(prospect.name);
                    this.prospectForm.get('description').setValue(prospect.description);
                    this.prospectForm.get('judicialNature').setValue(prospect.judicialNature);
                    this.prospectForm.get('siteWeb').setValue(prospect.siteWeb);
                    this.prospectForm.get('email').setValue(prospect.email);
                    this.prospectForm.get('telephoneOne').setValue(prospect.telephoneOne);
                    this.prospectForm.get('telephoneTwo').setValue(prospect.telephoneTwo);
                    this.prospectForm.get('addressOne').setValue(prospect.addressOne);
                    this.prospectForm.get('contactName').setValue(prospect.contactName);
                    this.prospectForm.get('telephoneContact').setValue(prospect.telephoneContact);
                    this.prospectForm.get('state').setValue(prospect.state);
                    this.prospectForm.get('enabled').setValue(prospect.enabled);
                    this.prospectForm.get('createDate').setValue(new Date(prospect.createDate));
                    this.pageType = 'edit';
                    this.prospect = new Customer(prospect);
                } else {
                    this.pageType = 'new';
                    this.prospect = new Customer();
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    getSectorOfActivities() {
        return this._crudProspectService.getBranchActivities().subscribe(data => {
            this.branchs = data['response'];
        }, error => {
            // console.log(error);
        });
    }

    getUsers() {
        return this._userService.list().subscribe(data => {
            this.users = data['response'];
        }, error => {
            // console.log(error);
        });
    }

    saveOrEdit() {
        const prospect = new Customer();
        prospect.id = this.prospectForm.get('id').value;
        prospect.name = this.prospectForm.get('name').value;
        prospect.description = this.prospectForm.get('description').value;
        prospect.judicialNature = this.prospectForm.get('judicialNature').value;
        prospect.siteWeb = this.prospectForm.get('siteWeb').value;
        prospect.email = this.prospectForm.get('email').value;
        prospect.telephoneOne = this.prospectForm.get('telephoneOne').value;
        prospect.telephoneTwo = this.prospectForm.get('telephoneTwo').value;
        prospect.judicialNature = this.prospectForm.get('judicialNature').value;
        prospect.addressOne = this.prospectForm.get('addressOne').value;
        prospect.contactName = this.prospectForm.get('contactName').value;
        prospect.telephoneContact = this.prospectForm.get('telephoneContact').value;
        prospect.state = this.prospectForm.get('state').value;
        prospect.createDate = this.prospectForm.get('createDate').value;
        prospect.enabled = this.prospectForm.get('enabled').value;
        prospect.branchActivity = this.branchActivity;
        if (this.user) {
            prospect.agent = this.user;
        } else {
            prospect.agent = this.dsnUtils.getAppUser();
        }
        prospect.createBy = this.dsnUtils.getAppUser();
        prospect.type = this.typeProspect;

        if (!prospect.id) {
            prospect.createBy = this.currentUser;
            this._crudProspectService.saveProspect(prospect)
                .subscribe((response: any) => {
                    if (response['status'] == 'OK') {
                        this._crudProspectService.onProspectChanged.next(prospect);
                        this.toastr.success(response['message'], 'Prospect');
                        this._router.navigateByUrl('/views/crm/prospects');
                    } else {
                        this.toastr.error(response['message']);
                    }
                }, e => {
                    this.toastr.error(environment.errorMessage);
                    // console.log(e);
                });

        } else {
            prospect.updateBy = this.currentUser;
            this._crudProspectService.updateProspect(prospect)
                .subscribe((response: any) => {
                    if (response['status'] == 'OK') {
                        this._crudProspectService.onProspectChanged.next(prospect);
                        this.toastr.success(response['message'], 'Prospect');
                        this._router.navigateByUrl('/views/crm/prospects');
                    } else {
                        this.toastr.error(response['message']);
                    }
                }, e => {
                    this.toastr.error(environment.errorMessage);
                    // console.log(e);
                });
        }
    }

    onBranchSelected(id: number) {
        this._branchActivityService.getBranchById(id).subscribe(data => {
            this.branchActivity = data['response'];
        }, error => console.log(error));
    }

    onUserSelected(id: number) {
        this._userService.getUserById(id).subscribe(data => {
            this.user = data['response'];
        }, error => console.log(error));
    }
}
