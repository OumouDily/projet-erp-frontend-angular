import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Customer} from '../../../data/models/customer.model';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CrudProspectService implements Resolve<any> {
    routeParams: any;
    prospect: Customer;
    readonly httpOptions: any;
    readonly serviceURL: string;
    onProspectChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onProspectChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/customers';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProspect()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getProspect(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onProspectChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id + '/all/', this.httpOptions)
                    .subscribe((response: any) => {
                        this.prospect = response['response'];
                        this.onProspectChanged.next(this.prospect);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }


    saveProspect(prospect): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/save', prospect, this.httpOptions);
    }

    updateProspect(prospect): Observable<any> {
        return this._httpClient.put(this.serviceURL + '/update', prospect, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    public getBranchActivities(): Observable<HttpEvent<BranchActivity[]>> {
        return this._httpClient.get<BranchActivity[]>(environment.serviceUrl + '/branch-activities/all', this.httpOptions);
    }
}
