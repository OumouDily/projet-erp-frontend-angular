import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Customer} from '../../../data/models/customer.model';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class CrudCustomerService implements Resolve<any> {

    customer: Customer;
    readonly httpOptions: any;
    readonly serviceURL: string;
    onCustomerChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */ routeParams: any;
    constructor(
        private _httpClient: HttpClient,
        private toastr: ToastrService
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onCustomerChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/customers';

    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCustomer()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getCustomer(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onCustomerChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id + '/all/', this.httpOptions)
                    .subscribe((response: any) => {
                        this.customer = response['response'];
                        this.onCustomerChanged.next(this.customer);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

    saveCustomer(customer): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/save', customer, this.httpOptions);
    }

    updateCustomer(customer): Observable<any> {
        return this._httpClient.put(this.serviceURL + '/update', customer, this.httpOptions);
    }


    public getSectorOfActivities(): Observable<HttpEvent<BranchActivity[]>> {
        return this._httpClient.get<BranchActivity[]>(environment.serviceUrl + '/branch-activities/all', this.httpOptions);
    }

}
