import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {Customer} from '../../../data/models/customer.model';
import {CrudCustomerService} from './crud-customer.service';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {UserService} from '../../../services/user.service';
import {BranchActivityService} from '../../setting/branch-activity/branch.activity.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../data/models/user.model';
import {environment} from '../../../../environments/environment';
import {CUSTOMER_STATE, CUSTOMER_TYPE} from '../../../data/enums/enums';
import {DsnUtils} from '../../../utils/dsn-utils';

@Component({
    selector: 'crud-customer',
    templateUrl: './crud-customer.component.html',
    styleUrls: ['./crud-customer.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CrudCustomerComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();
    customer: Customer;
    branchs: BranchActivity[];
    users: User[];
    user: User;
    branchActivity: BranchActivity = null;
    pageType: string;
    customerForm = this._formBuilder.group({});

    typeCustomer = CUSTOMER_TYPE['CLIENT'];
    customerstate = CUSTOMER_STATE;
    customerStates: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CrudCustomerService} _crudCustomerService
     * @param {BranchActivityService} _branchActivityService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     * @param {UserService} _userService
     * @param {Router} _router
     */
    constructor(
        private _crudCustomerService: CrudCustomerService,
        private _branchActivityService: BranchActivityService,
        private _userService: UserService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _router: Router,
        private _matSnackBar: MatSnackBar,
        private toastr: ToastrService
    ) {
        // Set the default
        this.customer = new Customer();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit(): void {
        document.title = 'DSN ERP | Clients';

        this.customerStates = Object.keys(this.customerstate);
        this.customerForm.addControl('id', new FormControl(''));
        this.customerForm.addControl('name', new FormControl('', [Validators.required]));
        this.customerForm.addControl('description', new FormControl(''));
        this.customerForm.addControl('judicialNature', new FormControl(''));
        this.customerForm.addControl('siteWeb', new FormControl(''));
        this.customerForm.addControl('email', new FormControl('', [Validators.email]));
        this.customerForm.addControl('telephoneOne', new FormControl('', [Validators.required]));
        this.customerForm.addControl('telephoneTwo', new FormControl(''));
        this.customerForm.addControl('addressOne', new FormControl('', [Validators.required]));
        this.customerForm.addControl('contactName', new FormControl(''));
        this.customerForm.addControl('telephoneContact', new FormControl(''));
        this.customerForm.addControl('state', new FormControl('', [Validators.required]));
        this.customerForm.addControl('enabled', new FormControl(''));
        this.customerForm.addControl('createDate', new FormControl(''));
        this.customerForm.addControl('branchActivity', new FormControl(''));
        this.customerForm.addControl('agent', new FormControl('', [Validators.required]));
        this.getSectorOfActivities();
        this.getUsers();
        // Subscribe to update product on changes
        this._crudCustomerService.onCustomerChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customer => {
                if (customer) {
                    if (customer.branchActivity) {
                        this.onBranchSelected(customer.branchActivity.id);
                        this.customerForm.get('branchActivity').setValue(customer.branchActivity.id);
                    }
                    if (customer.agent) {
                        this.onUserSelected(customer.agent.id);
                        this.customerForm.get('agent').setValue(customer.agent.id);
                    }
                    this.customerForm.get('id').setValue(customer.id);
                    this.customerForm.get('name').setValue(customer.name);
                    this.customerForm.get('description').setValue(customer.description);
                    this.customerForm.get('judicialNature').setValue(customer.judicialNature);
                    this.customerForm.get('siteWeb').setValue(customer.siteWeb);
                    this.customerForm.get('email').setValue(customer.email);
                    this.customerForm.get('telephoneOne').setValue(customer.telephoneOne);
                    this.customerForm.get('telephoneTwo').setValue(customer.telephoneTwo);
                    this.customerForm.get('addressOne').setValue(customer.addressOne);
                    this.customerForm.get('contactName').setValue(customer.contactName);
                    this.customerForm.get('telephoneContact').setValue(customer.telephoneContact);
                    this.customerForm.get('state').setValue(customer.state);
                    this.customerForm.get('enabled').setValue(customer.enabled);
                    this.customerForm.get('createDate').setValue(new Date(customer.createDate));
                    this.pageType = 'edit';
                    this.customer = new Customer(customer);
                } else {
                    this.pageType = 'new';
                    this.customer = new Customer();
                }

            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    getSectorOfActivities() {
        return this._crudCustomerService.getSectorOfActivities().subscribe(data => {
            this.branchs = data['response'];
        }, error => {
            // console.log(error);
        });
    }

    getUsers() {
        return this._userService.list().subscribe(data => {
            this.users = data['response'];
        }, error => {
            // console.log(error);
        });
    }

    saveOrEdit() {
        const customer = new Customer();
        customer.id = this.customerForm.get('id').value;
        customer.name = this.customerForm.get('name').value;
        customer.description = this.customerForm.get('description').value;
        customer.judicialNature = this.customerForm.get('judicialNature').value;
        customer.siteWeb = this.customerForm.get('siteWeb').value;
        customer.email = this.customerForm.get('email').value;
        customer.telephoneOne = this.customerForm.get('telephoneOne').value;
        customer.telephoneTwo = this.customerForm.get('telephoneTwo').value;
        customer.addressOne = this.customerForm.get('addressOne').value;
        customer.contactName = this.customerForm.get('contactName').value;
        customer.telephoneContact = this.customerForm.get('telephoneContact').value;
        customer.state = this.customerForm.get('state').value;
        customer.createDate = this.customerForm.get('createDate').value;
        customer.enabled = this.customerForm.get('enabled').value;
        customer.branchActivity = this.branchActivity;
        if (this.user) {
            customer.agent = this.user;
        } else {
            customer.agent = this.dsnUtils.getAppUser();
        }
        customer.type = this.typeCustomer;

        if (!customer.id) {
            customer.createBy = this.currentUser;
            this._crudCustomerService.saveCustomer(customer)
                .subscribe((response: any) => {
                    if (response['status'] == 'OK') {
                        this._crudCustomerService.onCustomerChanged.next(customer);
                        this.toastr.success(response['message'], 'Client');
                        this._router.navigateByUrl('/views/crm/customers');
                    } else {
                        this.toastr.error(response['message']);
                    }
                }, e => {
                    this.toastr.error(environment.errorMessage);
                    // console.log(e);
                });

        } else {
            customer.updateBy = this.currentUser;
            this._crudCustomerService.updateCustomer(customer)
                .subscribe((response: any) => {
                    if (response['status'] == 'OK') {
                        this._crudCustomerService.onCustomerChanged.next(customer);
                        this.toastr.success(response['message'], 'Client');
                        this._router.navigateByUrl('/views/crm/customers');
                    } else {
                        this.toastr.error(response['message']);
                    }
                }, e => {
                    this.toastr.error(environment.errorMessage);
                    // console.log(e);
                });
        }
    }

    onBranchSelected(id: number) {
        this._branchActivityService.getBranchById(id).subscribe(data => {
            this.branchActivity = data['response'];
        }, error => console.log(error));
    }

    onUserSelected(id: number) {
        this._userService.getUserById(id).subscribe(data => {
            this.user = data['response'];
        }, error => console.log(error));
    }
}
