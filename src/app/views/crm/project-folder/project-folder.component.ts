import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {ProjectFolderService} from './project-folder.service';
import {takeUntil} from 'rxjs/internal/operators';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {FuseConfirmDialogComponent} from '../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {PROJECT_FOLDER_PRIORITY, PROJECT_FOLDER_STATE} from '../../../data/enums/enums';
import {RoleUtils} from '../../../utils/role-utils';
import {ProjectFolderFormComponent} from '../project-folder-form/project-folder-form.component';
import {ProjectFolder} from 'app/data/models/project.folder.model';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Customer} from '../../../data/models/customer.model';
import {User} from 'app/data/models/user.model';
import {SearchBody} from '../../../utils/search-body';
import {EntityListUtils} from '../../../utils/entity.list.utils';


@Component({
    selector: 'project-folder',
    templateUrl: './project-folder.component.html',
    styleUrls: ['./project-folder.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class ProjectFolderComponent implements OnInit {

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    entityListUtils = new EntityListUtils();

    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    dataSource: FilesDataSource | null;
    displayedColumns = ['customer', 'name', 'priority', 'chanceRate', 'state', 'clientBudget', 'ca', 'agent', 'notes', 'buttons'];

    projectFolderState = PROJECT_FOLDER_STATE;
    projectFolderPriority = PROJECT_FOLDER_PRIORITY;

    folders: any[];

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    customers: Customer[];
    selectedCustomer: Customer;
    filteredCustomers: Customer[];


    searchBody = new SearchBody();

    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _projectFolderService: ProjectFolderService,
        public _matDialog: MatDialog,
        private toast: ToastrService
    ) {
        // Set the private defaults
        this.users = [];
        this.customers = [];
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Dossier projet';

        this.dataSource = new FilesDataSource(this._projectFolderService, this.paginator, this.sort);


        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(100),
                distinctUntilChanged()
            )
            .subscribe((folder) => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });

        this._projectFolderService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });


        this._projectFolderService.onCustomersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customers => {
                this.customers = customers;
                this.filteredCustomers = this.customers;
            });

    }

    onSearch(): void {
        this._projectFolderService.getProjectFolders(this.searchBody);
    }

    showFolderDialog(action?: string, folder?: ProjectFolder): void {

        if (action === 'edit' && !this.roleUtils.canUpdateProjectFolder()) {
            return;
        }

        this.dialogRef = this._matDialog.open(ProjectFolderFormComponent, {
            panelClass: 'project-folder-form-dialog',
            data: {
                folder: folder,
                action: action
            }
        });

        this.dialogRef.afterClosed().subscribe(response => {
        });
    }

    delete(folder): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûre de vouloir supprimé ?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._projectFolderService.delete(folder.id).subscribe(ret => {
                    if (ret['status'] == 'OK') {
                        this.toast.success(ret['message']);

                        const customerIndex = this._projectFolderService.projectFolders.indexOf(folder);
                        this._projectFolderService.projectFolders.splice(customerIndex, 1);
                        this._projectFolderService.onProjectFoldersChanged.next(this._projectFolderService.projectFolders);

                    } else {

                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }


    onCustomerSelected(customer?, user?): void {
        this.selectedCustomer = customer;
        this.searchBody.customerId = customer ? customer.id : customer;
        this.onSearch();
    }

    onUserSelected(user?): void {
        this.selectedUser = user;
        this.searchBody.userId = user ? user.id : user;
        this.onSearch();
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'customer') {
            this.filteredCustomers = this.entityListUtils.searchCustomer(query, this.customers);
        } else if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }

}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    constructor(
        private _projectFolderService: ProjectFolderService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._projectFolderService.projectFolders;
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._projectFolderService.onProjectFoldersChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._projectFolderService.projectFolders.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'customer':
                    [propertyA, propertyB] = [a.customer.id, b.customer.id];
                    break;
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'priority':
                    [propertyA, propertyB] = [a.priority, b.priority];
                    break;
                case 'chanceRate':
                    [propertyA, propertyB] = [a.chanceRate, b.chanceRate];
                    break;
                case 'state':
                    [propertyA, propertyB] = [a.state, b.state];
                    break;
                case 'clientBudget':
                    [propertyA, propertyB] = [a.clientBudget, b.clientBudget];
                    break;
                case 'ca':
                    [propertyA, propertyB] = [a.ca, b.ca];
                    break;
                case 'agent':
                    [propertyA, propertyB] = [a.agent.id, b.agent.id];
                    break;
                case 'notes':
                    [propertyA, propertyB] = [a.notes, b.notes];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
