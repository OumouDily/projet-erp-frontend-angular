import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {ProjectFolder} from '../../../data/models/project.folder.model';
import {SearchBody} from '../../../utils/search-body';
import {RoleUtils} from '../../../utils/role-utils';
import {UserService} from '../../../services/user.service';
import {User} from '../../../data/models/user.model';
import {Customer} from '../../../data/models/customer.model';
import {CustomersService} from '../customers/customers.service';

@Injectable({
    providedIn: 'root'
})
export class ProjectFolderService implements Resolve<any> {

    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();
    roleUtils = new RoleUtils();

    users: User[];
    customers: Customer[];

    onUsersChanged: BehaviorSubject<any>;
    onCustomersChanged: BehaviorSubject<any>;

    page: number = 0;
    projectFolders: ProjectFolder[];
    onProjectFoldersChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */ routeParams: any;

    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService,
        private _customersService: CustomersService,
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        this.serviceURL = environment.serviceUrl + '/project-folder';

        // Set the defaults
        this.onProjectFoldersChanged = new BehaviorSubject([]);

        this.onUsersChanged = new BehaviorSubject([]);
        this.onCustomersChanged = new BehaviorSubject([]);
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getProjectFolders(null),
                this.getUsers(),
                this.getCustomers()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProjectFolders(searchBody?: SearchBody): Promise<any> {
        this.roleUtils = new RoleUtils();
        if (searchBody==null) {
            searchBody = new SearchBody();
        }
        if (!this.roleUtils.canReadAllProjectFolder()) {
            searchBody.userId = this.currentUser.id;
        }
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/all', searchBody, this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.projectFolders = res['response'];
                        this.onProjectFoldersChanged.next(this.projectFolders);
                        resolve(res['response']);
                    } else {
                        // console.log('error : ' + res['status']);
                    }
                }, reject);
        });
    }

    public delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }


    editProjectFolder(folder): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.put(this.serviceURL + '/update', folder, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }

    saveProjectFolder(folder): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/save', folder, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {

            this._userService.list()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.users = ret['response'];
                        this.onUsersChanged.next(this.users);
                    }
                    resolve(this.users);
                });
        });
    }

    getCustomers(): any {
        return new Promise((resolve, reject) => {

            this._customersService.findAll()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.customers = ret['response'];
                        this.onCustomersChanged.next(this.customers);
                    }
                    resolve(this.customers);
                });
        });
    }

}
