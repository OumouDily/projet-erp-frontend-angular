import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {PROJECT_FOLDER_PRIORITY, PROJECT_FOLDER_STATE} from '../../../data/enums/enums';
import {Subject} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {ProjectFolder} from '../../../data/models/project.folder.model';
import {Customer} from '../../../data/models/customer.model';
import {CustomersService} from '../customers/customers.service';
import {ProjectFolderService} from '../project-folder/project-folder.service';
import {User} from '../../../data/models/user.model';
import {UserService} from '../../../services/user.service';

@Component({
    selector: 'project-folder-form-dialog',
    templateUrl: './project-folder-form.component.html',
    styleUrls: ['./project-folder-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProjectFolderFormComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();

    action: string;
    dialogTitle: string;

    projectFolder: ProjectFolder;

    folderState = PROJECT_FOLDER_STATE;
    folderPriority = PROJECT_FOLDER_PRIORITY;
    folderStates: any[];
    folderPriorities: any[];

    customers: Customer[];
    customer: Customer;

    users: User[];
    currentUser = this.dsnUtils.getAppUser();

    projectFolderForm: FormGroup;
    chanceRate=0;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<ProjectFolderFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     * @param {ProjectFolderService} _projectFolderService
     * @param {CustomersService} _customerService
     * @param {UserService} _userService
     * @param _toastrService
     * @param _router
     */
    constructor(
        public matDialogRef: MatDialogRef<ProjectFolderFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _projectFolderService: ProjectFolderService,
        private _customerService: CustomersService,
        private _userService: UserService,
        private _toastrService: ToastrService,
        private _router: Router
    ) {

        this._unsubscribeAll = new Subject();

        // Set the defaults
    }

    /**
     * On init
     */
    ngOnInit(): void {
        document.title = 'DSN ERP | Dossier projet';
        this.folderStates = Object.keys(this.folderState);
        this.folderPriorities = Object.keys(this.folderPriority);

        this.getUsers();
        this.findAllProspectCustomer();

        this.action = this._data.action;
        this.projectFolder = new ProjectFolder();

        if (this.action === 'add') {
            this.dialogTitle = 'Nouveau dossier';
        } else if (this.action === 'edit') {
            this.dialogTitle = 'Mise à jour dossier';
            this.projectFolder = this._data.folder;
        }

        this.initProjectFolderForm();
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    initProjectFolderForm() {
        this.projectFolderForm = this._formBuilder.group({
            id: new FormControl(this.projectFolder.id),
            customer: new FormControl(this.projectFolder.customer, [Validators.required]),
            name: new FormControl(this.projectFolder.name, [Validators.required]),
            turnover: new FormControl(this.projectFolder.turnover, Validators.required),
            clientBudget: new FormControl(this.projectFolder.clientBudget, Validators.required),
            state: new FormControl(this.projectFolder.state, [Validators.required]),
            priority: new FormControl(this.projectFolder.priority, [Validators.required]),
            chanceRate: new FormControl(this.projectFolder.chanceRate),
            agent: new FormControl(this.projectFolder.agent, [Validators.required]),
            startDate: new FormControl(this.projectFolder.startDate),
            endDate: new FormControl(this.projectFolder.endDate),
            note: new FormControl(this.projectFolder.note),

        });
    }

    getUsers() {
        return this._userService.list().subscribe(data => {
            this.users = data['response'];
        }, error => {
            // console.log(error);
        });
    }

    findAllProspectCustomer() {
        return this._customerService.findAll().subscribe(value => {
            this.customers = value['response'];
        }, error => console.log(error));
    }

    save(): void {
        this.projectFolder = this.projectFolderForm.getRawValue();

        if (this.action === 'add') {
            this.projectFolder.createBy = this.currentUser;
            this._projectFolderService.saveProjectFolder(this.projectFolder)
                .then(() => {
                    // Show the success message
                    this._toastrService.success('Dossier crée avec succès !');
                    this._projectFolderService.getProjectFolders(null);
                    this.matDialogRef.close();
                });
        } else if (this.action === 'edit') {
            this.projectFolder.updateBy = this.currentUser;
            this._projectFolderService.editProjectFolder(this.projectFolder).then(() => {
                // Show the success message
                this._toastrService.success('Dossier modifié avec succès !');
                this._projectFolderService.getProjectFolders(null);
                this.matDialogRef.close();
            });
        }
    }

    formatLabel(value: number | null) {
        if (!value) {
            return 0;
        }

        if (value >= 0) {
            return value + '%';
        }

        return value;
    }

}
