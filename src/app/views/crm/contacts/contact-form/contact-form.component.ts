import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Contact} from '../../../../data/models/contact.model';
import {CountryISO, SearchCountryField, TooltipLabel} from 'ngx-intl-tel-input';


@Component({
    selector: 'contacts-contact-form-dialog',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent {
    action: string;
    contact: Contact;
    contactForm: FormGroup;
    dialogTitle: string;


    phoneForm = new FormGroup({
        phone: new FormControl(undefined, [Validators.required])
    });

    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Mise à jour Contact';
            this.contact = _data.contact;
        } else {
            this.dialogTitle = 'Nouveau Contact';
            this.contact = new Contact({});
        }

        this.contactForm = this.createContactForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.contact.id],
            name: [this.contact.name],
            phoneOne: new FormControl(this.contact.phoneOne, [Validators.required]),
            phoneTwo: new FormControl(this.contact.phoneTwo),
            email: new FormControl(this.contact.email, [Validators.email]),
            address: [this.contact.address],
            company: [this.contact.company],
            job: [this.contact.job],
            notes: [this.contact.notes],
            country: [this.contact.country]
        });
    }

    onCountryChanged(event) {
        console.log(JSON.stringify(event));
    }
}
