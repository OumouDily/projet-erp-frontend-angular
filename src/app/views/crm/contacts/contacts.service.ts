import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { FuseUtils } from '@fuse/utils';

import {DsnUtils} from '../../../utils/dsn-utils';
import {Contact} from '../../../data/models/contact.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ContactsService implements Resolve<any>
{
    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();

    readonly serviceURL: string;
    readonly httpOptions: any;

    onContactsChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;

    contacts: Contact[];

    searchText: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        this.serviceURL = environment.serviceUrl + '/contacts';
        this.httpOptions = new DsnUtils().httpHeaders();

        // Set the defaults
        this.onContactsChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getContacts(),
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getContacts();
                    });

                    resolve();

                },
                reject
            );
        });
    }

    /**
     * Get contacts
     *
     * @returns {Promise<any>}
     */
    getContacts(): Promise<any> {
        return new Promise((resolve, reject) => {

                this._httpClient.get(this.serviceURL + '/all/' + 0, this.httpOptions)
                    .subscribe((ret: any) => {

                        this.contacts = ret['response'];

                        if ( this.searchText && this.searchText !== '' )
                        {
                            this.contacts = FuseUtils.filterArrayByString(this.contacts, this.searchText);
                        }

                        this.contacts = this.contacts.map(contact => {
                            return new Contact(contact);
                        });

                        this.onContactsChanged.next(this.contacts);
                        resolve(this.contacts);
                    }, reject);
            }
        );
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
    updateContact(contact): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.put(this.serviceURL + '/update', contact, this.httpOptions)
                .subscribe(ret => {
                    this.getContacts();
                    resolve(ret['response']);
                });
        });
    }


    /**
     * Delete contact
     *
     * @param contact
     */
    deleteContact(contact: Contact): void
    {   this._httpClient.delete(this.serviceURL + '/' + contact.id + '/delete', this.httpOptions)
        .subscribe(ret=>{
            if (ret['status']==='OK') {
                const contactIndex = this.contacts.indexOf(contact);
                this.contacts.splice(contactIndex, 1);
                this.onContactsChanged.next(this.contacts);
            }
        });
    }


    save(contact: Contact): Observable<any> {
        return this._httpClient.post(this.serviceURL + '/save', contact, this.httpOptions);
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(this.serviceURL + '/' + id + '/delete', this.httpOptions);
    }

    get(id: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/' + id + '/get', this.httpOptions);
    }

    find(customerId: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/all/' + customerId, this.httpOptions);
    }
}
