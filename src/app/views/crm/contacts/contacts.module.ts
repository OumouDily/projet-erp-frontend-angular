import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatTableModule,
    MatToolbarModule
} from '@angular/material';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseSidebarModule} from '@fuse/components';

import {ContactsComponent} from './contacts.component';
import {ContactsService} from './contacts.service';
import {ContactsContactListComponent} from './contact-list/contact-list.component';
import {ContactsContactFormDialogComponent} from './contact-form/contact-form.component';
import {SpinnerModule} from '../../../shared/modules/spinner.module';
import {NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';


const routes: Routes = [
    {
        path: '**',
        component: ContactsComponent,
        resolve: {
            contacts: ContactsService
        }
    }
];

@NgModule({
    declarations: [
        ContactsComponent,
        ContactsContactListComponent,
        ContactsContactFormDialogComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        SpinnerModule,
        NgxMatIntlTelInputModule
    ],
    providers: [],
    entryComponents: [
        ContactsContactFormDialogComponent
    ]
})
export class ContactsModule {
}
