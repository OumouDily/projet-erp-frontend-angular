import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BranchActivitiesService implements Resolve<any> {
    sectors: BranchActivity[];
    onSectorActivitiesChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.serviceURL = environment.serviceUrl + '/branch-activities';
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onSectorActivitiesChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getSectorActivities()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get customers
     *
     * @returns {Promise<any>}
     */
    getSectorActivities(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/all', this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.sectors = res['response'];
                        this.onSectorActivitiesChanged.next(this.sectors);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }


}
