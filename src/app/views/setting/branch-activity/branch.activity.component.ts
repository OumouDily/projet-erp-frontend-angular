import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {BranchActivityService} from './branch.activity.service';
import {BranchActivity} from '../../../data/models/branch.activity.model';


@Component({
    selector: 'setting-branch.activity',
    templateUrl: './branch.activity.component.html',
    styleUrls: ['./branch.activity.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class BranchActivityComponent implements OnInit, OnDestroy {
    branchActivity: BranchActivity;
    pageType: string;
    branchActivityForm: FormGroup;


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {BranchActivityService} _branchActivityService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _branchActivityService: BranchActivityService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar
    ) {
        // Set the default
        this.branchActivity = new BranchActivity();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this._branchActivityService.onBranchActivityChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(branchActivity => {
                if (branchActivity) {
                    this.pageType = 'edit';
                    this.branchActivity = new BranchActivity(branchActivity);

                } else {
                    this.pageType = 'new';
                    this.branchActivity = new BranchActivity();
                }
                this.branchActivityForm = this.createBranchActivityForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createBranchActivityForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.branchActivity.id],
            name: [this.branchActivity.name, Validators.required],
            description: [this.branchActivity.description],
            enabled: [this.branchActivity.enabled]
        });
    }


    /**
     * Save product
     */
    edit(): void {
        const data = this.branchActivityForm.getRawValue();
        this._branchActivityService.editSector(data)
            .then(() => {

                // Trigger the subscription with new data
                this._branchActivityService.onBranchActivityChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Modifié avec succès', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
            });
    }

    /**
     * Add product
     */
    save(): void {
        const data = this.branchActivityForm.getRawValue();
        //data.name = FuseUtils.handleize(data.name);
        data.enabled = true;
        this._branchActivityService.addSector(data)
            .then(() => {

                // Trigger the subscription with new data
                this._branchActivityService.onBranchActivityChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Ajouté avec succès', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });

                // Change the location with new one
                this._location.go('views/setting/sector-activities/' + this.branchActivity.id + '/' + this.branchActivity.name);
            });
    }

}
