import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {BranchActivity} from '../../../data/models/branch.activity.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BranchActivityService implements Resolve<any> {
    routeParams: any;
    sector: BranchActivity;
    readonly httpOptions: any;
    onBranchActivityChanged: BehaviorSubject<any>;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onBranchActivityChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/branch-activities';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getBranchActivity()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getBranchActivity(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onBranchActivityChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.sector = response['response'];
                        this.onBranchActivityChanged.next(this.sector);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

    /**
     * Edit sector
     *
     * @param sector
     * @returns {Promise<any>}
     */
    editSector(sector): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.put(this.serviceURL + '/update', sector, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }

    /**
     * Add sector
     *
     * @param sector
     * @returns {Promise<any>}
     */
    addSector(sector): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/save', sector, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }



    getBranchById(id: number): Observable<any> {
        return this._httpClient.get(this.serviceURL + '/' + id + '/getBranchActivity', this.httpOptions);
    }

}
