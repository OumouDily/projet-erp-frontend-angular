import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {DsnUtils} from '../../../utils/dsn-utils';
import {QuoteTrace} from '../../../data/wrapper/quote.trace.model';

@Injectable({
    providedIn: 'root'
})
export class QuoteTracesService implements Resolve<any> {
    quotes: QuoteTrace[];
    onQuotesChanged: BehaviorSubject<any>;

    readonly serviceURL: string;
    readonly httpOptions: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {

        this.serviceURL = environment.serviceUrl + '/quotes';
        this.httpOptions = new DsnUtils().httpHeaders();

        // Set the defaults
        this.onQuotesChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getQuotes()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get profiles
     *
     * @returns {Promise<any>}
     */
    getQuotes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/all', this.httpOptions)
                .subscribe((resBody: any) => {

                    if (resBody['status'] === 'OK') {
                        this.quotes = resBody['response'];
                        this.onQuotesChanged.next(this.quotes);
                        resolve(resBody['response']);
                    } else {
                    }
                }, reject);
        });
    }
}
