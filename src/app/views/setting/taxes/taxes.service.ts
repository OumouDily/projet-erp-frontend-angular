import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Tax} from '../../../data/models/tax.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TaxesService implements Resolve<any> {
    taxes: Tax[];
    onTaxesChanged: BehaviorSubject<any>;
    readonly httpOptions: any;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onTaxesChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/taxes';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getTaxes()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get customers
     *
     * @returns {Promise<any>}
     */
    getTaxes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(this.serviceURL + '/all', this.httpOptions)
                .subscribe((res: any) => {
                    if (res['status'] === 'OK') {
                        this.taxes = res['response'];
                        this.onTaxesChanged.next(this.taxes);
                        resolve(res['response']);
                    } else {
                    }
                }, reject);
        });
    }

    findAll() {
        return this._httpClient.get(this.serviceURL + '/all', this.httpOptions);
    }

}
