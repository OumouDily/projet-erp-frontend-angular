import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AgmCoreModule} from '@agm/core';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';
import {BranchActivitiesComponent} from './branch-activities/branch.activities.component';
import {BranchActivitiesService} from './branch-activities/branch.activities.service';
import {QuoteTracesService} from './quote-traces/quote-traces.service';
import {QuoteTracesComponent} from './quote-traces/quote-traces.component';
import {TaxesComponent} from './taxes/taxes.component';
import {TaxesService} from './taxes/taxes.service';
import {TaxeComponent} from './taxe/taxe.component';
import {TaxService} from './taxe/tax.service';
import {BranchActivityComponent} from './branch-activity/branch.activity.component';
import {BranchActivityService} from './branch-activity/branch.activity.service';
import {SpinnerModule} from '../../shared/modules/spinner.module';

const routes: Routes = [
    {
        path: 'sector-activities',
        component: BranchActivitiesComponent,
        resolve: {
            data: BranchActivitiesService
        }
    },
    {
        path: 'sector-activities/:id/:nom',
        component: BranchActivityComponent,
        resolve: {
            data: BranchActivityService
        }
    },
    {
        path: 'sector-activities/:id',
        component: BranchActivityComponent,
        resolve: {
            data: BranchActivityService
        }
    },
    {
        path: 'taxes',
        component: TaxesComponent,
        resolve: {
            data: TaxesService
        }
    },
    {
        path: 'taxes/:id/:libelle',
        component: TaxeComponent,
        resolve: {
            data: TaxService
        }
    },
    {
        path: 'taxes/:id',
        component: TaxeComponent,
        resolve: {
            data: TaxService
        }
    },
    {
        path: 'quote-traces',
        component: QuoteTracesComponent,
        resolve: {
            data: QuoteTracesService
        }
    }
];

@NgModule({
    declarations: [
        BranchActivitiesComponent,
        BranchActivityComponent,
        QuoteTracesComponent,
        TaxesComponent,
        TaxeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatChipsModule,
        MatGridListModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatListModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule,
        MatCardModule,
        SpinnerModule
    ],
    providers: []
})
export class SettingModule {
}
