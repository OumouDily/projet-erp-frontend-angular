import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {TaxService} from './tax.service';
import {Tax} from '../../../data/models/tax.model';
import {ToastrService} from 'ngx-toastr';


@Component({
    selector: 'setting-taxe',
    templateUrl: './taxe.component.html',
    styleUrls: ['./taxe.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TaxeComponent implements OnInit, OnDestroy {
    tax: Tax;
    pageType: string;
    taxForm: FormGroup;


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TaxService} _taxService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _taxService: TaxService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private toastr: ToastrService
    ) {
        // Set the default
        this.tax = new Tax();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this._taxService.onTaxeChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(tax => {
                if (tax) {
                    this.pageType = 'edit';
                    this.tax = new Tax(tax);

                } else {
                    this.pageType = 'new';
                    this.tax = new Tax();
                }
                this.taxForm = this.createTaxeForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createTaxeForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.tax.id],
            libelle: [this.tax.libelle],
            value: [this.tax.value]
        });
    }


    /**
     * Save product
     */
    edit(): void {
        const data = this.taxForm.getRawValue();
        this._taxService.editTaxe(data)
            .then(() => {

                // Trigger the subscription with new data
                this._taxService.onTaxeChanged.next(data);

                // Show the success message
                this.toastr.success('Modifié avec succès!', 'Modification');
            });
    }

    /**
     * Add product
     */
    save(): void {
        const data = this.taxForm.getRawValue();
        data.enabled = true;
        this._taxService.addTaxe(data)
            .then(() => {

                // Trigger the subscription with new data
                this._taxService.onTaxeChanged.next(data);

                // Show the success message
                this.toastr.success('Ajout effectué avec succès!', 'Insertion');

            });
    }

}
