import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {DsnUtils} from '../../../utils/dsn-utils';
import {Tax} from '../../../data/models/tax.model';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TaxService implements Resolve<any> {
    routeParams: any;
    taxe: Tax;
    readonly httpOptions: any;
    onTaxeChanged: BehaviorSubject<any>;
    readonly serviceURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        // Set the defaults
        this.onTaxeChanged = new BehaviorSubject({});
        this.serviceURL = environment.serviceUrl + '/taxes';
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getTaxe()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get taxe
     *
     * @returns {Promise<any>}
     */
    getTaxe(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onTaxeChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(this.serviceURL + '/' + this.routeParams.id, this.httpOptions)
                    .subscribe((response: any) => {
                        this.taxe = response['response'];
                        // console.log(this.taxe);
                        this.onTaxeChanged.next(this.taxe);
                        resolve(response['response']);
                    }, reject);
            }
        });
    }

    /**
     * Edit taxe
     *
     * @param taxe
     * @returns {Promise<any>}
     */
    editTaxe(taxe): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.put(this.serviceURL + '/update', taxe, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }

    /**
     * Add taxe
     *
     * @param taxe
     * @returns {Promise<any>}
     */
    addTaxe(taxe): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceURL + '/save', taxe, this.httpOptions)
                .subscribe((response: any) => {
                    resolve(response['response']);
                }, reject);
        });
    }


}
