import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule, MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    MatTooltipModule
} from '@angular/material';
import {NgxDnDModule} from '@swimlane/ngx-dnd';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule} from '@fuse/components';
import {TaskComponent} from './task.component';
import {TaskService} from './task.service';
import {TaskMainSidebarComponent} from './sidebars/main/main-sidebar.component';
import {TaskListItemComponent} from './task-list/task-list-item/task-list-item.component';
import {TaskListComponent} from './task-list/task-list.component';
import {TaskDetailsComponent} from './task-details/task-details.component';
import {SpinnerModule} from '../../shared/modules/spinner.module';
import {TaskCreateGuard, TaskMenuGuard, TaskUpdateGuard} from '../../shared/guard/role.guard';


const routes: Routes = [
    {
        path: 'all',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskMenuGuard, TaskCreateGuard, TaskUpdateGuard]
    },
    {
        path: 'current/:taskId',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskCreateGuard]
    },
    {
        path: 'priority/:priorityHandle',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskMenuGuard]
    },
    {
        path: 'priority/:priorityHandle/:taskId',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskMenuGuard]
    },
    {
        path: 'state/:stateHandle',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskMenuGuard]
    },
    {
        path: 'state/:stateHandle/:taskId',
        component: TaskComponent,
        resolve: {
            task: TaskService
        },
        canActivate: [TaskMenuGuard]
    },
    {
        path: '',
        redirectTo: 'all',
        canActivate: [TaskMenuGuard]
    }
];

@NgModule({
    declarations: [
        TaskComponent,
        TaskMainSidebarComponent,
        TaskListItemComponent,
        TaskListComponent,
        TaskDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTooltipModule,
        MatDividerModule,

        NgxDnDModule,

        FuseSharedModule,
        FuseSidebarModule,

        SpinnerModule
    ],
    providers: [
        TaskService
    ]
})
export class TaskModule {
}
