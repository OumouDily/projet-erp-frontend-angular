import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {TaskService} from '../../task.service';
import {User} from '../../../../data/models/user.model';
import {TASK_PRIORITY, TASK_STATE} from '../../../../data/enums/enums';
import {DsnUtils} from '../../../../utils/dsn-utils';
import {Customer} from '../../../../data/models/customer.model';
import {Task} from '../../../../data/models/task.model';
import {EntityListUtils} from '../../../../utils/entity.list.utils';
import {RoleUtils} from '../../../../utils/role-utils';


@Component({
    selector: 'task-main-sidebar',
    templateUrl: './main-sidebar.component.html',
    styleUrls: ['./main-sidebar.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TaskMainSidebarComponent implements OnInit, OnDestroy {

    roleUtils = new RoleUtils();
    dsnUtils = new DsnUtils();
    entityListUtils = new EntityListUtils();

    currentUser = this.dsnUtils.getAppUser();

    currentTask: Task;

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    customers: Customer[];
    selectedCustomer: Customer;
    filteredCustomers: Customer[];

    stateTask = TASK_STATE;
    priorityTask = TASK_PRIORITY;

    states: any[];
    priorities: any[];


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TaskService} _taskService
     * @param {Router} _router
     */
    constructor(
        private _taskService: TaskService,
        private _router: Router
    ) {
        // Set the defaults
        this.users = [];
        this.customers = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._taskService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });


        this._taskService.onSelectedUserChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.selectedUser = user;
            });

        this._taskService.onCustomersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customers => {
                this.customers = customers;
                this.filteredCustomers = this.customers;
            });


        this._taskService.onSelectedCustomerChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customer => {
                this.selectedCustomer = customer;
            });

        this._taskService.onStatesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(states => {
                this.states = states;
            });

        this._taskService.onPrioritiesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(priorities => {
                this.priorities = priorities;
            });

        this._taskService.onCurrentTaskChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([currentTask, formType]) => {
                if (!currentTask) {
                    this.currentTask = null;
                } else {
                    this.currentTask = currentTask;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * New task
     */
    newTask(): void {
        this._router.navigate(['/views/tasks/all']).then(() => {
            setTimeout(() => {
                this._taskService.onNewTaskClicked.next('');
            });
        });
    }

    toggleUserOnTask(user): void {
        this.deselectCurrentTask();
        this.selectedUser = user;
        this._taskService.onUserSelected(this.selectedUser);
    }

    toggleCustomerOnTask(customer): void {
        this.deselectCurrentTask();
        this.selectedCustomer = customer;
        this._taskService.onCustomerSelected(this.selectedCustomer);
    }

    deselectCurrentTask(): void {
        this._taskService.onCurrentTaskChanged.next([null, null]);
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        } else if (tag === 'customer') {
            this.filteredCustomers = this.entityListUtils.searchCustomer(query, this.customers);
        }
    }
}
