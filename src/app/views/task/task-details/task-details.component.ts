import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';

import {TaskService} from '../task.service';
import {Task} from '../../../data/models/task.model';
import {TASK_ACTION, TASK_PRIORITY, TASK_STATE} from '../../../data/enums/enums';
import {Router} from '@angular/router';
import {DsnUtils} from '../../../utils/dsn-utils';
import {User} from '../../../data/models/user.model';
import {Customer} from '../../../data/models/customer.model';
import {EntityListUtils} from '../../../utils/entity.list.utils';
import {MatDatepickerInputEvent} from '@angular/material';
import {RoleUtils} from '../../../utils/role-utils';

@Component({
    selector: 'task-details',
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TaskDetailsComponent implements OnInit, OnDestroy {

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    entityListUtils = new EntityListUtils();
    currentUser = this.dsnUtils.getAppUser();

    task: Task;

    priorities: any[];
    states: any[];
    actions: any[];

    users: User[];
    customers: Customer[];

    filteredUsers: User[];
    filteredCustomers: Customer[];

    formType: string;
    taskForm: FormGroup;

    stateTask = TASK_STATE;
    priorityTask = TASK_PRIORITY;
    actionTask = TASK_ACTION;

    minDate: Date;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TaskService} _taskService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _taskService: TaskService,
        private _router: Router,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.actions = Object.keys(TASK_ACTION);

        // Subscribe to update the current task
        this._taskService.onCurrentTaskChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([task, formType]) => {

                if (task && task.id && formType === 'edit') {
                    this.formType = 'edit';
                    this.task = task;
                    this.taskForm = this.createTaskForm();
                    this.minDate = this.taskForm.get('startDate').value;

                    this.taskForm.valueChanges
                        .pipe(
                            takeUntil(this._unsubscribeAll),
                            debounceTime(500),
                            distinctUntilChanged()
                        )
                        .subscribe(data => {
                            this._taskService.updateTask(data);
                        });
                }
            });

        // Subscribe to update on priority change
        this._taskService.onPrioritiesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(priorities => {
                this.priorities = priorities;
            });

        // Subscribe to update on priority change
        this._taskService.onStatesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(states => {
                this.states = states;
            });

        // Subscribe to update on user change
        this._taskService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });

        // Subscribe to update on user change
        this._taskService.onCustomersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(customers => {
                this.customers = customers;
                this.filteredCustomers = this.customers;
            });

        // Subscribe to update on priority change
        this._taskService.onNewTaskClicked
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.task = new Task({});
                // this.task.id = FuseUtils.generateGUID();
                this.formType = 'new';
                this.taskForm = this.createTaskForm();
                this.minDate = this.taskForm.get('startDate').value;

                this._taskService.onCurrentTaskChanged.next([this.task, 'new']);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Create task form
     *
     * @returns {FormGroup}
     */
    createTaskForm(): FormGroup {
        return this._formBuilder.group({
            'id': [this.task.id],
            'title': [this.task.title],
            'description': [this.task.description],
            'startDate': new FormControl({value: this.task.startDate, disabled: true}),
            'dueDate': new FormControl({value: this.task.dueDate, disabled: true}),
            'state': [this.task.state],
            'priority': [this.task.priority],
            'assignTo': [this.task.assignTo],
        });
    }


    /**
     * Toggle Deleted
     *
     * @param event
     */
    toggleDeleted(event): void {
        event.stopPropagation();
        if (this.task && this.task.id && this.formType === 'edit') {
            this._taskService.deleteTask(this.task.id);
        } else {
            this._taskService.onCurrentTaskChanged.next([null, null]);
        }
        this.task = null;
    }


    togglePriorityOnTask(priority): void {
        this.task.priority = priority;
        if (this.task && this.task.id && this.formType === 'edit') {
            this._taskService.togglePriorityOnTask(priority, this.task);
        }
    }

    toggleStateOnTask(state): void {
        this.task.state = state;
        if (this.task && this.task.id && this.formType === 'edit') {
            this._taskService.toggleStateOnTask(state, this.task);
        }
    }

    toggleUserOnTask(user): void {
        this.task.assignTo = user;
        if (this.task && this.task.id && this.formType === 'edit') {
            this._taskService.toggleUserOnTask(user, this.task);
        }
    }



    toggleCustomerOnTask(customer): void {
        this.task.customer = customer;
        if (this.task && this.task.id && this.formType === 'edit') {
            this._taskService.toggleCustomerOnTask(customer, this.task);
        }

    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag==='user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        } else if (tag==='customer') {
            this.filteredCustomers = this.entityListUtils.searchCustomer(query, this.customers);
        }
    }

    /**
     * Add task
     */
    addTask(): void {

        this.task.id = this.taskForm.get('id').value;
        this.task.title = this.taskForm.get('title').value;
        this.task.description = this.taskForm.get('description').value;
        this.task.startDate = this.taskForm.get('startDate').value;
        this.task.dueDate = this.taskForm.get('dueDate').value;
        this.task.createBy = this.currentUser;
        this.task.updateBy = this.currentUser;

        if (!this.task.assignTo) {
            this.task.assignTo = this.currentUser;
        }

        if (!this.task.id) {
            this._taskService.createTask(this.task);
        } else {
            this._taskService.updateTask(this.task);
        }

        this.task = null;

        this._router.navigateByUrl('/views/tasks');
    }

    addEvent(event?: MatDatepickerInputEvent<Date>) {
        let selectedDate = new Date();
        if (event) {
            selectedDate= new Date(event.value);
            this.minDate = new Date(selectedDate);
        }
    }
}
