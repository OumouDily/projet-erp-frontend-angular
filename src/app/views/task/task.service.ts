import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {FuseUtils} from '@fuse/utils';
import {TASK_PRIORITY, TASK_STATE} from '../../data/enums/enums';
import {DsnUtils} from '../../utils/dsn-utils';
import {environment} from '../../../environments/environment';
import {SearchBody} from '../../utils/search-body';
import {Task} from '../../data/models/task.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../services/user.service';
import {User} from '../../data/models/user.model';
import {Customer} from '../../data/models/customer.model';
import {CustomersService} from '../crm/customers/customers.service';


@Injectable({
    providedIn: 'root'
})
export class TaskService implements Resolve<any> {

    dsnUtils = new DsnUtils();
    currentUser = this.dsnUtils.getAppUser();

    readonly serviceURL: string;
    readonly httpOptions: any;

    tasks: Task[];
    filteredTasks: Task[];
    selectedTasks: Task[];
    currentTask: Task;

    users: User[];
    selectedUser: User;

    customers: Customer[];
    selectedCustomer: Customer;

    searchText: string;

    states = [];
    priorities = [];

    routeParams: any;

    onTasksChanged: BehaviorSubject<any>;
    onSelectedTasksChanged: BehaviorSubject<any>;
    onCurrentTaskChanged: BehaviorSubject<any>;

    onStatesChanged: BehaviorSubject<any>;
    onPrioritiesChanged: BehaviorSubject<any>;

    onSearchTextChanged: BehaviorSubject<any>;
    onNewTaskClicked: Subject<any>;

    onSelectedUserChanged: BehaviorSubject<any>;
    onUsersChanged: BehaviorSubject<any>;

    onSelectedCustomerChanged: BehaviorSubject<any>;
    onCustomersChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param {Location} _location
     */
    constructor(
        private _httpClient: HttpClient,
        private _location: Location,
        private _userService: UserService,
        private _customersService: CustomersService,
        private _toastr: ToastrService
    ) {
        this.serviceURL = environment.serviceUrl + '/tasks';
        this.httpOptions = new DsnUtils().httpHeaders();

        // Set the defaults
        this.filteredTasks = [];
        this.selectedTasks = [];
        this.searchText = '';
        this.onTasksChanged = new BehaviorSubject([]);
        this.onSelectedTasksChanged = new BehaviorSubject([]);
        this.onCurrentTaskChanged = new BehaviorSubject([]);
        this.onStatesChanged = new BehaviorSubject([]);
        this.onPrioritiesChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new BehaviorSubject('');
        this.onNewTaskClicked = new Subject();
        this.onSelectedUserChanged = new BehaviorSubject([]);
        this.onUsersChanged = new BehaviorSubject([]);
        this.onSelectedCustomerChanged = new BehaviorSubject([]);
        this.onCustomersChanged = new BehaviorSubject([]);
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getStates(),
                this.getPriorities(),
                this.getUsers(),
                this.getCustomers(),
                this.getTasks()
            ]).then(
                () => {
                    if (this.routeParams.taskId) {
                        this.setCurrentTask(this.routeParams.taskId);
                    } else {
                        this.setCurrentTask(null);
                    }

                    this.onSearchTextChanged.subscribe(searchText => {
                        if (searchText !== '') {
                            this.searchText = searchText;
                            this.tasks = FuseUtils.filterArrayByString(this.filteredTasks, this.searchText);
                            this.onTasksChanged.next(this.tasks);
                        } else {
                            this.searchText = searchText;
                            this.tasks = FuseUtils.filterArrayByString(this.filteredTasks, this.searchText);
                            this.onTasksChanged.next(this.tasks);
                        }
                    });
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all states
     *
     * @returns {Promise<any>}
     */
    getStates(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.states = Object.keys(TASK_STATE);
            this.onStatesChanged.next(this.states);
            resolve(this.states);
        });
    }

    /**
     * Get all priorities
     *
     * @returns {Promise<any>}
     */
    getPriorities(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.priorities = Object.keys(TASK_PRIORITY);
            this.onPrioritiesChanged.next(this.priorities);
            resolve(this.priorities);
        });
    }

    /**
     * Get tasks
     *
     * @returns {Promise<Task[]>}
     */
    getTasks(): Promise<Task[]> {

        if (this.routeParams.priorityHandle) {
            return this.getTasksByPriority(this.routeParams.priorityHandle);
        }

        if (this.routeParams.stateHandle) {
            return this.getTasksByState(this.routeParams.stateHandle);
        }

        return this.getTasksByParams(this.routeParams);
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {

            this._userService.list()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.users = ret['response'];
                        this.onUsersChanged.next(this.users);
                    }
                    resolve(this.users);
                });
        });
    }

    getCustomers(): any {
        return new Promise((resolve, reject) => {

            this._customersService.findAll()
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this.customers = ret['response'];
                        this.onCustomersChanged.next(this.customers);
                    }
                    resolve(this.customers);
                });
        });
    }

    /**
     * Get tasks by params
     *
     * @param handle
     * @returns {Promise<Task[]>}
     */
    getTasksByParams(handle): Promise<Task[]> {
        return new Promise((resolve, reject) => {

            let searchBody = new SearchBody();
            if (this.selectedUser) {
                searchBody.assignToId = this.selectedUser.id;
            }
            if (this.selectedCustomer) {
                searchBody.customerId = this.selectedCustomer.id;
            }
            // TODO : CHECK USER ROLE
            this._httpClient.post(this.serviceURL + '/find', searchBody, this.httpOptions)
                .subscribe(ret => {

                    this.filteredTasks = ret['response'].map(task => {
                        return new Task(task);
                    });

                    this.tasks = FuseUtils.filterArrayByString(this.filteredTasks, this.searchText);

                    this.onTasksChanged.next(this.tasks);

                    resolve(this.tasks);
                });
        });
    }

    /**
     * Get tasks by state
     *
     * @param handle
     * @returns {Promise<Task[]>}
     */
    getTasksByState(handle): Promise<Task[]> {

        return new Promise((resolve, reject) => {
            let searchBody = new SearchBody();
            searchBody.taskState = handle;
            if (this.selectedUser) {
                searchBody.assignToId = this.selectedUser.id;
            }
            if (this.selectedCustomer) {
                searchBody.customerId = this.selectedCustomer.id;
            }
            // TODO : CHECK USER ROLE

            this._httpClient.post(this.serviceURL + '/find', searchBody, this.httpOptions)
                .subscribe((ret: any) => {

                    this.filteredTasks = ret['response'].map(task => {
                        return new Task(task);
                    });

                    this.tasks = FuseUtils.filterArrayByString(this.filteredTasks, this.searchText);

                    this.onTasksChanged.next(this.tasks);

                    resolve(this.tasks);

                }, reject);
        });
    }

    /**
     * Get tasks by priority
     *
     * @param handle
     * @returns {Promise<Task[]>}
     */
    getTasksByPriority(handle): Promise<Task[]> {
        return new Promise((resolve, reject) => {

            let searchBody = new SearchBody();
            searchBody.taskPriority = handle;
            if (this.selectedUser) {
                searchBody.assignToId = this.selectedUser.id;
            }
            if (this.selectedCustomer) {
                searchBody.customerId = this.selectedCustomer.id;
            }
            // TODO : CHECK USER ROLE

            this._httpClient.post(this.serviceURL + '/find', searchBody, this.httpOptions)
                .subscribe((ret: any) => {

                    this.filteredTasks = ret['response'].map(task => {
                        return new Task(task);
                    });

                    this.tasks = FuseUtils.filterArrayByString(this.filteredTasks, this.searchText);

                    this.onTasksChanged.next(this.tasks);

                    resolve(this.tasks);

                }, reject);
        });
    }

    /**
     * Toggle selected task by id
     *
     * @param id
     */
    toggleSelectedTask(id): void {
        // First, check if we already have that task as selected...
        if (this.selectedTasks.length > 0) {
            for (const task of this.selectedTasks) {
                // ...delete the selected task
                if (task.id === id) {
                    const index = this.selectedTasks.indexOf(task);

                    if (index !== -1) {
                        this.selectedTasks.splice(index, 1);

                        // Trigger the next event
                        this.onSelectedTasksChanged.next(this.selectedTasks);

                        // Return
                        return;
                    }
                }
            }
        }

        // If we don't have it, push as selected
        this.selectedTasks.push(
            this.tasks.find(task => {
                return task.id === id;
            })
        );

        // Trigger the next event
        this.onSelectedTasksChanged.next(this.selectedTasks);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void {
        if (this.selectedTasks.length > 0) {
            this.deselectTasks();
        } else {
            this.selectTasks();
        }
    }

    /**
     * Select tasks
     *
     * @param filterParameter
     * @param stateValue
     */
    selectTasks(filterParameter?, stateValue?): void {
        this.selectedTasks = [];

        // If there is no state, select all tasks
        if (filterParameter === undefined || stateValue === undefined) {
            this.selectedTasks = this.tasks;
        } else {
            this.selectedTasks.push(...
                this.tasks.filter(task => {
                    return task[filterParameter] === stateValue;
                })
            );
        }

        // Trigger the next event
        this.onSelectedTasksChanged.next(this.selectedTasks);
    }

    /**
     * Deselect tasks
     */
    deselectTasks(): void {
        this.selectedTasks = [];

        // Trigger the next event
        this.onSelectedTasksChanged.next(this.selectedTasks);
    }

    /**
     * Set current task by id
     *
     * @param id
     */
    setCurrentTask(id): void {

        this.currentTask = this.tasks.find(task => {
            return task.id === id;
        });

        this.onCurrentTaskChanged.next([this.currentTask, 'edit']);

        const priorityHandle = this.routeParams.priorityHandle,
            stateHandle = this.routeParams.stateHandle;

        if (priorityHandle) {
            this._location.go('/views/tasks/priority/' + priorityHandle + '/' + id);
        } else if (stateHandle) {
            this._location.go('/views/tasks/state/' + stateHandle + '/' + id);
        } else {
            this._location.go('/views/tasks/all/' + id);
        }
    }

    /**
     * Toggle priority on task
     *
     * @param priority
     * @param task
     */
    togglePriorityOnTask(priority, task): void {
        task.priority = priority;
        this.updateTask(task);
    }

    /**
     * Toggle state on task
     *
     * @param state
     * @param task
     */
    toggleStateOnTask(state, task): void {
        task.state = state;
        this.updateTask(task);
    }

    toggleUserOnTask(user, task): void {
        task.assignTo = user;
        this.updateTask(task);
    }

    toggleCustomerOnTask(customer, task): void {
        task.customer = customer;
        this.updateTask(task);
    }

    /**
     * Toggle priority on selected tasks
     *
     * @param priority
     */
    togglePriorityOnSelectedTasks(priority): void {
        this.selectedTasks.map(task => {
            this.togglePriorityOnTask(priority, task);
        });
    }

    /**
     * Toggle state on selected tasks
     *
     * @param state
     */
    toggleStateOnSelectedTasks(state): void {
        this.selectedTasks.map(task => {
            this.toggleStateOnTask(state, task);
        });
    }


    onUserSelected(user): any {
        return new Promise((resolve, reject) => {
            if (user) {
                this.selectedUser = user;
                this.onSelectedUserChanged.next(this.selectedUser);
            } else {
                this.selectedUser = null;
                this.onSelectedUserChanged.next(this.selectedUser);
            }
            this.getTasks().then(tasks => {
                resolve(tasks);
            }, reject);

        });
    }

    onCustomerSelected(customer): any {
        return new Promise((resolve, reject) => {
            if (customer) {
                this.selectedCustomer = customer;
                this.onSelectedCustomerChanged.next(this.selectedCustomer);
            } else {
                this.selectedCustomer = null;
                this.onSelectedCustomerChanged.next(this.selectedUser);
            }
            this.getTasks().then(tasks => {
                resolve(tasks);
            }, reject);

        });
    }


    /**
     * Create the task
     *
     * @param task
     * @returns {Promise<any>}
     */
    createTask(task): any {
        return new Promise((resolve, reject) => {

            this._httpClient.post(this.serviceURL + '/save', task, this.httpOptions)
                .subscribe(ret => {
                    if (ret['status'] === 'OK') {
                        this._toastr.success(ret['message']);
                    }
                    this.getTasks().then(tasks => {

                        resolve(tasks);

                    }, reject);
                });
        });
    }

    /**
     * Update the task
     *
     * @param task
     * @returns {Promise<any>}
     */
    updateTask(task): any {
        return new Promise((resolve, reject) => {

            this._httpClient.put(this.serviceURL + '/update', task, this.httpOptions)
                .subscribe(ret => {

                    this.getTasks().then(tasks => {

                        resolve(tasks);

                    }, reject);
                });
        });
    }

    /**
     * Update the task
     *
     * @param taskId
     * @returns {Promise<any>}
     */
    deleteTask(taskId): any {
        return new Promise((resolve, reject) => {

            this._httpClient.delete(this.serviceURL + '/' + taskId + '/delete', this.httpOptions)
                .subscribe(ret => {

                    if (ret['status'] === 'OK') {
                        this._toastr.success(ret['message']);
                    }

                    this.getTasks().then(tasks => {

                        resolve(tasks);

                    }, reject);
                });
        });
    }

}
