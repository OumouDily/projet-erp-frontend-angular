import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule
} from '@angular/material';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule} from '@fuse/components';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';
import {MainDashboardComponent} from './main.dashboard.component';
import {MainDashboardService} from './main.dashboard.service';
import {ViewsModule} from '../../views.module';
import {SpinnerModule} from '../../../shared/modules/spinner.module';

const routes: Routes = [
    {
        path: '**',
        component: MainDashboardComponent,
        resolve: {
            data: MainDashboardService
        }
    }
];

@NgModule({
    declarations: [
        MainDashboardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        MatInputModule,

        NgxChartsModule,

        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        ViewsModule,
        MatTooltipModule,
        SpinnerModule,
        MatDatepickerModule
    ],
    providers: [
        MainDashboardService
    ]

})
export class MainDashboardModule {
}

