import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import {fuseAnimations} from '@fuse/animations';

import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {MainDashboardService} from './main.dashboard.service';
import {DsnUtils} from '../../../utils/dsn-utils';
import {User} from '../../../data/models/user.model';
import {InvoiceDashBody} from '../../../utils/dashboard/invoice-dash-body';
import {CrmDashBody} from '../../../utils/dashboard/crm-dash-body';
import {RoleUtils} from '../../../utils/role-utils';
import {ProjectFolderDashBody} from '../../../utils/dashboard/project-folder-dash-body';
import {environment} from '../../../../environments/environment';
import {CommercialDashBody} from '../../../utils/dashboard/commercial-dash-body';
import {SearchBody} from '../../../utils/search-body';
import {EntityListUtils} from '../../../utils/entity.list.utils';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'main-dashboard',
    templateUrl: './main.dashboard.component.html',
    styleUrls: ['./main.dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MainDashboardComponent implements OnInit {

    dsnUtils = new DsnUtils();
    roleUtils = new RoleUtils();
    entityListUtils = new EntityListUtils();

    currentUser: User = this.dsnUtils.getAppUser();
    searchBody = new SearchBody();

    users: User[];
    selectedUser: User;
    filteredUsers: User[];

    years: string[];
    selectedYear: string;
    filteredYears: string[];

    invoiceDashBody: InvoiceDashBody;
    crmDashBody: CrmDashBody;
    commercialDashBody: CommercialDashBody;
    projectFolderDashBody: ProjectFolderDashBody;

    months: string[] = [];
    data: number[] = [];

    customers: string[] = [];
    customerAmounts: number[] = [];

    dashForm: FormGroup;

    dateNow = Date.now();
    today = new Date(this.dateNow);

    userImage: string;

    private _unsubscribeAll: Subject<any>;


    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _mainDashboardService: MainDashboardService,
        private _formBuilder: FormBuilder
    ) {
        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

        this._unsubscribeAll = new Subject();
    }


    ngOnInit(): void {
        document.title = 'DSN ERP | Tableau board';

        this._mainDashboardService.onUsersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(users => {
                this.users = users;
                this.filteredUsers = this.users;
            });

        this._mainDashboardService.onInvoiceDashBodyChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(invoiceDashBody => {
                this.invoiceDashBody = invoiceDashBody;
            });

        this._mainDashboardService.onCrmDashChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(crmDashBody => {
                this.crmDashBody = crmDashBody;
            });

        this._mainDashboardService.onCommercialDashBodyChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(commercialDashBody => {
                this.commercialDashBody = commercialDashBody;
            });

        this._mainDashboardService.onProjectFolderDashBodyChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(projectDashBody => {
                this.projectFolderDashBody = projectDashBody;
            });

        this.userImage = environment.fileUri + '/userImage/';

        if (this.currentUser) {
            this.currentUser.imageUri = this.userImage + this.currentUser.id;
        }

        this.initDashForm();
        this.initYears();
        this.initDashBodies();
    }

    initDashForm() {
        this.dashForm = this._formBuilder.group({
            startDate: new FormControl(new Date(this.dsnUtils.subDays(this.today, 30))),
            endDate: new FormControl(new Date(this.dateNow))
        });
    }

    initDashBodies(): void {

        this.initInvoicingChartData();

        this.initCrmBarChartData();


        this.initCommBarChartData();
    }

    initInvoicingChartData() {
        this.months = [];
        this.data = [];
        if (this.invoiceDashBody.invoiceGraphBodies) {
            this.invoiceDashBody.invoiceGraphBodies.forEach(item => {
                this.months.push(item.name);
                this.data.push(item.value);
            });
        }
    }

    initCrmBarChartData() {
        this.customers = [];
        this.customerAmounts = [];
        if (this.crmDashBody.customerGraphBodies) {
            this.crmDashBody.customerGraphBodies.forEach(item => {
                this.customers.push(item.name);
                this.customerAmounts.push(item.value);
            });
        }
    }

    initCommBarChartData() {

    }

    initYears() {
        this.years = this.dsnUtils.getLastYears(7);
        this.filteredYears = this.years;
    }

    onSearch(): void {

        let newSearchBody = new SearchBody();
        newSearchBody.year = this.searchBody.year = this.selectedYear ? this.selectedYear : null;
        if (newSearchBody.year == null) {
            newSearchBody.endDate = new Date(this.dashForm.get('endDate').value);
            newSearchBody.startDate = new Date(this.dashForm.get('startDate').value);
        }
        newSearchBody.createById = this.searchBody.createById = this.selectedUser ? this.selectedUser.id : null;

        this._mainDashboardService.getInvoiceDashBody(newSearchBody);
        this._mainDashboardService.getCrmDashbody(newSearchBody);
        this._mainDashboardService.getCommercialDashbody(newSearchBody);
        this._mainDashboardService.getProjectFolderDashBody(newSearchBody);

        this.initDashBodies();
    }

    onYearSelected(year?): void {
        this.selectedYear = year;
        this.onSearch();
    }

    onUserSelected(user?): void {
        this.selectedUser = user;
        this.onSearch();
    }

    onListItemFiltering(tag: string, query: any) {
        if (tag === 'year') {
            this.filteredYears = this.entityListUtils.searchYears(query, this.years);
        } else if (tag === 'user') {
            this.filteredUsers = this.entityListUtils.searchUser(query, this.users);
        }
    }

    toggleOnSearch(event): void {
        event.stopPropagation();
    }

    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}


