import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {InvoiceDashBody} from '../../../utils/dashboard/invoice-dash-body';
import {DsnUtils} from '../../../utils/dsn-utils';
import {environment} from '../../../../environments/environment';
import {CrmDashBody} from '../../../utils/dashboard/crm-dash-body';
import {RoleUtils} from '../../../utils/role-utils';
import {ProjectFolderDashBody} from '../../../utils/dashboard/project-folder-dash-body';
import {SearchBody} from '../../../utils/search-body';
import {CommercialDashBody} from '../../../utils/dashboard/commercial-dash-body';
import {User} from '../../../data/models/user.model';
import {UserService} from '../../../services/user.service';

@Injectable({
    providedIn: 'root'
})
export class MainDashboardService implements Resolve<any> {

    dsnUtils = new DsnUtils();
    currentUser: User;
    roleUtils: RoleUtils;

    invoiceDashBody: InvoiceDashBody;
    crmDash: CrmDashBody;
    commercialDashBody: CommercialDashBody;
    projectFolderDashBody: ProjectFolderDashBody;


    users: User[];
    onUsersChanged: BehaviorSubject<any>;
    onInvoiceDashBodyChanged: BehaviorSubject<any>;
    onCrmDashChanged: BehaviorSubject<any>;
    onCommercialDashBodyChanged: BehaviorSubject<any>;
    onProjectFolderDashBodyChanged: BehaviorSubject<any>;

    years: string[] = [];

    readonly httpOptions: any;
    readonly serviceDashboardURL: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    ) {
        this.httpOptions = new DsnUtils().httpHeaders();
        this.serviceDashboardURL = environment.serviceUrl + '/dashboards';

        this.onUsersChanged = new BehaviorSubject([]);
        this.onInvoiceDashBodyChanged = new BehaviorSubject({});
        this.onCrmDashChanged = new BehaviorSubject({});
        this.onCommercialDashBodyChanged = new BehaviorSubject({});
        this.onProjectFolderDashBodyChanged = new BehaviorSubject({});

        this.years = this.dsnUtils.getLastYears(7);
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getInvoiceDashBody(null),
                this.getCrmDashbody(null),
                this.getCommercialDashbody(null),
                this.getProjectFolderDashBody(null),
                this.getUsers()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get InvoiceDashBoard
     *
     * @returns {Promise<any>}
     */
    getInvoiceDashBody(searchBody?: SearchBody): Promise<any> {

        this.roleUtils = new RoleUtils();
        this.currentUser = this.dsnUtils.getAppUser();
        if (searchBody == null) {
            searchBody = new SearchBody();
            searchBody.year = this.years[0];
        }
        if (!this.roleUtils.canReadAllCommercialStat()) {
            searchBody.createById = this.currentUser.id;
        }

        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceDashboardURL + '/invoice', searchBody, this.httpOptions)
                .subscribe((response: any) => {
                    this.invoiceDashBody = response['response'];
                    this.onInvoiceDashBodyChanged.next(this.invoiceDashBody);
                    resolve(response['response']);
                }, reject);
        });
    }

    /**
     * Get CRMDashBoard
     *
     * @returns {Promise<any>}
     */
    getCrmDashbody(searchBody?: SearchBody): Promise<any> {

        this.roleUtils = new RoleUtils();
        this.currentUser = this.dsnUtils.getAppUser();
        if (searchBody == null) {
            searchBody = new SearchBody();
            searchBody.year = this.years[0];
        }
        if (!this.roleUtils.canReadAllCommercialStat()) {
            searchBody.createById = this.currentUser.id;
        }

        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceDashboardURL + '/crm', searchBody, this.httpOptions)
                .subscribe((response: any) => {
                    this.crmDash = response['response'];
                    this.onCrmDashChanged.next(this.crmDash);
                    resolve(response['response']);
                }, reject);
        });
    }

    /**
     * Get CommercialDashBoard
     *
     * @returns {Promise<any>}
     */
    getCommercialDashbody(searchBody?: SearchBody): Promise<any> {

        this.roleUtils = new RoleUtils();
        this.currentUser = this.dsnUtils.getAppUser();
        if (searchBody == null) {
            searchBody = new SearchBody();
            searchBody.year = this.years[0];
        }
        if (!this.roleUtils.canReadAllCommercialStat()) {
            searchBody.createById = this.currentUser.id;
        }

        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceDashboardURL + '/commercial', searchBody, this.httpOptions)
                .subscribe((response: any) => {
                    this.commercialDashBody = response['response'];
                    this.onCommercialDashBodyChanged.next(this.commercialDashBody);
                    resolve(response['response']);
                }, reject);
        });
    }

    /**
     * Get ProjectFolderDashBoard
     *
     * @returns {Promise<any>}
     */
    getProjectFolderDashBody(searchBody?: SearchBody): Promise<any> {

        this.roleUtils = new RoleUtils();
        this.currentUser = this.dsnUtils.getAppUser();
        if (searchBody == null) {
            searchBody = new SearchBody();
            searchBody.year = this.years[0];
        }
        if (this.roleUtils.canReadAllCommercialStat()) {
            searchBody.createById = this.currentUser.id;
        }

        return new Promise((resolve, reject) => {
            this._httpClient.post(this.serviceDashboardURL + '/project-folder', searchBody, this.httpOptions)
                .subscribe((response: any) => {
                    this.projectFolderDashBody = response['response'];
                    this.onProjectFolderDashBodyChanged.next(this.projectFolderDashBody);
                    resolve(response['response']);
                }, reject);
        });
    }

    getUsers(): any {
        return new Promise((resolve, reject) => {
            this._userService.list().subscribe(ret => {
                if (ret['status'] === 'OK') {
                    this.users = ret['response'];
                    this.onUsersChanged.next(this.users);
                }
                resolve(this.users);
            });
        });
    }

}