import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-com-graph',
  templateUrl: './com-graph.component.html',
  styleUrls: ['./com-graph.component.scss']
})
export class ComGraphComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      let options: any = {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Evolution des turnover selon les commerciaux'
          },
          xAxis: {
              categories: ['Denon', 'Alassane', 'Fatoumata', 'Tidiane', 'Macalou']
          },
          yAxis: {
              title: {
                  text: 'Montants'
              }
          },
          credits: {
              enabled: false
          },
          series: [{
              name: 'turnover Objectif',
              data: [5, 3, 4, 7, 2]
          }, {
              name: 'turnover Realisé',
              data: [2, 2, 1.5, 2, 1]
          }, {
              name: 'turnover en Attente',
              data: [3, 4, 4, 2, 5]
          }]
      };
      Highcharts.chart('commercial', options);
  }

}
