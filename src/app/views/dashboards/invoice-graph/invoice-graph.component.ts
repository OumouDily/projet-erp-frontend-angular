import {Component, Input, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    selector: 'app-invoice-graph',
    templateUrl: './invoice-graph.component.html',
    styleUrls: ['./invoice-graph.component.scss']
})
export class InvoiceGraphComponent implements OnInit {

    @Input() months: any = [];
    @Input() data: any = [];

    constructor() { }
    ngOnInit() {
        let options: any = {
            Chart: {
                type: 'bar',
                height: 700
            },
            title: {
                text: 'Evolution de la facturation'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                title: {
                    text: 'Mois'
                },
                categories: this.months
            },
            yAxis: {
                title: {
                    text: 'Montant (F CFA)'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.x + ' : </strong>' + this.y;
                }
            },
            series: [{
                name: 'Facture',
                data: this.data
            }]
        };
        Highcharts.chart('container', options);
    }


}
