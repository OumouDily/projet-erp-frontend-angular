import {Component, Input, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    selector: 'app-crm-graph',
    templateUrl: './crm-graph.component.html',
    styleUrls: ['./crm-graph.component.scss']
})
export class CrmGraphComponent implements OnInit {

    @Input() amounts: number [] = [];
    @Input() customers: string [] = [];

    constructor() {

    }
    ngOnInit() {
        let options: any = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Facturation des clients'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                title: {
                    text: 'Client'
                },
                categories: this.customers
            },
            yAxis: {
                title: {
                    text: 'Montant'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f} F CFA'
                    }
                }
            },

            tooltip: {
                formatter: function() {
                    return '<strong>' + this.x + ' : </strong>' + this.y + ' F CFA';
                }
            },

            series: [{
                name: 'CRM',
                data: this.amounts
            }]
        };
        Highcharts.chart('barchart', options);
    }


}
