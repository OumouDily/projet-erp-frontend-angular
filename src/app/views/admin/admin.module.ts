import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule, MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule
} from '@angular/material';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AgmCoreModule} from '@agm/core';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';
import {UsersService} from './users/users.service';
import {UsersComponent} from './users/users.component';
import {RolesService} from './roles/roles.service';
import {RolesComponent} from './roles/roles.component';
import {SpinnerModule} from '../../shared/modules/spinner.module';
import {AdminCrudRoleComponent} from './crud-role/crud-role.component';
import {AdminCrudRoleService} from './crud-role/crud-role.service';
import {AdminCrudUserComponent} from './crud-user/crud-user.component';
import {AdminCrudUserService} from './crud-user/crud-user.service';
import {RoleMenuGuard, RoleUpdateGuard, UserCreateGuard, UserMenuGuard, UserUpdateGuard} from '../../shared/guard/role.guard';
import {IConfig, NgxMaskModule} from 'ngx-mask';


// @ts-ignore
export const options: Partial<IConfig> | (() => Partial<IConfig>);

const routes: Routes = [
    {
        path: 'roles',
        component: RolesComponent,
        resolve: {
            data: RolesService
        },
        canActivate: [RoleMenuGuard]
    },
    {
        path: 'roles/by-name/:name',
        component: AdminCrudRoleComponent,
        resolve: {
            data: AdminCrudRoleService
        }
    },
    {
        path: 'roles/:name',
        component: AdminCrudRoleComponent,
        resolve: {
            data: AdminCrudRoleService
        },
        canActivate: [RoleUpdateGuard]
    },
    {
        path: 'users',
        component: UsersComponent,
        resolve: {
            data: UsersService
        },
        canActivate: [UserMenuGuard]
    },
    {
        path: 'users/:id',
        component: AdminCrudUserComponent,
        resolve: {
            data: AdminCrudUserService
        },
        canActivate: [UserCreateGuard]
    },
    {
        path: 'users/:id/:name',
        component: AdminCrudUserComponent,
        resolve: {
            data: AdminCrudUserService
        },
        canActivate: [UserUpdateGuard]
    }
];

@NgModule({
    declarations: [
        RolesComponent,
        UsersComponent,
        AdminCrudRoleComponent,
        AdminCrudUserComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        NgxMaskModule.forRoot(),
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatDialogModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule,
        SpinnerModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatListModule,
        MatDividerModule
    ],
    providers: [AdminCrudRoleService, AdminCrudUserService],
    entryComponents: []
})
export class AdminModule {
}
