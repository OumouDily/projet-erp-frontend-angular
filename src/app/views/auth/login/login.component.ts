import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {AuthBody} from '../../../utils/auth-body';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import {FuseNavigationService} from '../../../../@fuse/components/navigation/navigation.service';
import {FuseSidebarService} from '../../../../@fuse/components/sidebar/sidebar.service';
import {NavItemUtils} from '../../../utils/nav-item-utils';

@Component({
    selector: 'login-view',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    authBody: AuthBody;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param _fuseNavigationService
     * @param _fuseSidebarService
     * @param {FormBuilder} _formBuilder
     * @param router
     * @param authService
     * @param {ToastrService} toastr
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        private toastr: ToastrService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        document.title = 'DSN ERP | Auth';

        // Unregister the navigation to the service
        this._fuseNavigationService.unregister('main'),

        localStorage.removeItem('app-token');
        localStorage.removeItem('isLoggedin');
        this.authBody = new AuthBody();

        this.loginForm = this._formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    onLogin(): void {

        this.authBody.username = this.loginForm.value.username;
        this.authBody.password = this.loginForm.value.password;
        this.authService.login(this.authBody).subscribe(ret => {
            if (ret['status'] === 'OK') {
                this.toastr.success(ret['message']);
                localStorage.setItem('app-token', btoa(JSON.stringify(ret['response'])));
                localStorage.setItem('isLoggedin', 'true');

                // Register the navigation to the service
                this._fuseNavigationService.register('main', new NavItemUtils().getNavigations());

                // Set the main navigation as our current navigation
                this._fuseNavigationService.setCurrentNavigation('main');

                this.router.navigate(['/views/dashboards/main']);
            } else {
                this.toastr.error(ret['message']);
            }

        }, e => {
            this.toastr.error(environment.errorMessage);
        });

    }
}
