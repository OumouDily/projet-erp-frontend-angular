import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '@fuse/shared.module';
import {InvoiceGraphComponent} from './dashboards/invoice-graph/invoice-graph.component';
import {CrmGraphComponent} from './dashboards/crm-graph/crm-graph.component';
import { ComGraphComponent } from './dashboards/com-graph/com-graph.component';

const routes = [

    {
        path: 'dashboards/main',
        loadChildren: './dashboards/main/main.dashboard.module#MainDashboardModule'
    },

    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule',
    },

    {
        path: 'crm',
        loadChildren: './crm/crm.module#CrmModule',
    },

    {
        path: 'products',
        loadChildren: './products/products.module#ProductsModule'
    },

    {
        path: 'tasks',
        loadChildren: './task/task.module#TaskModule',
    },

    {
        path: 'setting',
        loadChildren: './setting/setting.module#SettingModule',
    },
    {
        path: 'invoice',
        loadChildren: './invoice/invoice.module#InvoiceModule',
    }


];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    declarations: [
        InvoiceGraphComponent,
        CrmGraphComponent,
        ComGraphComponent
    ],
    providers: [],
    exports: [
        InvoiceGraphComponent,
        CrmGraphComponent,
        ComGraphComponent
    ],
    entryComponents: []
})
export class ViewsModule {
}
