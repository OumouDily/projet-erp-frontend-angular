import {FuseNavigation} from '@fuse/types';
import {NavItemUtils} from '../utils/nav-item-utils';

export const navigation: FuseNavigation[] = new NavItemUtils().getNavigations();
