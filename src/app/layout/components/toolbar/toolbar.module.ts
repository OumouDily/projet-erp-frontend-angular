import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    MatButtonModule,
    MatDialogModule,
    MatIconModule, MatInputModule,
    MatMenuModule,
    MatToolbarModule
} from '@angular/material';

import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ToolbarComponent } from 'app/layout/components/toolbar/toolbar.component';
import {AuthResetPasswordFormDialogComponent} from "../../../views/auth/reset-password-form/reset-password-form.component";
import {SpinnerSmallComponent} from '../../../shared/components/spinner/spinner.small.component';
import {SpinnerModule} from '../../../shared/modules/spinner.module';

@NgModule({
    declarations: [
        ToolbarComponent,
        AuthResetPasswordFormDialogComponent
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatDialogModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule,
        MatInputModule,
        SpinnerModule

    ],
    entryComponents: [
        AuthResetPasswordFormDialogComponent
    ],
    exports     : [
        ToolbarComponent
    ]
})
export class ToolbarModule
{
}
