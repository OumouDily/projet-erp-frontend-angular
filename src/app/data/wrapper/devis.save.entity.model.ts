import {Injectable} from '@angular/core';
import {Devis} from '../models/devis.model';
import {DevisLine} from '../models/devis.line.model';
import {Deserializable} from './deserializable.wrapper';


@Injectable()
export class DevisSaveEntity implements Deserializable{

    devis: Devis;
    devisLines: DevisLine[];

    constructor(devisSaveEntity?) {
        devisSaveEntity = devisSaveEntity || {};
        this.devis = devisSaveEntity.devis;
        this.devisLines = devisSaveEntity.devisLines ;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

}


