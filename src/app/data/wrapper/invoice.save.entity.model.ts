import {Injectable} from '@angular/core';
import {Invoice} from '../models/invoice.model';
import {InvoiceLine} from '../models/invoice.line.model';
import {Deserializable} from './deserializable.wrapper';


@Injectable()
export class InvoiceSaveEntity implements Deserializable {

    invoice: Invoice;
    invoiceLines: InvoiceLine[];

    constructor(invoiceSaveEntity?) {
        invoiceSaveEntity = invoiceSaveEntity || {};
        this.invoice = invoiceSaveEntity.invoice;
        this.invoiceLines = invoiceSaveEntity.invoiceLines;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}


