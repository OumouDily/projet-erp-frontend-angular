import {Injectable} from '@angular/core';

@Injectable()
export class QuoteTrace {
    id: number;
    number: string;
    reason: string;
    date: Date;

    constructor(quoteTrace?) {
        quoteTrace = quoteTrace || {};
        this.id = quoteTrace.id;
        this.number = quoteTrace.number;
        this.reason = quoteTrace.reason;
        this.date = quoteTrace.date;
    }


}


