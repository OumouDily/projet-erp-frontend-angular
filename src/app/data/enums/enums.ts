export   enum INVOICE_STATE {
    OPEN = 'Ouverte',
    VALIDATE = 'Validee',
    IN_PAYMENT = 'En paiement',
    PAID = 'Soldee',
    CANCEL = 'Annulee',
    CLOSE = 'Clos'
}

export enum DEVIS_STATE {
    OPEN = 'Ouvert',
    INVOICED = 'Facture',
    /*VALIDATE = 'Valide',*/
    DUPLICATE = 'Duplique',
    CANCEL = 'Annule'
}

export enum PAYMENT_STATE {
    PENDING = 'En attente',
    VALIDATE = 'Payee'
}

export enum TASK_STATE {
    TODO = 'Non commence',
    IN_PROGRESS = 'Commence',
    DONE = 'Termine',
    PENDING = 'En attente'
}

export enum PROJECT_FOLDER_STATE {
    TODO = 'Non commence',
    IN_PROGRESS = 'Commence',
    DONE = 'Termine',
    PENDING = 'En attente'
}

export enum TASK_PRIORITY {
    LOW = 'Faible',
    MEDIUM = 'Moyenne',
    HIGH = 'Haute',
    URGENT = 'Urgente'
}

export enum PROJECT_FOLDER_PRIORITY {
    LOW = 'Faible',
    MEDIUM = 'Moyenne',
    HIGH = 'Haute',
    URGENT = 'Urgente'
}

export enum TASK_ACTION {
    PHONING = 'Phoning',
    RDV = 'Rendez-vous',
    MAILING = 'Mailing',
    NEGO = 'Negociation',
    OTHER = 'Autre'
}

export enum CUSTOMER_TYPE {
    PROSPECT = 'PROSPECT',
    CLIENT = 'CLIENT'
}

export enum CUSTOMER_STATE {
    NOT_CONTACTED = 'Non contacte',
    CONTACTED = 'Contacte',
    PREQUALIFIED = 'Prequalifie',
    QUALIFIED = 'Qualifie',
    LOSED = 'Perdu'
}


export enum METHOD_OF_PAYMENT {
    CHECK = 'Chèque',
    TRANSFER = 'Virement',
    CASH = 'Espèce'
}