import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {User} from './user.model';
import {Customer} from './customer.model';
import {TASK_ACTION, TASK_PRIORITY, TASK_STATE} from '../enums/enums';


@Injectable()
export class Task implements Deserializable {

    id?: number;
    title?: TASK_ACTION;
    description?: string;
    startDate?: Date;
    dueDate?: Date;
    state?: TASK_STATE;
    priority?: TASK_PRIORITY;
    createDate?: Date;
    updateDate?: Date;
    enabled?: boolean;
    assignTo?: User;
    createBy?: User;
    updateBy?: User;
    customer?: Customer;


    constructor(task?) {
        task = task || {};
        this.id = task.id || null;
        this.title = task.title;
        this.description = task.description || '';
        this.startDate = task.startDate;
        this.dueDate = task.dueDate;
        this.state = task.state;
        this.priority = task.priority;
        this.createDate = task.createDate;
        this.updateDate = task.updateDate;
        this.enabled = task.enabled || true;
        this.assignTo = task.assignTo;
        this.createBy = task.createBy;
        this.updateBy = task.updateBy;
        this.customer = task.customer;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
