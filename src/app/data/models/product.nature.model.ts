import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class ProductNature implements Deserializable {

    id?: number;
    name?: string;

    constructor(productCategory?) {
        productCategory = productCategory || {};
        this.id = productCategory.id;
        this.name = productCategory.nom;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
