import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Customer} from './customer.model';
import {PROJECT_FOLDER_PRIORITY, PROJECT_FOLDER_STATE} from '../enums/enums';
import {User} from './user.model';


@Injectable()
export class ProjectFolder implements Deserializable {

    id?: number;
    name?: string;
    priority?: PROJECT_FOLDER_PRIORITY;
    chanceRate?: number;
    state?: PROJECT_FOLDER_STATE;
    startDate?: Date;
    endDate?: Date;
    clientBudget?: number;
    turnover?: number;
    note?: string;
    customer?: Customer;
    agent?: User;
    createBy?: User;
    updateBy?: User;


    constructor(projectFolder?) {
        projectFolder = projectFolder || {};
        this.id = projectFolder.id || null;
        this.name = projectFolder.name;
        this.priority = projectFolder.priority;
        this.chanceRate = projectFolder.chanceRate || 0;
        this.state = projectFolder.state;
        this.startDate = projectFolder.startDate;
        this.endDate = projectFolder.endDate;
        this.clientBudget = projectFolder.clientBudget || 0;
        this.turnover = projectFolder.turnover || 0;
        this.note = projectFolder.note;
        this.customer = projectFolder.customer;
        this.agent = projectFolder.agent;
        this.createBy = projectFolder.createBy;
        this.updateBy = projectFolder.updateBy;

    }


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }


}
