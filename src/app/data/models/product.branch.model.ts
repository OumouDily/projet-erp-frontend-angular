import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {BranchActivity} from './branch.activity.model';
import {Product} from './product.model';

@Injectable()
export class ProductBranch implements Deserializable {

    id?: number;
    product?: Product;
    branchActivity?: BranchActivity;

    constructor(productBranch?) {
        productBranch = productBranch || {};
        this.id = productBranch.id;
        this.product = productBranch.product;
        this.branchActivity = productBranch.branchActivity;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
