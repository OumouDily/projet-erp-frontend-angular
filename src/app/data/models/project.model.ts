import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {User} from './user.model';
import {Invoice} from './invoice.model';


@Injectable()
export class Project implements Deserializable {

    id?: number;
    title?: string;
    projectManager?: string;
    description?: string;
    amount?: number;
    startDate?: Date;
    endDate?: Date;
    enabled?: boolean;
    initiatedBy?: User;
    chargedBy?: User;
    createBy?: User;
    invoice?: Invoice;

    constructor(project?) {
        project = project || {};
        this.id = project.id || 0;
        this.title = project.title || '';
        this.projectManager = project.projectManager || '';
        this.description = project.description || '';
        this.amount = project.amber || 0;
        this.startDate = project.startDate;
        this.endDate = project.endDate;
        this.enabled = project.enabled || true;
        this.initiatedBy = project.initiatedBy;
        this.chargedBy = project.chargedBy;
        this.createBy = project.createBy;
        this.invoice = project.client;
    }


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }


}
