import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Devis} from './devis.model';

@Injectable()
export class DevisLine implements Deserializable {

    id?: number;
    designation?: string;
    quantity?: number = 0;
    unitPrice?: number = 0;
    amount?: number = 0;
    discount?: number = 0;
    devis?: Devis;

    constructor(devisLine?) {

        devisLine = devisLine || {};
        this.id = devisLine.id || null;
        this.designation = devisLine.designation;
        this.quantity = devisLine.quantity || 0;
        this.unitPrice = devisLine.unitPrice || 0;
        this.amount = devisLine.amount || 0;
        this.discount = devisLine.discount || 0;
        this.devis = devisLine.facture;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
