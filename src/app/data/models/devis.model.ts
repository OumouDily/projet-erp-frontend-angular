import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Customer} from './customer.model';
import {User} from './user.model';
import {DEVIS_STATE} from "../enums/enums";
import {Agent} from 'http';


@Injectable()
export class Devis implements Deserializable {

    id?: number;
    title?: string;
    number?: string;
    invoiceAddress?: string;
    taxValue?: number; // tva
    discount?: number;
    comment?: string;
    amountInLetter?: string;
    state: DEVIS_STATE.OPEN;
    privateComment?: string;
    devisDate?: Date = new Date();
    subTotal?: number = 0; // total devis ht
    total?: number = 0; // total du devis ttc
    taxAmount?: number = 0; // amount tva
    amountInvoiced?: number; // amount tva
    totalDiscount?: number; // remise total remise
    invoiceGenerated?: boolean = false;
    enabled?: boolean = true;
    canDuplicate?: boolean = false;
    showReglementMode?: boolean = true;
    showComment?: boolean = true;
    customer?: Customer;
    agent?: User;
    createDate?: Date = new Date();
    updateDate?: Date = new Date();
    cancelDate?: Date;
    createBy?: User;
    updateBy?: User;
    cancelBy?: User;


    constructor(devis?) {
        devis = devis || {};
        this.id = devis.id;
        this.title = devis.title;
        this.number = devis.number;
        this.discount = devis.discount;
        this.invoiceAddress = devis.invoiceAddress;
        this.taxValue = devis.taxValue;
        this.comment = devis.comment;
        this.amountInLetter = devis.amountInLetter;
        this.state = devis.state;
        this.privateComment = devis.privateComment;
        this.devisDate = devis.devisDate;
        this.subTotal = devis.subTotal;
        this.total = devis.total;
        this.taxAmount = devis.taxAmount;
        this.amountInvoiced = devis.amountInvoiced;
        this.invoiceGenerated = devis.invoiceGenerated;
        this.enabled = devis.enabled;
        this.canDuplicate = devis.canDuplicate;
        this.showReglementMode = devis.showReglementMode;
        this.showComment = devis.showComment;
        this.customer = devis.customer;
        this.agent = devis.agent;
        this.createBy = devis.createBy;
        this.updateBy = devis.updateBy;
        this.cancelBy = devis.cancelBy;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
