import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class BranchActivity implements Deserializable {

    id?: number;
    name?: string;
    description?: string;
    enabled?: boolean;

    constructor(branchActivity?) {
        branchActivity = branchActivity || {};
        this.id = branchActivity.id;
        this.name = branchActivity.nom;
        this.description = branchActivity.description;
        this.enabled = branchActivity.enabled || true;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
