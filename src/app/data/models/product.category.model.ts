import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class ProductCategory implements Deserializable {

    id?: number;
    name?: string;
    description?: string;

    constructor(productCategory?) {
        productCategory = productCategory || {};
        this.id = productCategory.id;
        this.name = productCategory.nom;
        this.description = productCategory.description;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
