import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Right} from './right.model';

@Injectable()
export class Profile implements Deserializable {

    id?: number;
    name?: string;
    description?: string;
    createDate?: Date;
    updateDate?: Date;
    enabled?: boolean;
    sourceRights?: Right[];
    targetRights?: Right[];


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
