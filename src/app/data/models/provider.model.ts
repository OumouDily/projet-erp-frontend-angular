import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Customer} from './customer.model';
import {Product} from './product.model';

@Injectable()
export class Provider implements Deserializable {

    id?: number;
    code?: string;
    name?: string;
    telephone?: string;
    email?: string;
    address?: string;
    company?: string;
    job?: string;
    notes?: string;
    country?: string;
    enabled?: boolean;
    products?: Product[];

    constructor(provider?) {
        provider = provider || {};
        this.id = provider.id || null;
        this.code = provider.code;
        this.name = provider.name;
        this.telephone = provider.telephone;
        this.email = provider.email;
        this.job = provider.job;
        this.address = provider.address;
        this.notes = provider.notes;
        this.company = provider.company;
        this.country = provider.country;
        this.products = provider.products;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
