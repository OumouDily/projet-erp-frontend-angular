import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Customer} from './customer.model';
import {Devis} from './devis.model';
import {User} from './user.model';
import {INVOICE_STATE, METHOD_OF_PAYMENT} from '../enums/enums';

@Injectable()
export class Invoice implements Deserializable {

    id?: number;
    number?: string;
    reference?: string;
    title?: string;
    invoiceAddress?: string;
    deliveryDelay?: string;
    methodOfPayment?: METHOD_OF_PAYMENT;
    comment?: string;
    amountInLetter?: string;
    discount?: number;
    privateComment?: string;
    state?: INVOICE_STATE = INVOICE_STATE.OPEN;
    invoiceDate?: Date = new Date();
    dueDate?: Date;   // echeance
    total?: number = 0;
    subTotal?: number = 0;
    taxAmount?: number = 0;
    amountPaid?: number = 0;
    stayToPay?: number = 0; // solde  = reste à payer
    taxValue?: number = 0;
    lastPaymentDate?: Date;
    createDate?: Date = new Date();
    updateDate?: Date;
    cancelDate?: Date;
    paymentCount?: number;
    paid?: boolean;
    enabled?: boolean;
    editable: boolean;
    customer?: Customer;
    devis?: Devis;
    agent?: User;
    createBy?: User;
    updateBy?: User;
    cancelBy?: User;


    constructor(invoice?) {
        invoice = invoice || {};
        this.id = invoice.id;
        this.number = invoice.number;
        this.reference = invoice.reference;
        this.title = invoice.title;
        this.discount = invoice.discount;
        this.invoiceAddress = invoice.invoiceAddress;
        this.deliveryDelay = invoice.deliveryDelay;
        this.methodOfPayment = invoice.methodOfPayment;
        this.comment = invoice.comment;
        this.amountInLetter = invoice.amountInLetter;
        this.customer = invoice.customer;
        this.privateComment = invoice.privateComment;
        this.invoiceDate = invoice.invoiceDate;
        this.state = invoice.state;
        this.dueDate = invoice.dueDate;
        this.total = invoice.total;
        this.subTotal = invoice.subTotal;
        this.taxAmount = invoice.taxAmount;
        this.amountPaid = invoice.amountPaid;
        this.stayToPay = invoice.stayToPay; // solde
        this.taxValue = invoice.taxValue;
        this.lastPaymentDate = invoice.lastPaymentDate;
        this.createDate = invoice.createDate;
        this.agent = invoice.agent;
        this.createBy = invoice.createBy;
        this.updateBy = invoice.updateBy;
        this.cancelBy = invoice.cancelBy;
        this.paymentCount = invoice.paymentCount;
        this.paid = invoice.paid;
        this.editable = invoice.editable;
        this.enabled = invoice.enabled;
        this.devis = invoice.devis;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
