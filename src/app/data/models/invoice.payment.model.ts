import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {User} from './user.model';
import {Invoice} from './invoice.model';
import {METHOD_OF_PAYMENT, PAYMENT_STATE} from '../enums/enums';


@Injectable()
export class InvoicePayment implements Deserializable {

    id?: number;
    number?: string;
    reference?: string;
    paymentDate?: Date;
    cancelDate?: Date;
    state?: PAYMENT_STATE;
    methodOfPayment?: METHOD_OF_PAYMENT;
    amount?: number;
    netToPay?: number;
    balanceBefore?: number;
    balanceAfter?: number;
    accountPaidBefore?: number;
    accountPaidAfter?: number;
    canceled?: boolean;
    amountInLetter?: string;
    invoice?: Invoice;
    createBy?: User;
    validateBy?: User;
    cancelBy?: User;


    constructor(invoicePayment?) {
        invoicePayment = invoicePayment || {};
        this.id = invoicePayment.id;
        this.number = invoicePayment.number;
        this.reference = invoicePayment.reference;
        this.paymentDate = invoicePayment.paymentDate;
        this.cancelDate = invoicePayment.cancelDate;
        this.state = invoicePayment.state;
        this.methodOfPayment = invoicePayment.methodOfPayment;
        this.amount = invoicePayment.amount;
        this.netToPay = invoicePayment.netToPay;
        this.balanceBefore = invoicePayment.balanceBefore;
        this.balanceAfter = invoicePayment.balanceAfter;
        this.accountPaidBefore = invoicePayment.accountPaidBefore;
        this.accountPaidAfter = invoicePayment.accountPaidAfter;
        this.canceled = invoicePayment.canceled;
        this.amountInLetter = invoicePayment.amountInLetter;
        this.invoice = invoicePayment.invoice || null;
        this.createBy = invoicePayment.createBy;
        this.validateBy = invoicePayment.validateBy;
        this.cancelBy = invoicePayment.cancelBy;

    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
