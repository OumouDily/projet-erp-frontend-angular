import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Contact} from './contact.model';
import {Customer} from './customer.model';

@Injectable()
export class CustomerContact implements Deserializable {

    id?: number;
    contact?: Contact;
    customer?: Customer;

    constructor(customerContact?) {
        customerContact = customerContact || {};
        this.id = customerContact.id || null;
        this.contact = customerContact.contact;
        this.customer = customerContact.customer;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
