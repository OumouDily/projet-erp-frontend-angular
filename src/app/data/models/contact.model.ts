import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Customer} from './customer.model';

@Injectable()
export class Contact implements Deserializable {

    id?: number;
    name?: string;
    phoneOne?: string;
    phoneTwo?: string;
    email?: string;
    address?: string;
    company?: string;
    job?: string;
    notes?: string;
    country?: string;
    customers?: Customer[];

    constructor(contact?) {
        contact = contact || {};
        this.id = contact.id || null;
        this.name = contact.name;
        this.phoneOne = contact.phoneOne;
        this.phoneTwo = contact.phoneTwo;
        this.email = contact.email;
        this.job = contact.job;
        this.address = contact.address;
        this.notes = contact.notes;
        this.company = contact.company;
        this.country = contact.country;
        this.customers = contact.customers;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
