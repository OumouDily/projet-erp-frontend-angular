import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class SectorActivity implements Deserializable {

    id: number;
    nom: string;
    description: string;
    enabled: boolean;

    constructor(sectorActivity?) {
        sectorActivity = sectorActivity || {};
        this.id = sectorActivity.id;
        this.nom = sectorActivity.nom ;
        this.description = sectorActivity.description;
        this.enabled = sectorActivity.enabled || true;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
