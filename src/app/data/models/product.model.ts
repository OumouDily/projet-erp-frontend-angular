import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {ProductCategory} from './product.category.model';
import {BranchActivity} from './branch.activity.model';
import {Provider} from './provider.model';
import {ProductNature} from './product.nature.model';

@Injectable()
export class Product implements Deserializable {

    id?: number;
    solution?: string;
    reference?: string;
    designation?: string;
    quantity?: number;
    unitPriceEURO?: number;
    unitPriceXOF?: number;
    distributivePrice?: number;
    customerPrice?: number;
    enabled?: boolean;
    productCategory?: ProductCategory;
    provider?: Provider;
    productNature?: ProductNature;
    branchActivities?: BranchActivity;

    constructor(product?) {
        product = product || {};
        this.id = product.id;
        this.solution = product.solution;
        this.reference = product.reference;
        this.designation = product.designation;
        this.quantity = product.quantity;
        this.unitPriceEURO = product.unitPriceEURO;
        this.unitPriceXOF = product.unitPriceXOF;
        this.distributivePrice = product.distributivePrice;
        this.customerPrice = product.customerPrice;
        this.enabled = product.enabled || true;
        this.productCategory = product.productCategory;
        this.provider = product.provider;
        this.productNature = product.productNature;
        this.branchActivities = product.branchActivities;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
