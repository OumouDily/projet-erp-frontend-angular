import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class Tax implements Deserializable {

    id?: number;
    libelle?: string;
    value?: number;

    constructor(taxe?) {
        taxe = taxe || {};
        this.id = taxe.id;
        this.libelle = taxe.libelle;
        this.value = taxe.number;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
