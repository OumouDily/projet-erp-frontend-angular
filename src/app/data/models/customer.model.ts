import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {BranchActivity} from './branch.activity.model';
import {User} from './user.model';
import {CUSTOMER_STATE, CUSTOMER_TYPE} from '../enums/enums';
import {Contact} from './contact.model';


@Injectable()
export class Customer implements Deserializable {

    id?: number;
    name?: string;
    description?: string;
    judicialNature?: string;
    telephoneOne?: string;
    telephoneTwo?: string;
    email?: string;
    addressOne?: string;
    addressTwo?: string;
    siteWeb?: string;
    turnover?: number;    // turnover
    numberOfEmployees?: number;
    contactName?: string;
    telephoneContact?: string;
    state?: CUSTOMER_STATE;
    type?: CUSTOMER_TYPE;
    createDate?: Date;
    updateDate?: Date;
    tranformationDate?: Date;
    enabled?: boolean;
    branchActivity?: BranchActivity;
    agent?: User;
    createBy?: User;
    updateBy?: User;
    contacts?: Contact[];

    /**
     * Constructor
     *
     * @param customer
     */
    constructor(customer?) {
        customer = customer || {};
        this.id = customer.id;
        this.name = customer.nom;
        this.description = customer.description;
        this.judicialNature = customer.judicialNature;
        this.telephoneOne = customer.telephoneOne;
        this.telephoneTwo = customer.telephoneTwo || '';
        this.email = customer.email || '';
        this.addressOne = customer.adresseOne || '';
        this.addressTwo = customer.adresseTwo || '';
        this.siteWeb = customer.siteWeb || '';
        this.turnover = customer.chiffreAffaire || '';
        this.numberOfEmployees = customer.nombreEmploye || '';
        this.contactName = customer.contactName || '';
        this.telephoneContact = customer.telephoneContact || '';
        this.state = customer.state || '';
        this.type = customer.type || '';
        this.createDate = customer.createDate || new Date();
        this.updateDate = customer.updateDate;
        this.tranformationDate = customer.dateTransformation;
        this.enabled = customer.enabled || true;
        this.branchActivity = customer.secteurActivite;
        this.agent = customer.agent;
        this.createBy = customer.createBy;
        this.updateBy = customer.updateBy;
        this.contacts = customer.contacts;

    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
