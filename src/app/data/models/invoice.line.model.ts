import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';
import {Invoice} from './invoice.model';

@Injectable()
export class InvoiceLine implements Deserializable {

    id?: number;
    designation?: string;
    quantity?: number = 0;
    unitPrice?: number = 0;
    amount?: number = 0;
    discount?: number = 0;
    invoice?: Invoice;

    constructor(invoiceLine?) {

        invoiceLine = invoiceLine || {};
        this.id = invoiceLine.id || 0;
        this.designation = invoiceLine.designation;
        this.quantity = invoiceLine.quantity || 0;
        this.unitPrice = invoiceLine.unitPrice || 0;
        this.amount = invoiceLine.amount || 0;
        this.discount = invoiceLine.discount || 0;
        this.invoice = invoiceLine.facture;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
