import {Deserializable} from './deserializable.model';
import {Injectable} from '@angular/core';

@Injectable()
export class Right implements Deserializable {

    id?: number;
    name?: string;
    description?: string;


    deserialize(input: any): this {
        return Object.assign(this, input);
    }

    equals(obj: any): boolean {
        return this.id === obj.id;
    }
}
