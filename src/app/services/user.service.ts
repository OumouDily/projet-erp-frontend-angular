import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DsnUtils} from '../utils/dsn-utils';
import {Observable} from 'rxjs';
import {User} from '../data/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    readonly serviceURL: string;
    readonly httpOptions: any;
    private appUser: User;


    constructor(private http: HttpClient) {
        let dsnUtils = new DsnUtils();
        this.serviceURL = environment.serviceUrl + '/users';
        this.httpOptions = dsnUtils.httpHeaders();
        this.appUser = dsnUtils.getAppUser();
    }

    public list() {
        return this.http.get(this.serviceURL + '/all', this.httpOptions);
    }


    public disable(userId: number) {
        return this.http.delete(this.serviceURL + '/' + userId + '/disable', this.httpOptions);
    }

    public enable(userId: number) {
        return this.http.get(this.serviceURL + '/' + userId + '/enable', this.httpOptions);
    }

    getUserById(id: number): Observable<any> {
        return this.http.get(this.serviceURL+'/' + id+'/getUser', this.httpOptions);
    }


}
