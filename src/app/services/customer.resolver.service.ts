import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {CustomersService} from '../views/crm/customers/customers.service';

@Injectable({
    providedIn: 'root'
})
export class CustomerResolverService implements Resolve<any> {

    constructor(private customersService: CustomersService) {
    }

    resolve() {
        return this.customersService.allCustomer();

    }


}
