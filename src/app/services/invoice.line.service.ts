import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DsnUtils} from '../utils/dsn-utils';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class InvoiceLineService {

    readonly serviceURL: string;
    readonly httpOptions: any;


    constructor(private http: HttpClient) {
        let dsnUtils = new DsnUtils();
        this.serviceURL = environment.serviceUrl + '/invoice-lines';
        this.httpOptions = dsnUtils.httpHeaders();
    }


    public findListByInvoice(invoiceId: number): Observable<any> {
        return this.http.get(this.serviceURL+'/find-by-invoice/' + invoiceId, this.httpOptions)
    }

    public findInvoiceLine(invoiceId: number): Observable<any> {
        return this.http.get(this.serviceURL+'/find-line/' + invoiceId,this.httpOptions);
    }


}
