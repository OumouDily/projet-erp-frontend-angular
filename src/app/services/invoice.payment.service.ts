import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DsnUtils} from '../utils/dsn-utils';
import {InvoicePayment} from "../data/models/invoice.payment.model";

@Injectable({
    providedIn: 'root'
})
export class InvoicePaymentService {

    readonly serviceURL: string;
    readonly httpOptions: any;

    constructor(private http: HttpClient) {
        let dsnUtils = new DsnUtils();
        this.serviceURL = environment.serviceUrl + '/payments';
        this.httpOptions = dsnUtils.httpHeaders();
    }

    public list() {
        return this.http.get(this.serviceURL + '/all', this.httpOptions);
    }

    public get(id: number) {
        return this.http.get(this.serviceURL + '/'+id+'/get', this.httpOptions);
    }

    public create(invoicePayment: InvoicePayment) {
        return this.http.post(this.serviceURL + '/create', invoicePayment, this.httpOptions);
    }

    public validate(invoicePayment: InvoicePayment) {
        return this.http.post(this.serviceURL + '/validate', invoicePayment, this.httpOptions);
    }

    public update(invoicePayment: InvoicePayment) {
        return this.http.put(this.serviceURL + '/update', invoicePayment, this.httpOptions);
    }

    public cancelPayment(invoicePayment: InvoicePayment) {
        return this.http.put(this.serviceURL + '/cancel', invoicePayment, this.httpOptions);
    }


}
