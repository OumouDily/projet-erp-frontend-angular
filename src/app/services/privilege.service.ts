import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DsnUtils} from '../utils/dsn-utils';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PrivilegeService {

    readonly serviceURL: string;
    readonly httpOptions: any;

    constructor(private http: HttpClient) {
        let dsnUtils = new DsnUtils();
        this.serviceURL = environment.serviceUrl + '/privileges';
        this.httpOptions = dsnUtils.httpHeaders();
    }

    findAll(): Observable<any> {
        return this.http.get(this.serviceURL+'/all', this.httpOptions)
    }



}
