import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DsnUtils} from '../utils/dsn-utils';
import {Observable} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class DevisLineService {

    readonly serviceURL: string;
    readonly httpOptions: any;


    constructor(private http: HttpClient) {
        let dsnUtils = new DsnUtils();
        this.serviceURL = environment.serviceUrl + '/devis-lines';
        this.httpOptions = dsnUtils.httpHeaders();
    }


    public findListByDevis(devisId: number): Observable<any> {
        return this.http.get(this.serviceURL+'/find-by-devis/' + devisId, this.httpOptions)
    }

    public findDevisLine(devisId: number): Observable<any> {
        return this.http.get(this.serviceURL+'/find-line/' + devisId,this.httpOptions);
    }


}
