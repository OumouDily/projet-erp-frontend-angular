import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {CustomersService} from '../views/crm/customers/customers.service';

@Injectable({
    providedIn: 'root'
})
export class ProspectResolverService implements Resolve<any> {

    constructor(private customersService: CustomersService) {
    }

    resolve() {
        return this.customersService.getProspects();

    }


}
