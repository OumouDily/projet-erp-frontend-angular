// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr       : false,
    dsnRedColor  : '#A61341',
    dsnGrisColor  : '#5F5F60',
    copyrightText : '<p>Copyright ® 2019 ' +
        '<a href="http://dsnmali.com" target="_blank" ' +
        'style="margin-left: 5px; color: #A61341; font-weight: normal !important;">DSN-MALI SARL</a></p>',
    baseUrl: 'http://localhost:8080/dsnerp-service',
    serviceUrl: 'http://localhost:8080/dsnerp-service/api',
    fileUri: 'http://193.70.37.40:8080/dsnerp',
    frenchCalendar: {
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['janv.', 'févr.', 'mars', 'avr.', 'mai', 'juin',
            'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
        dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
        dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    },
    dateFormati18n: {
        dayNames: [
            'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam',
            'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'
        ],
        monthNames: [
            'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec',
            'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
        ],
        timeNames: [
            'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
        ]
    },
    authTitle: 'AUTHENTIFICATION',
    errorMessage: 'Une erreur est survenue',
    errorNetworkMessage: 'Echec de connexion au serveur',
    notAuthorizedUserMessage: 'Vous n\'êtes pas autorisé à vous connecter sur cette application'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
