import { environment as defaultEnvironment } from './environment';
export const environment = {
    ...defaultEnvironment,
    production: false,
    hmr       : true,
    baseUrl: 'http://localhost:8080/dsnerp-service',
    serviceUrl: 'http://localhost:8080/dsnerp-service/api',
    fileUri: 'http://localhost:8080/dsnerp',
};
