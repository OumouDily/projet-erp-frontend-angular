import { environment as defaultEnvironment } from './environment';
export const environment = {
    ...defaultEnvironment,
    production: true,
    hmr       : false,
    baseUrl: 'http://193.70.37.40:8080/dsnerp-service',
    serviceUrl: 'http://193.70.37.40:8080/dsnerp-service/api',
    fileUri: 'http://193.70.37.40:8080/dsnerp',
};
